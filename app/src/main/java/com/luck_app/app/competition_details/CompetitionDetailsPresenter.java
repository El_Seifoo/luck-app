package com.luck_app.app.competition_details;

public interface CompetitionDetailsPresenter {
    void onButtonClicked(int index);
}
