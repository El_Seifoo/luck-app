package com.luck_app.app.competition_details;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompetitionDetailsModel {
    private Context context;

    public CompetitionDetailsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<Competition> competitionMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<Competition> fetchDetails(Map<String, String> body, Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<Competition>> call = MySingleton.getInstance(context).createService().privateCompDetails(header, body);

        call.enqueue(new Callback<ApiResponse<Competition>>() {
            @Override
            public void onResponse(Call<ApiResponse<Competition>> call, Response<ApiResponse<Competition>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<Competition> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        Competition competition = apiResponse.getData();
                        if (competition != null)
                            competitionMutableLiveData.setValue(competition);
                        else
                            callback.showResponseMessage(apiResponse.getMessage());

                    } else
                        callback.showResponseMessage(apiResponse.getMessage());
                } else if (response.code() == 500)
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
                else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        callback.showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        callback.showResponseMessage(context.getString(R.string.error_sending_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Competition>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return competitionMutableLiveData;
    }

    protected void answerQuestion(Map<String, String> body, Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().sendAnswer(header, body);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handlePostReqResponse(response, 0);
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t,1);
            }
        });
    }

    protected void likeUnLike(Map<String, String> body, Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().likeUnLike(header, body);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.handlePostReqResponse(response, 1);
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.onFailureHandler(t,1);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t, int index);

        void handlePostReqResponse(Response<ApiResponse<String>> response, int index);

        void showResponseMessage(String message);
    }
}
