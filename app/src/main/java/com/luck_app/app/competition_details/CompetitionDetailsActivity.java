package com.luck_app.app.competition_details;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityCompetitionDetailsBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.Gift;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CompetitionDetailsActivity extends AppCompatActivity implements CompetitionDetailsPresenter, CompetitionDetailsViewModel.ViewListener, GiftsAdapter.GiftItemListener {
    private ActivityCompetitionDetailsBinding dataBinding;
    private CompetitionDetailsViewModel viewModel;
    private RecyclerView giftsList;
    private GiftsAdapter adapter;

    private PlayerView playerView;
    private SimpleExoPlayer player;
    private boolean playWhenReady = false;
    private int currentWindow = 0;
    private long playbackPosition = 0;
    private final String PLAY_WHEN_READY_KEY = "playWhenReadyKey";
    private final String CURRENT_WINDOW_KEY = "currWindowKey";
    private final String PLAY_BACK_POSITION_KEY = "playBackPosKey";

    private CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_competition_details);
        viewModel = new ViewModelProvider(this).get(CompetitionDetailsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);
        dataBinding.setFirstAnswer(dataBinding.firstAnswer);
        dataBinding.setSecondAnswer(dataBinding.secondAnswer);
        dataBinding.setThirdAnswer(dataBinding.thirdAnswer);
        dataBinding.setFourthAnswer(dataBinding.fourthAnswer);


        playerView = dataBinding.videoView;


        giftsList = dataBinding.giftsList;
        giftsList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        adapter = new GiftsAdapter(this);
        giftsList.setAdapter(adapter);

        viewModel.checkIntent(getIntent());

        if (savedInstanceState != null) {
            playWhenReady = savedInstanceState.getBoolean(PLAY_WHEN_READY_KEY);
            currentWindow = savedInstanceState.getInt(CURRENT_WINDOW_KEY);
            playbackPosition = savedInstanceState.getLong(PLAY_BACK_POSITION_KEY);
        }

    }

    private void startCounter(String start) {
        //86400000
        //2020-04-09 20:15:00

        try {
            long endLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(start).getTime() + 86400000;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date today = Calendar.getInstance().getTime();
            long currentLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dateFormat.format(today)).getTime();

            long diff = endLong - currentLong;
            countDownTimer = new CountDownTimer(diff, 1000) {
                String counterString = "";

                @Override
                public void onTick(long millisUntilFinished) {
                    int second = (int) (millisUntilFinished / 1000) % 60;
                    int minute = (int) (millisUntilFinished / (1000 * 60)) % 60;
                    int hour = (int) (millisUntilFinished / (1000 * 60 * 60)) % 24;
                    int days = (int) (millisUntilFinished / (1000 * 60 * 60 * 24));

                    counterString = "";
                    if (days == 0) {
                        counterString = "";
                    } else if (days < 10) {
                        counterString += "0" + days + ":";
                    } else counterString += days + ":";

                    if (hour < 10)
                        counterString += "0" + hour + ":";
                    else counterString += hour + ":";

                    if (minute < 10)
                        counterString += "0" + minute + ":";
                    else counterString += minute + ":";

                    if (second < 10)
                        counterString += "0" + second;
                    else counterString += second;


                    dataBinding.setCounterTime(counterString);
                }

                @Override
                public void onFinish() {
                    dataBinding.setCounterTime("00:00:00");
                    finish();
                }
            };
            countDownTimer.start();

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("aaaaa", e.toString());
            dataBinding.counter.setVisibility(View.GONE);
        }
    }

    @Override
    public void setCompetitionDetails(Competition competition) {
        dataBinding.setCompetition(competition);
        startCounter(competition.getStartDate());
        viewModel.setShareViewVisibility(dataBinding.getCompetition().getCompetitionType());
        setGifts(competition.getGifts());
        if (dataBinding.getCompetition().getQuestionType() == 3) {
            initializePlayer();
        }

    }

    @Override
    public void startRequestingDetails() {
        viewModel.requestCompetitionDetails().observe(this, new Observer<Competition>() {
            @Override
            public void onChanged(Competition competition) {
                dataBinding.setCompetition(competition);
                startCounter(competition.getStartDate());
                viewModel.setShareViewVisibility(dataBinding.getCompetition().getCompetitionType());
                if (dataBinding.getCompetition().getQuestionType() == 3) {
                    initializePlayer();
                }
                setGifts(competition.getGifts());
            }
        });
    }

    @Override
    public void startSharing(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showQuestionPhoto() {
        String photo = dataBinding.getCompetition().getQuestion();
        if (photo != null) {
            if (!photo.isEmpty()) {

                ArrayList<String> photos = new ArrayList<>();
                photos.add(photo);

                new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
                    @Override
                    public void loadImage(ImageView imageView, String image) {
                        Glide.with(imageView.getContext())
                                .load(image)
                                .thumbnail(0.5f)
                                .error(R.mipmap.ic_launcher)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    }
                })
                        .withStartPosition(0)
                        .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                        .allowSwipeToDismiss(true)
                        .show();

            }
        }
    }

    private void setGifts(List<Gift> gifts) {
        viewModel.getGifts(gifts).observe(this, new Observer<List<Gift>>() {
            @Override
            public void onChanged(List<Gift> gifts) {
                adapter.setGifts(gifts);
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : share icon
                |-> 2 : send answer
                |-> 3 : retry fetching competition details
                |-> 4 : like competition
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void startAnswering() {
        Competition competition = dataBinding.getCompetition();
        ArrayList<CheckBox> checkBoxes = new ArrayList<>();
        checkBoxes.add(dataBinding.firstAnswer);
        checkBoxes.add(dataBinding.secondAnswer);
        checkBoxes.add(dataBinding.thirdAnswer);
        checkBoxes.add(dataBinding.fourthAnswer);

        viewModel.requestSendAnswer(competition.getId(), competition.getIsOwner(), competition.getQuestionType(), competition.getAnswers(), checkBoxes);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public int isCompetitionLiked() {
        return dataBinding.getCompetition().getIsLike();
    }

    @Override
    public void updateCompetitionLikes(int isLiked, int increase) {
        Competition competition = dataBinding.getCompetition();
        competition.setIsLike(isLiked);
        competition.increaseLikes(increase);
        dataBinding.setCompetition(competition);

    }

    @Override
    public void startLiking() {
        viewModel.requestLikeUnLike(dataBinding.getCompetition().getId());
    }

    @Override
    public String getCompetitionId() {
        return getIntent().getExtras().getString("competitionId");
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PLAY_BACK_POSITION_KEY, playWhenReady);
        outState.putInt(CURRENT_WINDOW_KEY, currentWindow);
        outState.putLong(PLAY_BACK_POSITION_KEY, playbackPosition);
    }

    private void initializePlayer() {
        if (dataBinding.getCompetition() != null) {
            if (dataBinding.getCompetition().getQuestionType() == 3) {
                player = new SimpleExoPlayer.Builder(this).build();
                playerView.setPlayer(player);
                Uri uri = Uri.parse(dataBinding.getCompetition().getQuestion());
                MediaSource mediaSource = buildMediaSource(uri);
                player.setPlayWhenReady(playWhenReady);
                player.seekTo(currentWindow, playbackPosition);
//            player.addListener(playbackStateListener);
                player.prepare(mediaSource, false, false);
            }
        }

    }

    private MediaSource buildMediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, "luck-app");
        return new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
    }


    private void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
//            player.removeListener(playbackStateListener);
            player.release();
            player = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23)
            initializePlayer();

    }


    @Override
    public void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23)
            initializePlayer();

    }

    @Override
    public void onPause() {
        super.onPause();
        releasePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        }).withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();
    }
}
