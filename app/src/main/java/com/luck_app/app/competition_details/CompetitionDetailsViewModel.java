package com.luck_app.app.competition_details;

import android.app.Application;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.Answer;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.Gift;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

public class CompetitionDetailsViewModel extends AndroidViewModel implements CompetitionDetailsModel.ModelCallback {
    private CompetitionDetailsModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyListTextView /* flag to know if gifts list is empty or not */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<Integer> checkedAnswer/* flag to know which answer is selected by user */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<Integer> shareView/* flag to know if share view is visible or not */;
    private int questionType = 1;

    public CompetitionDetailsViewModel(@NonNull Application application) {
        super(application);
        model = new CompetitionDetailsModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        checkedAnswer = new ObservableField<>(-1);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        shareView = new ObservableField<>(View.INVISIBLE);
    }

    private Intent getShareIntent() {
        Intent share = new Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, getApplication().getString(R.string.share_private_competition_link, viewListener.getCompetitionId()));

        return share;
    }

    public void setShareViewVisibility(int competitionType) {
        if (getShareIntent().resolveActivity(getApplication().getPackageManager()) == null)
            setShareView(View.INVISIBLE);
        else setShareView(competitionType == 1 ? View.INVISIBLE : View.VISIBLE);
    }

    public void checkIntent(Intent intent) {
        if (intent.hasExtra("competition")) {
            Competition competition = (Competition) intent.getSerializableExtra("competition");
            viewListener.setCompetitionDetails(competition);
//            questionType = competition.getQuestionType();
        } else if (intent.hasExtra("competitionId")) {
//            questionType = intent.getExtras().getInt("questionType");
            viewListener.startRequestingDetails();
        }
    }

    private MutableLiveData<List<Gift>> giftsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Gift>> getGifts(List<Gift> gifts) {
        if (gifts == null)
            setEmptyListTextView(View.VISIBLE);
        else if (gifts.isEmpty())
            setEmptyListTextView(View.VISIBLE);
        else {
            setEmptyListTextView(View.GONE);
            giftsMutableLiveData.setValue(gifts);
        }

        return giftsMutableLiveData;
    }

    protected MutableLiveData<Competition> requestCompetitionDetails() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchDetails(getRequestBody(), getRequestHeader(), this);
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : share icon
                |-> 2 : send answer
                |-> 3 : retry fetching competition details
                |-> 4 : like competition
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                // startSharing
                viewListener.startSharing(getShareIntent());
                break;
            case 2:
                // send user answer
                viewListener.startAnswering();
                break;
            case 3:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchDetails(getRequestHeader(), getRequestBody(), this);
                break;
            case 4:
                // like/unLike competition
                viewListener.startLiking();
                break;
            case 5:
                viewListener.showQuestionPhoto();
        }
    }

    private Map<String, String> getRequestBody() {
        Map<String, String> body = new HashMap<>();
        body.put("competion", viewListener.getCompetitionId());

        return body;
    }

    /*
        call model fn to send user answer
        @Param .. id : competition id
        @Param .. isOwner : flag to know if the user is the owner of this question or not
                            0 == false .. 1 == true
        @Param .. questionType : type of the question if it's (normal , photo , video or " not question")
        @Param .. answers : list of competition answers
        @Param .. checkBoxes : list of check boxes which contains the competition answers

        check if user is the owner of the question then check if user checked any answer then make (sendAnswer) req.
     */
    public void requestSendAnswer(String id, int isOwner, int questionType, List<Answer> answers, List<CheckBox> checkBoxes) {
        // if the user is the owner of the question
        if (isOwner == 1) {
            viewListener.showToastMessage(getApplication().getString(R.string.you_can_not_answer_your_own_question));
            return;
        }

        // if question type is not " not question " and user didn't check any answer
        if (questionType != 4 && checkedAnswer.get().intValue() == -1) {
            viewListener.showToastMessage(getApplication().getString(R.string.please_check_your_answer));
            return;
        }

        Map<String, String> body = new HashMap<>();
        // if question type is not " not question " get user answer id
        if (questionType != 4) {
            String answer = "";
            String answerId = "";

            for (CheckBox checkBox : checkBoxes) {
                if (checkBox.getId() == checkedAnswer.get().intValue()) {
                    answer = checkBox.getText().toString();
                    for (Answer answer1 : answers) {
                        if (answer1.getAnswer().equals(answer)) {
                            answerId = answer1.getId();
                            break;
                        }
                    }
                    break;
                }
            }

            body.put("answer", answerId + "");
        }

        body.put("competion", id);
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.answerQuestion(body, getRequestHeader(), this);
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    /*
        call model fn to like/un-like competition
        @Param .. id : competition id
     */
    public void requestLikeUnLike(String id) {
        Map<String, String> body = new HashMap<>();
        body.put("competion", id);

        model.likeUnLike(body, getRequestHeader(), this);
    }

    /*
        handle onFailure of requests
        @Param .. index : flag to know which req.'s failure response is going to be handled
                    |-> 0 : competition details
                    |-> 1 : send answer & like/un-like competition
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(getFailureMessage(t));
        } else {
            viewListener.showToastMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data));
        }

    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }


    /*
        handle sendAnswer & like/unLike  req.'s response
        @Param .. index : flag to know which req. will be handled
                    |-> 0 : sendAnswer
                    |-> 1 : like/unLike
     */
    @Override
    public void handlePostReqResponse(Response<ApiResponse<String>> response, int index) {
        if (response.code() == 200) {
            ApiResponse<String> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                // check which req. is going to be handled
                if (index == 0) {// sending answer req.
                    viewListener.showToastMessage(getApplication().getString(R.string.your_answer_sent_successfully));
                } else {// like/unlike competition
                    viewListener.showToastMessage(apiResponse.getMessage());
                    updateUiAfterLikeOrUnlike();
                }
            } else viewListener.showToastMessage(apiResponse.getMessage());

        } else if (response.code() == 500)
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }

    private void updateUiAfterLikeOrUnlike() {
        if (viewListener.isCompetitionLiked() == 1) // competition was liked and user un-liked it
            viewListener.updateCompetitionLikes(0, -1);
        else
            viewListener.updateCompetitionLikes(1, 1);
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getShareView() {
        return shareView;
    }

    public void setShareView(int shareView) {
        this.shareView.set(shareView);
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getCheckedAnswer() {
        return checkedAnswer;
    }

    public void setCheckedAnswer(int checkedAnswer) {
        this.checkedAnswer.set(checkedAnswer);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {
        void finishActivity();

        void startAnswering();

        void showToastMessage(String message);

        int isCompetitionLiked();

        void updateCompetitionLikes(int isLiked, int increase);

        void startLiking();

        String getCompetitionId();

        void setCompetitionDetails(Competition competition);

        void startRequestingDetails();

        void startSharing(Intent intent);

        void showQuestionPhoto();
    }

}
