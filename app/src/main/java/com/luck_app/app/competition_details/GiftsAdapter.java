package com.luck_app.app.competition_details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.GiftListItemBinding;
import com.luck_app.app.models.Gift;

import java.util.ArrayList;
import java.util.List;

public class GiftsAdapter extends RecyclerView.Adapter<GiftsAdapter.Holder> {
    private List<Gift> gifts;
    private final GiftItemListener listener;

    public GiftsAdapter(GiftItemListener listener) {
        this.listener = listener;
    }

    public void setGifts(List<Gift> gifts) {
        this.gifts = gifts;
        notifyDataSetChanged();
    }

    public interface GiftItemListener {
        void onClick(int position, ArrayList<String> photos);
    }

    @NonNull
    @Override
    public GiftsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((GiftListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.gift_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GiftsAdapter.Holder holder, int position) {
        holder.dataBinding.setGift(gifts.get(position));
    }

    @Override
    public int getItemCount() {
        return gifts != null ? gifts.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GiftListItemBinding dataBinding;

        public Holder(@NonNull GiftListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;

            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ArrayList<String> photos = new ArrayList<>();
            for (Gift gift : gifts) {
                photos.add(gift.getGiftImage());
            }
            listener.onClick(getAdapterPosition(), photos);
        }
    }
}
