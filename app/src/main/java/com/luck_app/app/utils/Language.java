package com.luck_app.app.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

import java.util.Locale;

public class Language {
    public static void setLanguage(Context context, String language) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            context.getResources().updateConfiguration(config, null);
        } else {
            config.locale = locale;
            context.getResources().updateConfiguration(config, null);
        }

        config.setLocale(locale);
        config.setLayoutDirection(locale);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        context.createConfigurationContext(config);
    }
}
