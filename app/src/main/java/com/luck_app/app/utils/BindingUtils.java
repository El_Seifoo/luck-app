package com.luck_app.app.utils;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.constraintlayout.widget.Group;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.exoplayer2.ui.PlayerView;
import com.luck_app.app.R;
import com.luck_app.app.add_competition.AddViewModel;
import com.luck_app.app.competition_details.CompetitionDetailsViewModel;
import com.luck_app.app.models.Answer;
import com.luck_app.app.models.Gift;
import com.luck_app.app.models.Price;
import com.luck_app.app.models.User;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.List;

public class BindingUtils {

    public static String validateCompetitionPrice(Price price, int competitionType) {
        if (price == null) return "";
        if (competitionType == 1) return price.getPrice() + " " + price.getCurrency();
        return price.getPrivatePrice() + " " + price.getCurrency();
    }

    /*
        check the visibility of passwords container in add competition activity
     */
    public static int checkCompetitionType(int competitionType) {
        return competitionType == 1 ? View.GONE : View.VISIBLE;
    }

    /*
        check bio visibility in profile & trend details  view
     */
    public static int bioVisibility(String bio) {
        if (bio == null) return View.GONE;
        if (bio.isEmpty()) return View.GONE;
        return View.VISIBLE;
    }

    /*
        make private list item clickable or not ..
        if one item is checking entered password -> make all items non clickable
        else -> make them clickable
     */
    public static boolean setPrivateItemClickable(int progressVisibility) {
        return progressVisibility == View.GONE;
    }

    public static String convertIntToString(int number) {
        return number + "";
    }

    public static String convertDoubleToString(double price) {
        String priceString = String.valueOf(price);
        return price % 1 == 0 ? priceString.substring(0, priceString.indexOf(".")) : priceString;
    }

    @BindingAdapter({"android:question_type", "android:answers"})
    public static void handleAnswer(CheckBox checkBox, int questionType, List<Answer> answers) {
        if (questionType == 4) {
            checkBox.setVisibility(View.GONE);
            return;
        }

        if (answers == null)
            return;

        if (checkBox.getId() == R.id.first_answer)
            checkBox.setText(answers.get(0).getAnswer());
        else if (checkBox.getId() == R.id.second_answer)
            checkBox.setText(answers.get(1).getAnswer());
        else if (checkBox.getId() == R.id.third_answer)
            checkBox.setText(answers.get(2).getAnswer());
        else if (checkBox.getId() == R.id.fourth_answer)
            checkBox.setText(answers.get(3).getAnswer());
    }

    @BindingAdapter("android:question_type")
    public static void setVisibility(View view, int questionType) {
        if (view instanceof ImageView) {
            if (questionType == 2)
                view.setVisibility(View.VISIBLE);
            else view.setVisibility(View.GONE);
        } else if (view instanceof TextView) {
            if (((TextView) view).getText().toString().equals(view.getContext().getString(R.string.entring_the_draw_without_a_question))) {
                if (questionType == 4)
                    view.setVisibility(View.VISIBLE);
                else view.setVisibility(View.GONE);
            } else {
                if (questionType == 1)
                    view.setVisibility(View.VISIBLE);
                else view.setVisibility(View.GONE);
            }
        } else if (view instanceof PlayerView) {
            if (questionType == 3)
                view.setVisibility(View.VISIBLE);
            else view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("android:winners_details")
    public static void setNotificationWinners(TextView textView, List<User> winners) {
        if (winners == null)
            textView.setVisibility(View.GONE);
        else if (winners.isEmpty())
            textView.setVisibility(View.GONE);
        else {
            textView.setVisibility(View.VISIBLE);
            String data = textView.getContext().getString(R.string.winners_names) + ": \n";
            String userData = "";
            for (User user : winners) {
                userData = "";
                if (user.getUsername() != null) {
                    if (!user.getUsername().isEmpty())
                        userData += user.getUsername() + "\n";
                }

                if (user.getCountryCode() != null)
                    userData += user.getCountryCode();

                if (user.getPhoneNumber() != null)
                    userData += user.getPhoneNumber();

                data += userData + "\n";
            }

            textView.setText(data.trim().equals(textView.getContext().getString(R.string.winners_names)) ? "" : data);
        }
    }

    @BindingAdapter("android:conditions_text")
    public static void setConditions(TextView textView, String conditions) {
        if (conditions != null) {
            if (conditions.isEmpty())
                textView.setText(textView.getContext().getString(R.string.no_conditions));
            else textView.setText(conditions);
        }
    }

    @BindingAdapter("android:gift_name")
    public static void setGift(TextView textView, String giftName) {
        if (giftName == null) return;
        textView.setText(textView.getContext().getString(R.string.gift).concat(": ").concat(giftName));
    }

    @BindingAdapter("android:load_image")
    public static void setSrc(ImageView imageView, String path) {
        Glide.with(imageView.getContext())
                .load(path)
                .thumbnail(0.5f)
                .error(R.mipmap.logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @BindingAdapter("android:load_like_button")
    public static void setSrc(ImageView imageView, int isFavourite) {
        imageView.setImageResource(isFavourite == 0 ? R.drawable.ic_like_icon : R.drawable.ic_liked_icon);
    }

    @BindingAdapter("android:load_competitions_image")
    public static void setSrc(ImageView imageView, List<Gift> images) {
        String path;
        if (images == null)
            path = "";
        else if (images.isEmpty())
            path = "";
        else path = images.get(0).getGiftImage();
        Glide.with(imageView.getContext())
                .load(path)
                .thumbnail(0.5f)
                .error(R.mipmap.logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @BindingAdapter("android:focus_background")
    public static void setBackground(final EditText editText, String fake) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    editText.setBackground(v.getResources().getDrawable(R.drawable.focused_edit_text_background));
                else
                    editText.setBackground(v.getResources().getDrawable(R.drawable.edit_text_background));
            }
        });
    }

    /*
     * handle checking answers check box in "add competition" view
     */
    @BindingAdapter({"android:second_check", "android:third_check", "android:fourth_check", "android:view_model"})
    public static void setChecked(final CheckBox firstCheckBox, final CheckBox secondCheckBox, final CheckBox thirdCheckBox, final CheckBox fourthCheckBox, final AddViewModel viewModel) {
        firstCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    viewModel.setCheckedAnswer(firstCheckBox.getId());
                    secondCheckBox.setChecked(false);
                    thirdCheckBox.setChecked(false);
                    fourthCheckBox.setChecked(false);
                } else {
                    if (secondCheckBox.isChecked())
                        viewModel.setCheckedAnswer(secondCheckBox.getId());
                    else if (thirdCheckBox.isChecked())
                        viewModel.setCheckedAnswer(thirdCheckBox.getId());
                    else if (fourthCheckBox.isChecked())
                        viewModel.setCheckedAnswer(fourthCheckBox.getId());
                    else viewModel.setCheckedAnswer(-1);
                }
            }
        });
    }

    /*
     * handle checking answers check box in answering view
     */
    @BindingAdapter({"android:second_check", "android:third_check", "android:fourth_check", "android:view_model"})
    public static void setAnsweringCheckBoxes(final CheckBox firstCheckBox, final CheckBox secondCheckBox, final CheckBox thirdCheckBox, final CheckBox fourthCheckBox, final CompetitionDetailsViewModel viewModel) {
        firstCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    viewModel.setCheckedAnswer(firstCheckBox.getId());
                    secondCheckBox.setChecked(false);
                    thirdCheckBox.setChecked(false);
                    fourthCheckBox.setChecked(false);
                } else {
                    if (secondCheckBox.isChecked())
                        viewModel.setCheckedAnswer(secondCheckBox.getId());
                    else if (thirdCheckBox.isChecked())
                        viewModel.setCheckedAnswer(thirdCheckBox.getId());
                    else if (fourthCheckBox.isChecked())
                        viewModel.setCheckedAnswer(fourthCheckBox.getId());
                    else viewModel.setCheckedAnswer(-1);
                }
            }
        });
    }

    /*
        set question types list into the spinner
     */
    @BindingAdapter("android:question_type_list")
    public static void setList(Spinner spinner, AddViewModel viewModel) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(spinner.getContext(), R.array.question_type, R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.setQuestionType(position + 1);
                // clear photo/video question
                viewModel.clearMediaQuestion();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @BindingAdapter("android:min_value")
    public static void setMinValue(NumberPicker picker, int minValue) {
        picker.setMinValue(minValue);
    }

    @BindingAdapter("android:max_value")
    public static void setMaxValue(NumberPicker picker, int maxValue) {
        picker.setMaxValue(maxValue);
    }

    @BindingAdapter("android:value")
    public static void setValue(NumberPicker picker, int value) {
        picker.setValue(value);
    }

    @BindingAdapter("android:values")
    public static void setValues(NumberPicker picker, String[] values) {
        picker.setDisplayedValues(values);
    }


    /*
        *( this method used for Months NumberPicker view )*
        watch changing in months to update days list
        if selected months are April, Jun, September or November -> 30 days
        if selected month is Feb -> 28 or 39 days depending on year value ( if year can be divided on 4 so days will be 29 , else days will be 28)
        else (rest of months) -> 31 days
     */
    @BindingAdapter({"android:days_picker", "android:days", "android:current_year"})
    public static void onMonthsPickerValueChanged(NumberPicker monthsPicker, final NumberPicker daysPicker,
                                                  final String[] days, final int year) {
        monthsPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                switch (newVal) {
                    // months with 30 days
                    case 4://April
                    case 6://Jun
                    case 9://September
                    case 11://November
                        if (daysPicker.getValue() == 31)
                            daysPicker.setValue(30);
                        daysPicker.setMaxValue(days.length - 1);
                        break;
                    case 2:// february 28/29 days
                        // check if the selected feb has 28 or 29 days
                        if (year % 4 == 0) {
                            if (daysPicker.getValue() == 31 || daysPicker.getValue() == 30)
                                daysPicker.setValue(29);
                            daysPicker.setMaxValue(days.length - 2);
                        } else {
                            if (daysPicker.getValue() == 31 || daysPicker.getValue() == 30 || daysPicker.getValue() == 29)
                                daysPicker.setValue(28);
                            daysPicker.setMaxValue(days.length - 3);
                        }
                        break;
                    default://rest of months
                        daysPicker.setMaxValue(days.length);
                }
            }
        });
    }

    @BindingAdapter("android:answers_visibility")
    public static void setAnswersVisibility(Group group, int questionType) {
        if (questionType == 4) group.setVisibility(View.GONE);
        else group.setVisibility(View.VISIBLE);
    }

    @BindingAdapter("android:question_visibility")
    public static void setQuestionVisibility(Group group, int questionType) {
        if (questionType == 1) group.setVisibility(View.VISIBLE);
        else group.setVisibility(View.GONE);
    }

    @BindingAdapter("android:upload_visibility")
    public static void setUploadVisibility(Group group, int questionType) {
        if (questionType == 2 || questionType == 3) group.setVisibility(View.VISIBLE);
        else group.setVisibility(View.GONE);
    }

    @BindingAdapter("android:upload_text")
    public static void setText(TextView textView, int questionType) {
        if (questionType == 2)
            textView.setText(textView.getContext().getString(R.string.upload_photo));
        else textView.setText(textView.getContext().getString(R.string.upload_video));
    }

}
