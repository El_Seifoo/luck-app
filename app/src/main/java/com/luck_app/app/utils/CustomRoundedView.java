package com.luck_app.app.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;

import com.luck_app.app.R;

public class CustomRoundedView extends FrameLayout {
    private float topRightRadius, topLeftRadius, bottomRightRadius, bottomLeftRadius;

    public CustomRoundedView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CustomRoundedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CustomRoundedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyle) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomRoundedView, 0, 0);

        //get the default value form the attrs
        setTopLeftRadius(typedArray.getDimension(R.styleable.
                CustomRoundedView_top_left_corner_radius, 0));
        setTopRightRadius(typedArray.getDimension(R.styleable.
                CustomRoundedView_top_right_corner_radius, 0));
        setBottomLeftRadius(typedArray.getDimension(R.styleable.
                CustomRoundedView_bottom_left_corner_radius, 0));
        setBottomRightRadius(typedArray.getDimension(R.styleable.
                CustomRoundedView_bottom_right_corner_radius, 0));

        typedArray.recycle();
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int count = canvas.save();

        final Path path = new Path();

        float[] cornerDimensions = {
                topLeftRadius, topLeftRadius,
                topRightRadius, topRightRadius,
                bottomRightRadius, bottomRightRadius,
                bottomLeftRadius, bottomLeftRadius};

        path.addRoundRect(new RectF(0, 0, canvas.getWidth(), canvas.getHeight())
                , cornerDimensions, Path.Direction.CW);

        canvas.clipPath(path, Region.Op.INTERSECT);
        canvas.clipPath(path);

        super.dispatchDraw(canvas);
        canvas.restoreToCount(count);
    }


    public void setTopRightRadius(float topRightRadius) {
        this.topRightRadius = topRightRadius;
        invalidate();
    }

    public void setTopLeftRadius(float topLeftRadius) {
        this.topLeftRadius = topLeftRadius;
        invalidate();
    }

    public void setBottomRightRadius(float bottomRightRadius) {
        this.bottomRightRadius = bottomRightRadius;
        invalidate();
    }

    public void setBottomLeftRadius(float bottomLeftRadius) {
        this.bottomLeftRadius = bottomLeftRadius;
        invalidate();
    }
}
