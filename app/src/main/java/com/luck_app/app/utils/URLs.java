package com.luck_app.app.utils;

public class URLs {

    public static final String MY_FATOORAH_BASE_URL = "https://api.myfatoorah.com";

    public static final String ROOT = "http://theluckapp.com/api/v1/";

    public static final String COUNTRIES = "country";
    public static final String REGISTER = "register";
    public static final String LOGIN = "login";
    public static final String NEW_COMPETITION = "competions";
    public static final String TREND = "trend";
    public static final String PREVIOUS_COMPETITIONS = "old/competions";
    public static final String PRIVATE_COMPETITIONS = "privet/competions";
    public static final String HOME_SLIDER = "home/slide";
    public static final String TERMS_CONDITIONS = "terms";
    public static final String INFO = "options";
    public static final String PROFILE = "profile";
    public static final String PACKAGES = "packages";
    public static final String NOTIFICATIONS = "notification";
    public static final String ANSWER = "answer/competions";
    public static final String LIKE_UNLIKE = "like";
    public static final String COMPETITION_DETAILS = "competions/details";
    public static final String WINNERS = "winner/data";
    public static final String CREATE_COMPETITION = "competions";
    public static final String COMPETITION_PRICE = "price";
    public static final String UPDATE_PROFILE = "update/profile";
    public static final String CHANGE_PASSWORD = "update/password";
    public static final String CONTACT_US = "contact";
    public static final String BUY_PACKAGE = "credit/package";
}
