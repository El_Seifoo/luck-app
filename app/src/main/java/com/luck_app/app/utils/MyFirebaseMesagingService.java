package com.luck_app.app.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.luck_app.app.R;

import java.util.Random;

public class MyFirebaseMesagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String token) {
//        Log.e("FirebaseMService", "Refreshed token: " + token);
        MySingleton.getInstance(getApplicationContext()).saveStringToSharedPref(Constants.FIREBASE_TOKEN, token);
    }

    private final String CHANNEL_ID = "luck_app";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

//        if (remoteMessage.getData() != null) {
//            Log.e("notificationData", remoteMessage.getData().toString());
//        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence notificationChannelName = "Notification";

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            NotificationChannel notificationChannel;
            notificationChannel = new NotificationChannel(CHANNEL_ID, notificationChannelName, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableVibration(false);
            notificationChannel.setVibrationPattern(new long[]{0, 0, 0, 0});
            notificationChannel.enableLights(false);
            notificationChannel.setSound(notificationSoundUri, audioAttributes);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(notificationSoundUri);


        int notificationId = new Random().nextInt(60000);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }


}
