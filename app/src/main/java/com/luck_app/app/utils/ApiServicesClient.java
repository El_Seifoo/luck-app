package com.luck_app.app.utils;

import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.Country;
import com.luck_app.app.models.LoginResponse;
import com.luck_app.app.models.Info;
import com.luck_app.app.models.NotificationsResponse;
import com.luck_app.app.models.PackageObj;
import com.luck_app.app.models.Price;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.models.ProfileResponse;
import com.luck_app.app.models.Trend;
import com.luck_app.app.models.User;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.models.Winner;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiServicesClient {

    @GET(URLs.COUNTRIES)
    Call<ApiResponse<List<Country>>> fetchCountries(@Header("lang") String lang);

    @POST(URLs.REGISTER)
    Call<ApiResponse<String>> register(@Header("lang") String lang, @Body UserLoginRegister user);

    @POST(URLs.LOGIN)
    Call<ApiResponse<LoginResponse>> login(@Header("lang") String lang, @Body UserLoginRegister user);

    @GET(URLs.NEW_COMPETITION)
    Call<ApiResponse<CompetitionsTrendsResponse<Competition>>> fetchNewCompetitions(@HeaderMap Map<String, String> header);

    @GET(URLs.TREND)
    Call<ApiResponse<CompetitionsTrendsResponse<Trend>>> fetchTrends(@HeaderMap Map<String, String> header);

    @GET(URLs.PREVIOUS_COMPETITIONS)
    Call<ApiResponse<CompetitionsTrendsResponse<Competition>>> fetchPreviousCompetitions(@HeaderMap Map<String, String> header);

    @GET(URLs.PRIVATE_COMPETITIONS)
    Call<ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>>> fetchPrivateCompetitions(@HeaderMap Map<String, String> header);

    @GET(URLs.HOME_SLIDER)
    Call<ApiResponse<List<String>>> fetchHomeSlider(@HeaderMap Map<String, String> header);

    @GET(URLs.TERMS_CONDITIONS)
    Call<ApiResponse<Info>> fetchTermsConditions(@Header("lang") String header);

    @GET(URLs.INFO)
    Call<ApiResponse<Info>> fetchInfo(@Header("lang") String header);

    @GET(URLs.PROFILE)
    Call<ApiResponse<ProfileResponse>> fetchProfileData(@HeaderMap Map<String, String> header);

    @GET(URLs.PROFILE)
    Call<ApiResponse<ProfileResponse>> fetchProfileById(@HeaderMap Map<String, String> header, @Query("id") String userId);

    @GET(URLs.PACKAGES)
    Call<ApiResponse<List<PackageObj>>> fetchPackages(@HeaderMap Map<String, String> header);

    @GET(URLs.NOTIFICATIONS)
    Call<ApiResponse<NotificationsResponse>> fetchNotifications(@HeaderMap Map<String, String> header, @Query("page") int page);

    @POST(URLs.ANSWER)
    Call<ApiResponse<String>> sendAnswer(@HeaderMap Map<String, String> header, @Body Map<String, String> body);

    @POST(URLs.LIKE_UNLIKE)
    Call<ApiResponse<String>> likeUnLike(@HeaderMap Map<String, String> header, @Body Map<String, String> body);

    @POST(URLs.COMPETITION_DETAILS)
    Call<ApiResponse<Competition>> privateCompDetails(@HeaderMap Map<String, String> header, @Body Map<String, String> body);

    @GET(URLs.WINNERS)
    Call<ApiResponse<List<Winner>>> fetchWinners(@HeaderMap Map<String, String> header, @Query("competion") String id);

    @GET(URLs.COMPETITION_PRICE)
    Call<ApiResponse<Price>> fetchCompetitionPrice(@HeaderMap Map<String, String> header);

    @Multipart
    @POST(URLs.CREATE_COMPETITION)
    Call<ApiResponse<Competition>> createCompetition(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> body);

    @Multipart
    @POST(URLs.CREATE_COMPETITION)
    Call<ApiResponse<Competition>> createCompetitionTest(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> body, @Part MultipartBody.Part file);

    @Multipart
    @POST(URLs.CREATE_COMPETITION)
    Call<ApiResponse<Competition>> createCompetition(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> body, @Part MultipartBody.Part file);

    @POST(URLs.UPDATE_PROFILE)
    Call<ApiResponse<User>> updateUser(@HeaderMap Map<String, String> header, @Body Map<String, Object> body);

    @Multipart
    @POST(URLs.UPDATE_PROFILE)
    Call<ApiResponse<User>> updateUser(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> body, @Part MultipartBody.Part photo);

    @POST(URLs.CHANGE_PASSWORD)
    Call<ApiResponse<User>> changePassword(@HeaderMap Map<String, String> header, @Body Map<String, String> body);

    @POST(URLs.CONTACT_US)
    Call<ApiResponse<String>> sendMessage(@HeaderMap Map<String, String> header, @Body Map<String, String> body);

    @GET(URLs.BUY_PACKAGE)
    Call<ApiResponse<String>> buyPackage(@HeaderMap Map<String, String> header, @Query("package_id") String packageId);

}
