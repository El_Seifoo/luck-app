package com.luck_app.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.github.islamkhsh.CardSliderAdapter;
import com.luck_app.app.databinding.HomeSliderItemBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends CardSliderAdapter<SliderAdapter.Holder> {
    private List<String> images;
    private final SliderItemListener listener;

    public void setImages(List<String> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    public SliderAdapter(SliderItemListener listener) {
        this.listener = listener;
    }

    public interface SliderItemListener {
        void onClick(int position, ArrayList<String> photos);
    }

    @Override
    public void bindVH(@NotNull Holder holder, int position) {
        holder.dataBinding.setImage(images.get(position));
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((HomeSliderItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.home_slider_item, parent, false));
    }

    @Override
    public int getItemCount() {
        return images != null ? images.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private HomeSliderItemBinding dataBinding;

        public Holder(@NonNull HomeSliderItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getAdapterPosition(), (ArrayList<String>) images);
        }
    }
}
