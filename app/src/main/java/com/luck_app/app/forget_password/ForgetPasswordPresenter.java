package com.luck_app.app.forget_password;

public interface ForgetPasswordPresenter {
    void onButtonClicked();
}
