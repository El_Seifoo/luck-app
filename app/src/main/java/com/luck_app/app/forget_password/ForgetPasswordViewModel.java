package com.luck_app.app.forget_password;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class ForgetPasswordViewModel extends AndroidViewModel {
    private ViewListener viewListener;

    public ForgetPasswordViewModel(@NonNull Application application) {
        super(application);
    }

    /*
        Handle send code button action
     */
    public void handleButtonsAction() {

    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }// Setter & Getters ----------> END


    protected interface ViewListener {
    }
}
