package com.luck_app.app.forget_password;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityForgetPasswordBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;


public class ForgetPasswordActivity extends AppCompatActivity implements ForgetPasswordViewModel.ViewListener, ForgetPasswordPresenter {
    private ActivityForgetPasswordBinding dataBinding;
    private ForgetPasswordViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_forget_password);
        viewModel = new ViewModelProvider(this).get(ForgetPasswordViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.forget_password_title));
        actionBar.setElevation(0);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    /*
        Handle send code button action
     */
    @Override
    public void onButtonClicked() {
        viewModel.handleButtonsAction();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
