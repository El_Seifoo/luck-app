package com.luck_app.app.verification;

import android.content.Context;
import android.view.View;

import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationModel {
    private Context context;

    public VerificationModel(Context context) {
        this.context = context;
    }

    protected void register(UserLoginRegister user, String lang, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().register(lang, user);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleRegisterResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleRegisterResponse(Response<ApiResponse<String>> response);
    }
}
