package com.luck_app.app.verification;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.luck_app.app.R;
import com.luck_app.app.login.LoginActivity;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import retrofit2.Response;

public class VerificationViewModel extends AndroidViewModel {
    private ViewListener viewListener;
    private VerificationModel model;
    private VerificationModel.ModelCallback modelCallback;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;


    public VerificationViewModel(@NonNull Application application) {
        super(application);
        model = new VerificationModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);

        modelCallback = new VerificationModel.ModelCallback() {
            @Override
            public void setProgress(int progress) {
                VerificationViewModel.this.setProgress(progress);
            }

            @Override
            public void setButtonsClickable(boolean clickable) {
                VerificationViewModel.this.setButtonsClickable(clickable);
            }

            /*
                handle error response of reg. request
             */
            @Override
            public void onFailureHandler(Throwable t) {
                viewListener.showToastMessage(t instanceof IOException ?
                        getApplication().getString(R.string.no_internet_connection) :
                        getApplication().getString(R.string.error_fetching_data));
            }

            /*
                handle register req.'s response
             */
            @Override
            public void handleRegisterResponse(Response<ApiResponse<String>> response) {
                int code = response.code();
                if (code == 200) {
                    ApiResponse<String> registerResponse = response.body();
                    if (registerResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                        Intent intent = new Intent(getApplication(), LoginActivity.class);
                        intent.putExtra("email", viewListener.getEmail());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        viewListener.navigateToDestination(intent);
                    } else {
                        viewListener.showToastMessage(registerResponse.getMessage());
                        viewListener.backToRegister();
                    }
                } else if (code == 500) {
                    viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        viewListener.showToastMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
                    }
                }
            }
        };
    }


    /*
        call firebase method to send otp code to user phone number
     */
    public void startVerifying(PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationStateChangedCallbacks, String phone) {
        phone = phone.replace("+", "");
        phone = "+".concat(phone);

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,
                15,
                TimeUnit.SECONDS,
                viewListener.activity(),
                verificationStateChangedCallbacks
        );
    }

    private boolean isActive = true;

    /*
        call firebase method to check if code is correct or not
        if verification done successfully then call reg. fn to create user
     */
    public void signInWithPhoneCredential(PhoneAuthCredential phoneAuthCredential, UserLoginRegister user) {
        FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    if (isActive) {
                        setProgress(View.VISIBLE);
                        setButtonsClickable(false);
                        model.register(user,
                                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                                modelCallback);
                    }
                } else {
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException)
                        // The verification code entered was invalid
                        viewListener.showToastMessage(getApplication().getString(R.string.invalid_code));

                    viewListener.backToRegister();
                }
            }
        });

    }

    /*
        firebase usually make auto-verification ... send code to user and check the code without letting user send code
        to check if correct or not .. but sometime this doesn't happen so we use this method
        when user write down the code and click verify button ...
        @Param verificationId -> id sent from firebase
        @param otpCode -> code written down by user
     */
    public void handleVerificationButtonAction(String verificationId, String otp, UserLoginRegister user) {
        // that means user does not need to write down his code
        if (verificationId == null) return;

        if (otp.length() < 6) {
            viewListener.showToastMessage(getApplication().getString(R.string.invalid_code));
            return;
        }

        signInWithPhoneCredential(PhoneAuthProvider.getCredential(verificationId, otp), user);
    }

    /*
        handle error message (exception) of sending code to user phone
     */
    public void handleOnVerificationFailed(FirebaseException e) {
        if (e instanceof FirebaseAuthInvalidCredentialsException)
            viewListener.showToastMessage(getApplication().getString(R.string.invalid_phone_format));
        else
            viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));

        viewListener.backToRegister();
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : verify code
                |-> 1 : resend code
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.verificationButtonAction();
                break;
            case 1:
                viewListener.resendCode();
        }

    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }


    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    @Override
    protected void onCleared() {
        super.onCleared();
        isActive = false;
    }

    protected interface ViewListener {
        Activity activity();

        void showToastMessage(String message);

        void verificationButtonAction();

        String getEmail();

        void navigateToDestination(Intent intent);

        void backToRegister();

        void resendCode();
    }
}
