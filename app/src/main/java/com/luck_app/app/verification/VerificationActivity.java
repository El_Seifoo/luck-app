package com.luck_app.app.verification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityVerificationBinding;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

public class VerificationActivity extends AppCompatActivity implements VerificationViewModel.ViewListener, VerificationPresenter {
    private ActivityVerificationBinding dataBinding;
    private VerificationViewModel viewModel;
    private String mVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationStateChangedCallbacks;
    private UserLoginRegister user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_verification);
        viewModel = new ViewModelProvider(this).get(VerificationViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.verify_code));
        actionBar.setElevation(0);
        actionBar.setDisplayHomeAsUpEnabled(true);

        user = (UserLoginRegister) getIntent().getSerializableExtra("user");

        String phone = user.getCountryCode().concat(user.getPhoneNumber());
        phone = phone.replace("+", "");
        phone = "+".concat(phone);

        dataBinding.verificationLabelTwo.append(":\n" + phone);

        verificationStateChangedCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                viewModel.signInWithPhoneCredential(phoneAuthCredential, user);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                viewModel.handleOnVerificationFailed(e);
            }

            @Override
            public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(verificationId, forceResendingToken);
                mVerificationId = verificationId;
            }


        };

        viewModel.startVerifying(verificationStateChangedCallbacks, user.getCountryCode().concat(user.getPhoneNumber()));


    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : verify code
                |-> 1 : resend code
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public Activity activity() {
        return this;
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void verificationButtonAction() {
        viewModel.handleVerificationButtonAction(mVerificationId, dataBinding.otpView.getText().toString(), user);
    }

    @Override
    public String getEmail() {
        return user.getEmail();
    }

    @Override
    public void navigateToDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void backToRegister() {
        finish();
    }

    @Override
    public void resendCode() {
        viewModel.startVerifying(verificationStateChangedCallbacks, user.getCountryCode().concat(user.getPhoneNumber()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (viewModel.getButtonsClickable().get()) {
            if (item.getItemId() == android.R.id.home) {
                finish();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        if (viewModel.getButtonsClickable().get())
            super.onBackPressed();
    }
}
