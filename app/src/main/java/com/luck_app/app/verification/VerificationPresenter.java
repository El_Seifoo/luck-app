package com.luck_app.app.verification;

public interface VerificationPresenter {
    void onButtonClicked(int index);
}
