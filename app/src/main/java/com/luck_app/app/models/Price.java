package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

public class Price {
    @SerializedName("price")
    private double price;
    @SerializedName("privet")
    private double privatePrice;
    @SerializedName("currency")
    private String currency;

    public double getPrice() {
        return price;
    }

    public double getPrivatePrice() {
        return privatePrice;
    }

    public String getCurrency() {
        return currency;
    }
}
