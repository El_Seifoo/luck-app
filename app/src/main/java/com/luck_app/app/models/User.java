package com.luck_app.app.models;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User extends BaseObservable implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("codeCountry")
    private String countryCode;
    @SerializedName("type")
    private String userType;
    @SerializedName("image")
    private String photo;
    @SerializedName("credit")
    private double credit;
    @SerializedName("fcm_token")
    private String firebaseToken;
    @SerializedName("bio")
    private String bio;

    public boolean equals(User user) {
        return this.username.equals(user.getUsername())
                && this.email.equals(user.getEmail())
                && this.phoneNumber.equals(user.getPhoneNumber())
                && this.bio.equals(user.getBio())
                && this.photo.equals(user.getPhoto());
    }

    public User(String id, String username, String email, String phoneNumber, String countryCode, String userType, String photo, double credit, String firebaseToken, String bio) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.countryCode = countryCode;
        this.userType = userType;
        this.photo = photo;
        this.credit = credit;
        this.firebaseToken = firebaseToken;
        this.bio = bio;
    }

    public String getId() {
        return id;
    }

    @Bindable
    public String getUsername() {
        return username;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    @Bindable
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getUserType() {
        return userType;
    }

    @Bindable
    public String getPhoto() {
        return photo;
    }

    public double getCredit() {
        return credit;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    @Bindable
    public String getBio() {
        return bio;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
        notifyPropertyChanged(BR.photo);
    }

    public void setUsername(String username) {
        this.username = username;
        notifyPropertyChanged(BR.username);
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        notifyPropertyChanged(BR.phoneNumber);
    }

    public void setBio(String bio) {
        this.bio = bio;
        notifyPropertyChanged(BR.bio);
    }

    public User Copy() {
        return new User(this.id, this.username, this.email, this.phoneNumber, this.countryCode, this.userType, this.photo, this.credit, this.firebaseToken, this.bio);
    }
}
