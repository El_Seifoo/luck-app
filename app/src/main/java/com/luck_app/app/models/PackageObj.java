package com.luck_app.app.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.annotations.SerializedName;

public class PackageObj{
    @SerializedName("id")
    private String id;
    @SerializedName("package")
    private String packageValue;
    private boolean isSelected;

    public String getId() {
        return id;
    }

    public String getPackageValue() {
        return packageValue;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
