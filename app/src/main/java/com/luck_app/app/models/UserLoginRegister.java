package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserLoginRegister implements Serializable {
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("country")
    private String countryCode;
    @SerializedName("password")
    private String password;
    @SerializedName("password_confirmation")
    private String confirmPassword;
    @SerializedName("privacy")
    private boolean privacy;
    @SerializedName("fcm_token")
    private String firbaseToken;

    public UserLoginRegister(String email, String password, String firbaseToken) {
        this.email = email;
        this.password = password;
        this.firbaseToken = firbaseToken;
    }

    public UserLoginRegister(String username, String email, String phoneNumber, String countryCode, String password) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.countryCode = countryCode;
        this.password = password;
        this.confirmPassword = password;
        this.privacy = true;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getEmail() {
        return email;
    }
}
