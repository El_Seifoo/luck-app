package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsResponse {
    @SerializedName("current_page")
    private int currentPage;
    @SerializedName("last_page")
    private int lastPage;
    @SerializedName("data")
    private List<Notification> notifications;

    public int getCurrentPage() {
        return currentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }
}
