package com.luck_app.app.models;

import android.view.View;

public class PrivateCompetition extends Competition {
    private int progress = View.GONE;
    private String password ;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
