package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompetitionsTrendsResponse<T> {
    @SerializedName(value = "trend", alternate = {"competitions"})
    private List<T> list;
    @SerializedName("slides")
    private List<String> slider;

    public CompetitionsTrendsResponse() {
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public void setSlider(List<String> slider) {
        this.slider = slider;
    }

    public CompetitionsTrendsResponse(List<T> list, List<String> slider) {
        this.list = list;
        this.slider = slider;
    }

    public List<T> getList() {
        return list;
    }

    public List<String> getSlider() {
        return slider;
    }
}
