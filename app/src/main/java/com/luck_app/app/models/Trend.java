package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

public class Trend {
    @SerializedName("id")
    private String id;
    @SerializedName("count")
    private int count;
    @SerializedName("user")
    private User user;

    public String getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public User getUser() {
        return user;
    }
}
