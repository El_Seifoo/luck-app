package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

public class Winner {
    @SerializedName("id")
    private String id;
    @SerializedName("username")
    private String name;


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
