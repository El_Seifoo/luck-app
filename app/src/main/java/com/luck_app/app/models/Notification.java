package com.luck_app.app.models;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("competion")
    private String competitionId;
    @SerializedName("competions")
    private Competition competition;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getCompetitionId() {
        return competitionId;
    }

    public Competition getCompetition() {
        return competition;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }
}
