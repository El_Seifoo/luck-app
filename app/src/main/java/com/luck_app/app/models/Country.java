package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Country implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("code")
    private String code;
    @SerializedName("countryCode")
    private String countryCode;


    public Country(String id, String code, String countryCode) {
        this.id = id;
        this.code = code;
        this.countryCode = countryCode;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
