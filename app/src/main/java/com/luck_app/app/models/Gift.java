package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Gift implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("gift")
    private String giftImage;

    public Gift(String id, String giftImage) {
        this.id = id;
        this.giftImage = giftImage;
    }

    public String getId() {
        return id;
    }

    public String getGiftImage() {
        return giftImage;
    }
}
