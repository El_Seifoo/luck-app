package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

public class Info {
    @SerializedName(value = "terms" ,alternate = "about")
    private String data;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("whatsNumber")
    private String whatsNumber;

    public String getData() {
        return data;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWhatsNumber() {
        return whatsNumber;
    }
}
