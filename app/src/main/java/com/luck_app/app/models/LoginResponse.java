package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @Override
    public String toString() {
        return "LoginResponse{" +
                "user=" + user +
                ", token='" + token + '\'' +
                ", tokenType='" + tokenType + '\'' +
                '}';
    }

    @SerializedName("user")
    private User user;
    @SerializedName("access_token")
    private String token;
    @SerializedName("token_type")
    private String tokenType;

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public String getTokenType() {
        return tokenType;
    }
}
