package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Competition implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("name")
    private String name;
    @SerializedName("winner")
    private int numberOfWinners;
    @SerializedName("giftName")
    private String giftName;
    @SerializedName("type")
    private int questionType;
    @SerializedName("question")
    private String question;
    @SerializedName("conditions")
    private String conditions;
    @SerializedName("competionType")
    private int competitionType;
    @SerializedName("active")
    private int active;
    @SerializedName("trend")
    private int trend;
    @SerializedName("tasbeet")
    private int tasbeet;
    @SerializedName("RegisteredCount")
    private int registeredCount;
    @SerializedName("isRegistered")
    private int isRegistered;
    @SerializedName("isOwner")
    private int isOwner;
    @SerializedName("isLike")
    private int isLike;
    @SerializedName(value = "like_count", alternate = "likeCount")
    private int likesCount;
    @SerializedName("user")
    private User user;
    @SerializedName("answers")
    private List<Answer> answers;
    @SerializedName("gift")
    private List<Gift> gifts;
    @SerializedName("winners")
    private List<User> winners;
    @SerializedName("start")
    private String startDate;


    @Override
    public String toString() {
        return "Competition{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", numberOfWinners=" + numberOfWinners +
                ", giftName='" + giftName + '\'' +
                ", questionType=" + questionType +
                ", question='" + question + '\'' +
                ", conditions='" + conditions + '\'' +
                ", competitionType=" + competitionType +
                ", active=" + active +
                ", trend=" + trend +
                ", tasbeet=" + tasbeet +
                ", registeredCount=" + registeredCount +
                ", isRegistered=" + isRegistered +
                ", isOwner=" + isOwner +
                ", isLike=" + isLike +
                ", likesCount=" + likesCount +
                ", user=" + user +
                ", answers=" + answers +
                ", gifts=" + gifts +
                '}';
    }

    public void setData(Competition competition) {
        this.id = competition.getId();
        this.userId = competition.getUserId();
        this.name = competition.getName();
        this.numberOfWinners = competition.getNumberOfWinners();
        this.giftName = competition.getGiftName();
        this.questionType = competition.getQuestionType();
        this.question = competition.getQuestion();
        this.conditions = competition.getConditions();
        this.competitionType = competition.getCompetitionType();
        this.active = competition.getActive();
        this.trend = competition.getTrend();
        this.tasbeet = competition.getTasbeet();
        this.registeredCount = competition.getRegisteredCount();
        this.isRegistered = competition.getIsRegistered();
        this.isOwner = competition.getIsOwner();
        this.isLike = competition.getIsLike();
        this.likesCount = competition.getLikesCount();
        this.user = competition.getUser();
        this.answers = competition.getAnswers();
        this.gifts = competition.getGifts();
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfWinners() {
        return numberOfWinners;
    }

    public String getGiftName() {
        return giftName;
    }

    public int getQuestionType() {
        return questionType;
    }

    public String getQuestion() {
        return question;
    }

    public String getConditions() {
        return conditions;
    }

    public int getCompetitionType() {
        return competitionType;
    }

    public int getActive() {
        return active;
    }

    public int getTrend() {
        return trend;
    }

    public int getTasbeet() {
        return tasbeet;
    }

    public int getRegisteredCount() {
        return registeredCount;
    }

    public int getIsRegistered() {
        return isRegistered;
    }

    public int getIsOwner() {
        return isOwner;
    }

    public int getIsLike() {
        return isLike;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public User getUser() {
        return user;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public List<Gift> getGifts() {
        return gifts;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public void increaseLikes(int count) {
        likesCount = likesCount + count;
    }

    public List<User> getWinners() {
        return winners;
    }

    public String getStartDate() {
        return startDate;
    }
}
