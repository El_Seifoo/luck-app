package com.luck_app.app.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileResponse extends BaseObservable {
    @SerializedName("user")
    private User user;
    @SerializedName("competition")
    private List<Competition> MyCompetitions;
    @SerializedName("history")
    private List<Competition> history;

    @Bindable
    public User getUser() {
        return user;
    }

    public List<Competition> getMyCompetitions() {
        return MyCompetitions;
    }

    public List<Competition> getHistory() {
        return history;
    }

    public void setUser(User user) {
        this.user = user;
        notifyPropertyChanged(BR.user);
    }
}
