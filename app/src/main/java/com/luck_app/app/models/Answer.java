package com.luck_app.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Answer implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("answer")
    private String answer;
    @SerializedName("right")
    private int rightAnswer;


    public String getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public int getRightAnswer() {
        return rightAnswer;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setRightAnswer(int rightAnswer) {
        this.rightAnswer = rightAnswer;
    }
}
