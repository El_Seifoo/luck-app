package com.luck_app.app.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.luck_app.app.R;
import com.luck_app.app.language.LanguageActivity;
import com.luck_app.app.main.MainActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.luck_app.app.utils.URLs;
import com.myfatoorah.sdk.views.MFSDK;

public class SplashActivity extends AppCompatActivity {
    private static final int DELAY_TIME = 3000;
    private Handler handler;
    private Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        setContentView(R.layout.activity_splash);


        // initialize myfatoorah sdk
        MFSDK.INSTANCE.init(URLs.MY_FATOORAH_BASE_URL, Constants.MY_FATOORAH_TOKEN);
        //for test
//        MFSDK.INSTANCE.init("https://apitest.myfatoorah.com/", "7Fs7eBv21F5xAocdPvvJ-sCqEyNHq4cygJrQUFvFiWEexBUPs4AkeLQxH4pzsUrY3Rays7GVA6SojFCz2DMLXSJVqk8NG-plK-cZJetwWjgwLPub_9tQQohWLgJ0q2invJ5C5Imt2ket_-JAlBYLLcnqp_WmOfZkBEWuURsBVirpNQecvpedgeCx4VaFae4qWDI_uKRV1829KCBEH84u6LYUxh8W_BYqkzXJYt99OlHTXHegd91PLT-tawBwuIly46nwbAs5Nt7HFOozxkyPp8BW9URlQW1fE4R_40BXzEuVkzK3WAOdpR92IkV94K_rDZCPltGSvWXtqJbnCpUB6iUIn1V-Ki15FAwh_nsfSmt_NQZ3rQuvyQ9B3yLCQ1ZO_MGSYDYVO26dyXbElspKxQwuNRot9hi3FIbXylV3iN40-nCPH4YQzKjo5p_fuaKhvRh7H8oFjRXtPtLQQUIDxk-jMbOp7gXIsdz02DrCfQIihT4evZuWA6YShl6g8fnAqCy8qRBf_eLDnA9w-nBh4Bq53b1kdhnExz0CMyUjQ43UO3uhMkBomJTXbmfAAHP8dZZao6W8a34OktNQmPTbOHXrtxf6DS-oKOu3l79uX_ihbL8ELT40VjIW3MJeZ_-auCPOjpE3Ax4dzUkSDLCljitmzMagH2X8jN8-AYLl46KcfkBV");

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (!MySingleton.getInstance(getApplicationContext()).isLoggedIn()) {
                    // navigate to lang. act. then sign option act.
                    navigateDestination(LanguageActivity.class, false);
                } else {
                    if (MySingleton.getInstance(getApplicationContext()).getBooleanFromSharedPref(Constants.IS_REMEMBERED, false)) {
                        // navigate to home act.
                        navigateDestination(MainActivity.class, false);
                    } else {
                        // navigate to lang. act. then sign option act.
                        navigateDestination(LanguageActivity.class, true);
                    }
                }
            }
        };
    }

    private void navigateDestination(Class destination, boolean clearSharedPrefData) {
        if (clearSharedPrefData)
            MySingleton.getInstance(getApplicationContext()).logout();

        Intent intent = new Intent(getApplicationContext(), destination);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(runnable, DELAY_TIME);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}
