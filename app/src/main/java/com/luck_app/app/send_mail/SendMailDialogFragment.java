package com.luck_app.app.send_mail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.luck_app.app.R;
import com.luck_app.app.databinding.SendMailDialogFragmentLayoutBinding;

public class SendMailDialogFragment extends DialogFragment implements SendMailViewModel.ViewListener, SendMailPresenter {
    private SendMailDialogFragmentLayoutBinding dataBinding;
    private SendMailViewModel viewModel;

    public static SendMailDialogFragment newInstance(String competitionId) {
        SendMailDialogFragment fragment = new SendMailDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("competitionId", competitionId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.send_mail_dialog_fragment_layout, container, false);
        viewModel = new ViewModelProvider(this).get(SendMailViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        return dataBinding.getRoot();
    }

    @Override
    public void onSendButtonClicked() {
        viewModel.requestSendMessage(dataBinding.subjectEditText, dataBinding.messageEditText, getArguments().getString("competitionId"));
    }

    @Override
    public void showEditTextError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void sendMailDone(String message) {
        showToastMessage(message);
        dismiss();
    }
}
