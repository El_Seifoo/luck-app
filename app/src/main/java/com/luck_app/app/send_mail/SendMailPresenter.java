package com.luck_app.app.send_mail;

public interface SendMailPresenter {
    void onSendButtonClicked();
}
