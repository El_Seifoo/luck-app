package com.luck_app.app.send_mail;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class SendMailViewModel extends AndroidViewModel implements SendMailModel.ModelCallback {
    private SendMailModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    protected ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;

    public SendMailViewModel(@NonNull Application application) {
        super(application);
        model = new SendMailModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        Validate all user inputs
        then call model fn. to send message .
     */
    protected void requestSendMessage(EditText subjectEditText, EditText messageEditText, String competitionId) {
        String subject = subjectEditText.getText().toString().trim();
        if (subject.isEmpty()) {
            viewListener.showEditTextError(subjectEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String message = messageEditText.getText().toString().trim();
        if (message.isEmpty()) {
            viewListener.showEditTextError(messageEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));

        Map<String, String> body = new HashMap<>();
        User user = MySingleton.getInstance(getApplication()).userData();
        body.put("subject", subject);
        body.put("massages", message);
        body.put("name", user.getUsername());
        body.put("phone", user.getCountryCode().concat(user.getPhoneNumber()));
        body.put("email", user.getEmail());
        if (!competitionId.isEmpty())
            body.put("competion_id", competitionId);

        model.sendMessage(header, body, this);
    }


    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    @Override
    public void handleSendingMessageResponse(Response<ApiResponse<String>> response) {
        int code = response.code();
        if (code == 200) {
            ApiResponse<String> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                viewListener.sendMailDone(getApplication().getString(R.string.ur_message_sent_successfully));
            } else viewListener.showToastMessage(apiResponse.getMessage());
        } else if (code == 500) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        } else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {

        void showEditTextError(EditText editText, String message);

        void showToastMessage(String message);

        void sendMailDone(String message);
    }
}
