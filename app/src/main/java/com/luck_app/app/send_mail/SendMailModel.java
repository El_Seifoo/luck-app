package com.luck_app.app.send_mail;

import android.content.Context;
import android.view.View;

import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMailModel {
    private Context context;

    public SendMailModel(Context context) {
        this.context = context;
    }

    protected void sendMessage(Map<String, String> header, Map<String, String> body, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().sendMessage(header, body);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleSendingMessageResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleSendingMessageResponse(Response<ApiResponse<String>> response);
    }
}
