package com.luck_app.app.notifications;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.models.Notification;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationsViewModel extends AndroidViewModel implements NotificationModel.ModelCallback {
    private NotificationModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> loadMoreProgress/* flag to know the status of small progress dialog (load more)*/;
    private ObservableField<Integer> emptyListTextView /* flag to know if competitions list is empty or not */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<Integer> currentPage;
    private ObservableField<Integer> lastPage;

    public NotificationsViewModel(@NonNull Application application) {
        super(application);
        model = new NotificationModel(application);
        progress = new ObservableField<>(View.GONE);
        loadMoreProgress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        currentPage = new ObservableField<>(0);
        lastPage = new ObservableField<>(-1);
    }

    /*
        Call model fun. fetchNotifications to get notifications
        show loader then make req.
     */
    protected MutableLiveData<List<Notification>> requestNotifications() {
        // check if user at the last page or not
        if (currentPage.get().intValue() != lastPage.get().intValue()) {// user at the last page
            int page = currentPage.get().intValue();
            // check if user is loading the first page or not
            if (page == 0)// first page -> view the main progress
                setProgress(View.VISIBLE);
            else // the rest pages -> view the small progress
                setLoadMoreProgress(View.VISIBLE);

            return model.fetchNotifications(getRequestHeader(), ++page, this);
        }
        return new MutableLiveData<List<Notification>>(new ArrayList<Notification>());
    }

    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            int pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            int total = recyclerView.getAdapter().getItemCount();
            if (progress.get().intValue() == View.GONE || loadMoreProgress.get().intValue() == View.GONE) {
                if (pastVisibleItem >= total - 1) {
                    this.requestNotifications();
                }
            }
        }
    }

    public void checkSetOrUpdateNotifications(List<Notification> notifications) {
        if (currentPage.get().intValue() == 1)
            viewListener.setNotifications(notifications);
        else viewListener.updateNotifications(notifications);
    }


    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t, int page) {
        if (page == 1) {
            setErrorView(View.VISIBLE);
            setErrorMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data));
        } else viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));

    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message, int page) {
        if (page == 1) {
            setErrorView(View.VISIBLE);
            setErrorMessage(message);
        } else viewListener.showToastMessage(message);

    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : retry fetching notifications request
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                setErrorView(View.GONE);
//                setProgress(View.VISIBLE);
                this.requestNotifications();
//                model.fetchNotifications(getRequestHeader(), currentPage.get().intValue(), this);

        }
    }


    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }


    public ObservableField<Integer> getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setCurrentPage(int currentPage) {
        this.currentPage.set(currentPage);
    }

    public ObservableField<Integer> getLastPage() {
        return lastPage;
    }

    @Override
    public void setLastPage(int lastPage) {
        this.lastPage.set(lastPage);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getLoadMoreProgress() {
        return loadMoreProgress;
    }

    @Override
    public void setLoadMoreProgress(int progress) {
        this.loadMoreProgress.set(progress);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {
        void finishActivity();

        void showToastMessage(String message);

        void setNotifications(List<Notification> notifications);

        void updateNotifications(List<Notification> notifications);
    }
}
