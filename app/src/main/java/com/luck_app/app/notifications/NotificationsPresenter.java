package com.luck_app.app.notifications;

public interface NotificationsPresenter {
    void onButtonClicked(int index);
}
