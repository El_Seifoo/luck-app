package com.luck_app.app.notifications;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Notification;
import com.luck_app.app.models.NotificationsResponse;
import com.luck_app.app.utils.MySingleton;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationModel {
    private Context context;

    public NotificationModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<Notification>> notificationsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Notification>> fetchNotifications(Map<String, String> header, final int page, final ModelCallback callback) {
        Call<ApiResponse<NotificationsResponse>> call = MySingleton.getInstance(context).createService().fetchNotifications(header, page);

        call.enqueue(new Callback<ApiResponse<NotificationsResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<NotificationsResponse>> call, Response<ApiResponse<NotificationsResponse>> response) {
                if (page == 1)
                    callback.setProgress(View.GONE);
                else callback.setLoadMoreProgress(View.GONE);
                if (response.code() == 200) {
                    ApiResponse<NotificationsResponse> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        NotificationsResponse notificationsResponse = apiResponse.getData();
                        callback.setCurrentPage(notificationsResponse.getCurrentPage());
                        callback.setLastPage(notificationsResponse.getLastPage());
                        if (notificationsResponse != null) {
                            List<Notification> notifications = notificationsResponse.getNotifications();
                            if (notifications == null) {
                                if (page == 1)
                                    callback.setEmptyListTextView(View.VISIBLE);
                            } else if (notifications.isEmpty()) {
                                if (page == 1)
                                    callback.setEmptyListTextView(View.VISIBLE);
                            } else {
                                callback.setEmptyListTextView(View.GONE);
                                notificationsMutableLiveData.setValue(notifications);
                            }

                        } else callback.showResponseMessage(apiResponse.getMessage(), page);
                    } else callback.showResponseMessage(apiResponse.getMessage(), page);
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data), page);
            }

            @Override
            public void onFailure(Call<ApiResponse<NotificationsResponse>> call, Throwable t) {
                if (page == 1)
                    callback.setProgress(View.GONE);
                else callback.setLoadMoreProgress(View.GONE);

                callback.onFailureHandler(t, page);
            }
        });


        return notificationsMutableLiveData;
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setLoadMoreProgress(int progress);

        void setCurrentPage(int currentPage);

        void setLastPage(int lastPage);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t, int page);

        void showResponseMessage(String message, int page);
    }
}
