package com.luck_app.app.notifications;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityNotificationsBinding;
import com.luck_app.app.models.Notification;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.util.List;

public class  NotificationsActivity extends AppCompatActivity implements NotificationsViewModel.ViewListener, NotificationsPresenter {
    private ActivityNotificationsBinding dataBinding;
    private NotificationsViewModel viewModel;
    private NotificationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_notifications);
        viewModel = new ViewModelProvider(this).get(NotificationsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        final RecyclerView notificationList = dataBinding.notificationsList;
        notificationList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapter = new NotificationAdapter();
        notificationList.setAdapter(adapter);

        notificationList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                viewModel.onScrolled(recyclerView, dx, dy);
            }
        });

        viewModel.requestNotifications().observe(this, new Observer<List<Notification>>() {
            @Override
            public void onChanged(List<Notification> notifications) {
                viewModel.checkSetOrUpdateNotifications(notifications);
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : retry fetching notifications request
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setNotifications(List<Notification> notifications) {
        adapter.setNotifications(notifications);
    }

    @Override
    public void updateNotifications(List<Notification> notifications) {
        adapter.addNotifications(notifications);
    }
}
