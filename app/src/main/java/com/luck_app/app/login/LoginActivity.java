package com.luck_app.app.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityLoginBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;


public class LoginActivity extends AppCompatActivity implements LoginPresenter, LoginViewModel.ViewListener {
    private ActivityLoginBinding dataBinding;
    private LoginViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.login));
        actionBar.setElevation(0);

        viewModel.checkIntent(getIntent());
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : login button
                |-> 1 : forget password
                |-> 2 : sign-up
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public void navigateDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showEditTextError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startLogging() {
        viewModel.requestLogin(dataBinding.emailEditText, dataBinding.passwordEditText, dataBinding.rememberAccountCheckBox.isChecked());
    }

    @Override
    public void setEmail(String email) {
        dataBinding.emailEditText.setText(email);
    }


}
