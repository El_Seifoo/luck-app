package com.luck_app.app.login;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.luck_app.app.R;
import com.luck_app.app.forget_password.ForgetPasswordActivity;
import com.luck_app.app.main.MainActivity;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.LoginResponse;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.register.RegisterActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;


public class LoginViewModel extends AndroidViewModel implements LoginModel.ModelCallback {
    private LoginModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    protected ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        model = new LoginModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        generateFirebaseToken();
    }

    private void generateFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//                            Log.e("LoginActivity", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.FIREBASE_TOKEN, token);
                    }
                });
    }


    public void checkIntent(Intent intent) {
        if (intent.hasExtra("email"))
            viewListener.setEmail(intent.getExtras().getString("email"));
    }


    /*
        validate all user inputs ..
        then call model method to make login req.
     */
    protected void requestLogin(EditText emailEditText, EditText passwordEditText, boolean rememberMe) {
        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (rememberMe)
            MySingleton.getInstance(getApplication()).saveBooleanToSharedPref(Constants.IS_REMEMBERED, true);

        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        String firebaseToken = MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.FIREBASE_TOKEN, "");
        if (firebaseToken.isEmpty())
            generateFirebaseToken();
        firebaseToken = MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.FIREBASE_TOKEN, "");
        model.login(new UserLoginRegister(email, password, firebaseToken),
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                this);
    }


    /*
        check email validation using RE
     */
    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }


    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle login req.'s response
        @Param code : response code
     */
    @Override
    public void handleLoginResponse(Response<ApiResponse<LoginResponse>> response, int code) {
        if (code == 200) {
            ApiResponse<LoginResponse> loginResponse = response.body();
            if (loginResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {

                FirebaseMessaging.getInstance().subscribeToTopic("LuckApp");
                LoginResponse loginData = loginResponse.getData();
                MySingleton.getInstance(getApplication()).saveUserData(loginData.getUser());
                MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.USER_TOKEN, loginData.getTokenType() + " " + loginData.getToken());

                // if user check "remember me" save that there is user logged in
                // else don't
                if (MySingleton.getInstance(getApplication()).getBooleanFromSharedPref(Constants.IS_REMEMBERED, false))
                    MySingleton.getInstance(getApplication()).loginUser();

                Intent intent = new Intent(getApplication(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                viewListener.navigateDestination(intent);
            } else viewListener.showToastMessage(loginResponse.getMessage());
        } else if (code == 500) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        } else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : login button
                |-> 1 : forget password
                |-> 2 : sign-up
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.startLogging();
                break;
            case 1:
//                Intent intent = new Intent(getApplication(), ForgetPasswordActivity.class);
//                viewListener.navigateDestination(intent);
                break;
            case 2:
                Intent intent1 = new Intent(getApplication(), RegisterActivity.class);
                viewListener.navigateDestination(intent1);
                break;
        }
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {
        void navigateDestination(Intent intent);

        void showEditTextError(EditText editText, String message);

        void showToastMessage(String message);

        void startLogging();

        void setEmail(String email);
    }
}
