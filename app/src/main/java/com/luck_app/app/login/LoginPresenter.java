package com.luck_app.app.login;

public interface LoginPresenter {
    void onButtonClicked(int index);
}
