package com.luck_app.app.login;

import android.content.Context;
import android.view.View;

import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.LoginResponse;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel {
    private Context context;

    public LoginModel(Context context) {
        this.context = context;
    }

    protected void login(UserLoginRegister user, String lang, final ModelCallback callback) {
        Call<ApiResponse<LoginResponse>> call = MySingleton.getInstance(context).createService().login(lang, user);

        call.enqueue(new Callback<ApiResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<LoginResponse>> call, Response<ApiResponse<LoginResponse>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleLoginResponse(response, response.code());
            }

            @Override
            public void onFailure(Call<ApiResponse<LoginResponse>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleLoginResponse(Response<ApiResponse<LoginResponse>> response, int code);
    }
}
