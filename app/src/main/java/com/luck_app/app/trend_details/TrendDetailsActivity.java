package com.luck_app.app.trend_details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.luck_app.app.ProfileCompetitionsAdapter;
import com.luck_app.app.R;
import com.luck_app.app.competition_details.CompetitionDetailsActivity;
import com.luck_app.app.databinding.ActivityTrendDetailsBinding;
import com.luck_app.app.main.profile.ProfileAdapter;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.models.ProfileResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.previous_competition.PreviousAdapter;
import com.luck_app.app.previous_competition_details.PrevCompDetailsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TrendDetailsActivity extends AppCompatActivity implements TrendDetailsPresenter, TrendDetailsViewModel.ViewListener, ProfileCompetitionsAdapter.CompetitionItemListener, ProfileCompetitionsAdapter.PrivateCompetitionItemListener {
    private ActivityTrendDetailsBinding dataBinding;
    private TrendDetailsViewModel viewModel;
    private ProfileCompetitionsAdapter competitionsAdapter, historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_trend_details);
        viewModel = new ViewModelProvider(this).get(TrendDetailsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        RecyclerView myCompetitionsList = dataBinding.competitionsList;
        myCompetitionsList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        competitionsAdapter = new ProfileCompetitionsAdapter(this, this, false);
        myCompetitionsList.setAdapter(competitionsAdapter);

        RecyclerView historyList = dataBinding.historyList;
        historyList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        historyAdapter = new ProfileCompetitionsAdapter(this, this, true);
        historyList.setAdapter(historyAdapter);


        viewModel.requestProfile(getUserId()).observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse profileResponse) {
                dataBinding.setUser(profileResponse.getUser());
                List<Competition> competitions = profileResponse.getMyCompetitions();

                ArrayList<Object> adapterList = new ArrayList<>();

                PrivateCompetition privateComp;
                Competition competition1;

                for (int i = 0; i < competitions.size(); i++) {
                    if (competitions.get(i).getCompetitionType() == 1) {
                        adapterList.add(competitions.get(i));
                    } else {
                        competition1 = new PrivateCompetition();
                        competition1.setData(competitions.get(i));
                        privateComp = (PrivateCompetition) competition1;
                        privateComp.setProgress(View.GONE);
                        adapterList.add(privateComp);
                    }
                }

                competitionsAdapter.setCompetitions(adapterList);

                ArrayList<Object> historyList = new ArrayList<>();
                List<Competition> historyCompetitions = profileResponse.getHistory();
                for (int i = 0; i < historyCompetitions.size(); i++) {
                    if (historyCompetitions.get(i).getCompetitionType() == 1) {
                        historyList.add(historyCompetitions.get(i));
                    } else {
                        competition1 = new PrivateCompetition();
                        competition1.setData(historyCompetitions.get(i));
                        privateComp = (PrivateCompetition) competition1;
                        privateComp.setProgress(View.GONE);
                        historyList.add(privateComp);
                    }
                }

                historyAdapter.setCompetitions(historyList);
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : retry fetching user data
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public String getUserId() {
        return getIntent().getExtras().getString("userId");
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCompetitionDetails(Competition competition) {
        long endLong = 0l;
        long currentLong = 0l;
        try {
            endLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(competition.getStartDate()).getTime() + 86400000;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date today = Calendar.getInstance().getTime();
            currentLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dateFormat.format(today)).getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            Intent intent = null;
            if (currentLong >= endLong) //prev
                intent = new Intent(getApplicationContext(), PrevCompDetailsActivity.class);
            else
                intent = new Intent(getApplicationContext(), CompetitionDetailsActivity.class);

            intent.putExtra("competition", competition);
            startActivity(intent);
        }
    }

    @Override
    public void setPrivateItemProgress(int index, int progress, boolean isHistory) {
        if (isHistory)
            historyAdapter.setProgressState(index, progress);
        else
            competitionsAdapter.setProgressState(index, progress);

    }

    @Override
    public int getSelectedIndex(boolean isHistory) {
        return isHistory ? historyAdapter.getSelectedIndex() : competitionsAdapter.getSelectedIndex();
    }

    @Override
    public void showProfilePicture() {
        String photo = dataBinding.getUser().getPhoto();
        if (photo != null) {
            if (!photo.isEmpty()) {
                ArrayList<String> photos = new ArrayList<>();
                photos.add(photo);

                new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
                    @Override
                    public void loadImage(ImageView imageView, String image) {
                        Glide.with(imageView.getContext())
                                .load(image)
                                .thumbnail(0.5f)
                                .error(R.mipmap.ic_launcher)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    }
                })
                        .withStartPosition(0)
                        .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                        .allowSwipeToDismiss(true)
                        .show();

            }
        }
    }

    @Override
    public void onPrivateCompetitionClicked(Competition competition, String password, int index, boolean isHistory) {
        viewModel.requestDetails(competition.getId(), isHistory, password, index);
    }

    @Override
    public void onCompetitionClicked(Competition competition) {
        // if active == 0 that means competition is prev. so send competition object in intent and navigate to prevCompDetailsActivity
        // if active == 1 that means competition is new so send competitionId and questionType in intent and navigate to CompetitionDetailsActivity
        Intent intent = new Intent(getApplicationContext(), competition.getActive() == 0 ? PrevCompDetailsActivity.class : CompetitionDetailsActivity.class);
        if (competition.getActive() == 0)
            intent.putExtra("competition", competition);
        else {
            intent.putExtra("competitionId", competition.getId());
            intent.putExtra("questionType", competition.getQuestionType());
        }
        startActivity(intent);
    }
}
