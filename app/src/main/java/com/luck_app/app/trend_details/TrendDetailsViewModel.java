package com.luck_app.app.trend_details;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.ProfileResponse;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class TrendDetailsViewModel extends AndroidViewModel implements TrendDetailsModel.ModelCallback {
    private TrendDetailsModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyCompetitionsListTextView /* flag to know if competitions list is empty or not */;
    private ObservableField<Integer> emptyHistoryListTextView /* flag to know if history list is empty or not */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private boolean isDetailsLoading;

    public TrendDetailsViewModel(@NonNull Application application) {
        super(application);
        model = new TrendDetailsModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyCompetitionsListTextView = new ObservableField<>(View.GONE);
        emptyHistoryListTextView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
    }

    /*
        Call model fun. fetchProfile to get profile data
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<ProfileResponse> requestProfile(String id) {
        setProgress(View.VISIBLE);
        return model.fetchProfile(id, getRequestHeader(), this);
    }

    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(getFailureMessage(t));
    }

    /*
        handle error response of requests
        requesting details
     */
    @Override
    public void onFailureHandler(Throwable t, boolean isHistory) {
        isDetailsLoading = false;
        viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.GONE, isHistory);
        viewListener.showToastMessage(getFailureMessage(t));

    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }


    protected void requestDetails(String id, boolean isHistory, String password, int index) {
        if (isDetailsLoading) return;
        if (password.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_length_error));
            return;
        }

        isDetailsLoading = true;
        viewListener.setPrivateItemProgress(index, View.VISIBLE, isHistory);
        Map<String, String> body = new HashMap<>();
        body.put("competion", id);
        body.put("password", password);

        model.fetchDetails(body, getRequestHeader(), isHistory, this);
    }

    @Override
    public void handleRequestingDetailsResponse(Response<ApiResponse<Competition>> response, boolean isHistory) {
        isDetailsLoading = false;
        viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.VISIBLE, isHistory);
        int code = response.code();
        if (code == 200) {
            ApiResponse<Competition> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                Competition competition = apiResponse.getData();
                if (competition != null) {
                    viewListener.setCompetitionDetails(competition);
                    isDetailsLoading = false;
                    viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.GONE, isHistory);
                } else
                    this.showResponseMessage(apiResponse.getMessage(), isHistory);

            } else
                this.showResponseMessage(apiResponse.getMessage(), isHistory);
        } else if (code == 500)
            this.showResponseMessage(getApplication().getString(R.string.error_fetching_data), isHistory);
        else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                this.showResponseMessage(jsonObject.getString("message"), isHistory);
            } catch (Exception e) {
                this.showResponseMessage(getApplication().getString(R.string.error_sending_data), isHistory);
            }
        }
    }

    /*
        show response message
     */
    public void showResponseMessage(String message, boolean isHistory) {
        isDetailsLoading = false;
        viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.GONE, isHistory);
        viewListener.showToastMessage(message);
    }

    /*
       handle action of every button in the activity
       @Param index .. flag to know which button was clicked
               |-> 0 : back icon
               |-> 1 : retry fetching user data
    */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);

                model.fetchProfile(viewListener.getUserId(), getRequestHeader(), this);
                break;
            case 5:
                viewListener.showProfilePicture();
        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getEmptyCompetitionsListTextView() {
        return emptyCompetitionsListTextView;
    }

    @Override
    public void setEmptyCompetitionsListTextView(int empty) {
        this.emptyCompetitionsListTextView.set(empty);
    }

    public ObservableField<Integer> getEmptyHistoryListTextView() {
        return emptyHistoryListTextView;
    }

    @Override
    public void setEmptyHistoryListTextView(int empty) {
        this.emptyHistoryListTextView.set(empty);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }


    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {
        void finishActivity();

        String getUserId();

        void showToastMessage(String message);

        void setCompetitionDetails(Competition competition);

        void setPrivateItemProgress(int index, int progress, boolean isHistory);

        int getSelectedIndex(boolean isHistory);

        void showProfilePicture();
    }
}
