package com.luck_app.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.databinding.CompetitionListItemBinding;
import com.luck_app.app.databinding.PrivateCompetitionsListItemBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.PrivateCompetition;

import java.util.List;

public class ProfileCompetitionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> competitions;
    private final CompetitionItemListener listener;
    private final PrivateCompetitionItemListener privateListener;
    private boolean isHistory;

    public ProfileCompetitionsAdapter(CompetitionItemListener listener, PrivateCompetitionItemListener privateListener, boolean isHistory) {
        this.listener = listener;
        this.privateListener = privateListener;
        this.isHistory = isHistory;
    }

    public void setCompetitions(List<Object> competitions) {
        this.competitions = competitions;
        notifyDataSetChanged();
    }

    public interface CompetitionItemListener {
        void onCompetitionClicked(Competition competition);
    }

    public interface PrivateCompetitionItemListener {
        void onPrivateCompetitionClicked(Competition competition, String password, int index, boolean isHistory);
    }

    public void setProgressState(int index, int state) {
        ((PrivateCompetition) competitions.get(index)).setProgress(state);
        notifyDataSetChanged();
    }

    public int getSelectedIndex() {
        for (int i = 0; i < competitions.size(); i++) {
            if (competitions.get(i) instanceof PrivateCompetition) {
                if (((PrivateCompetition) competitions.get(i)).getProgress() == View.VISIBLE) {
                    return i;
                }
            }
        }
        return 0;
    }


    @Override
    public int getItemViewType(int position) {
        return competitions.get(position) instanceof PrivateCompetition ? 1 : 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == 0 ?
                new PublicCompetitionHolder((CompetitionListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.competition_list_item, parent, false)) :
                new PrivateCompetitionHolder((PrivateCompetitionsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.private_competitions_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ((PublicCompetitionHolder) holder).dataBinding.setCompetition((Competition) competitions.get(position));
                break;
            case 1:
                ((PrivateCompetitionHolder) holder).dataBinding.setCompetition((PrivateCompetition) competitions.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return competitions != null ? competitions.size() : 0;
    }

    class PublicCompetitionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CompetitionListItemBinding dataBinding;

        public PublicCompetitionHolder(CompetitionListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onCompetitionClicked(dataBinding.getCompetition());
        }
    }

    class PrivateCompetitionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private PrivateCompetitionsListItemBinding dataBinding;

        public PrivateCompetitionHolder(PrivateCompetitionsListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;

            dataBinding.submitButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            privateListener.onPrivateCompetitionClicked(dataBinding.getCompetition(), dataBinding.getCompetition().getPassword(), getAdapterPosition(), isHistory);
        }
    }


}
