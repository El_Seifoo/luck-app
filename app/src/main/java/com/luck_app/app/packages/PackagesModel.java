package com.luck_app.app.packages;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.PackageObj;
import com.luck_app.app.utils.MySingleton;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PackagesModel {
    private Context context;

    public PackagesModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<PackageObj>> packagesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<PackageObj>> fetchPackages(Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<List<PackageObj>>> call = MySingleton.getInstance(context).createService().fetchPackages(header);

        call.enqueue(new Callback<ApiResponse<List<PackageObj>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<PackageObj>>> call, Response<ApiResponse<List<PackageObj>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<List<PackageObj>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        List<PackageObj> packages = apiResponse.getData();
                        if (packages == null)
                            callback.setEmptyListTextView(View.VISIBLE);
                        else if (packages.isEmpty())
                            callback.setEmptyListTextView(View.VISIBLE);
                        else {
                            callback.setEmptyListTextView(View.GONE);
                            packagesMutableLiveData.setValue(packages);
                        }
                    } else
                        callback.showResponseMessage(apiResponse.getMessage());

                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
            }

            @Override
            public void onFailure(Call<ApiResponse<List<PackageObj>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });

        return packagesMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t);

        void showResponseMessage(String message);
    }
}
