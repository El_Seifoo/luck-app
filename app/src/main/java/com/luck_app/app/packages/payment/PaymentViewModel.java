package com.luck_app.app.packages.payment;

import android.app.Activity;
import android.app.Application;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.google.gson.Gson;
import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;
import com.myfatoorah.sdk.model.executepayment.MFExecutePaymentRequest;
import com.myfatoorah.sdk.model.executepayment_cardinfo.MFCardInfo;
import com.myfatoorah.sdk.model.executepayment_cardinfo.MFDirectPaymentResponse;
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentRequest;
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentResponse;
import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod;
import com.myfatoorah.sdk.model.paymentstatus.MFGetPaymentStatusResponse;
import com.myfatoorah.sdk.utils.MFAPILanguage;
import com.myfatoorah.sdk.utils.MFCurrencyISO;
import com.myfatoorah.sdk.views.MFResult;
import com.myfatoorah.sdk.views.MFSDK;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import retrofit2.Response;

public class PaymentViewModel extends AndroidViewModel implements PaymentModel.ModelCallback {
    private PaymentModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyListTextView /* flag to know if competitions list is empty or not */;

    public PaymentViewModel(@NonNull Application application) {
        super(application);
        model = new PaymentModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
    }

    protected void initiatePayment(String amount) {
        setProgress(View.VISIBLE);

        double invoiceAmount = Double.valueOf(amount);
        MFInitiatePaymentRequest request = new MFInitiatePaymentRequest(
                invoiceAmount, MFCurrencyISO.KUWAIT_KWD);

        MFSDK.INSTANCE.initiatePayment(request,
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)).equals(getApplication().getString(R.string.english_key)) ? MFAPILanguage.EN : MFAPILanguage.AR,
                (MFResult<MFInitiatePaymentResponse> result) -> {

                    if (result instanceof MFResult.Success) {
                        ArrayList<PaymentMethod> paymentMethods = (((MFResult.Success<MFInitiatePaymentResponse>) result)
                                .getResponse().getPaymentMethods());
                        if (paymentMethods == null)
                            setEmptyListTextView(View.VISIBLE);
                        else if (paymentMethods.isEmpty())
                            setEmptyListTextView(View.VISIBLE);
                        else {
                            setEmptyListTextView(View.GONE);
                            viewListener.setAvailablePayments(paymentMethods);
                        }
                    } else if (result instanceof MFResult.Fail) {
                        viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
                        setEmptyListTextView(View.VISIBLE);
                    }
                    setProgress(View.GONE);

                    return Unit.INSTANCE;
                });
    }

    protected void handlePayButtonAction(String packageId, String amount, PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.please_select_payment_method));
            return;
        }

        if (!paymentMethod.getDirectPayment())
            executePayment(paymentMethod.getPaymentMethodId(), packageId, amount);
        else {
            String cardNumber = viewListener.getCardNumber();
            if (cardNumber.isEmpty()) {
                viewListener.showToastMessage(getApplication().getString(R.string.card_number_is_required));
                return;
            }

            String expiryMonth = viewListener.getExpiryMonth();
            if (expiryMonth.isEmpty()) {
                viewListener.showToastMessage(getApplication().getString(R.string.expiry_month_is_required));
                return;
            }
            if (expiryMonth.length() == 1)
                expiryMonth = "0".concat(expiryMonth);

            String expiryYear = viewListener.getExpiryYear();
            if (expiryYear.isEmpty()) {
                viewListener.showToastMessage(getApplication().getString(R.string.expiry_year_is_required));
                return;
            }
            if (expiryYear.length() == 1)
                expiryYear = "0".concat(expiryYear);

            String securityCode = viewListener.getSecurityCode();
            if (securityCode.isEmpty()) {
                viewListener.showToastMessage(getApplication().getString(R.string.security_code_is_required));
                return;
            }

            executePaymentWithCardInfo(paymentMethod.getPaymentMethodId(), packageId, amount, cardNumber, expiryMonth, expiryYear, securityCode);


        }
    }

    private void executePayment(Integer paymentMethod, String packageId, String amount) {

        double invoiceAmount = Double.valueOf(amount);
        MFExecutePaymentRequest request = new MFExecutePaymentRequest(paymentMethod, invoiceAmount);
        request.setDisplayCurrencyIso(MFCurrencyISO.KUWAIT_KWD);

        MFSDK.INSTANCE.executePayment(viewListener.activity(), request,
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)).equals(getApplication().getString(R.string.english_key)) ? MFAPILanguage.EN : MFAPILanguage.AR,
                (String invoiceId, MFResult<MFGetPaymentStatusResponse> result) -> {
                    if (result instanceof MFResult.Success)
                        // add points to user balance after payment done successfully
                        model.buyPackage(getRequestHeader(), packageId, this);
                    else if (result instanceof MFResult.Fail)
                        viewListener.showToastMessage(getApplication().getString(R.string.payment_failed));

                    return Unit.INSTANCE;
                });
    }

    private void executePaymentWithCardInfo(Integer paymentMethod, String packageId, String amount,
                                            String cardNumber, String expiryMonth, String expiryYear, String securityCode) {

        setProgress(View.VISIBLE);

        double invoiceAmount = Double.parseDouble(amount);
        MFExecutePaymentRequest request = new MFExecutePaymentRequest(paymentMethod, invoiceAmount);
        request.setDisplayCurrencyIso(MFCurrencyISO.KUWAIT_KWD);

        MFCardInfo mfCardInfo = new MFCardInfo(cardNumber, expiryMonth, expiryYear, securityCode,
                true, false);

        MFSDK.INSTANCE.executeDirectPayment(viewListener.activity(), request, mfCardInfo,
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)).equals(getApplication().getString(R.string.english_key)) ? MFAPILanguage.EN : MFAPILanguage.AR,
                (String invoiceId, MFResult<MFDirectPaymentResponse> result) -> {
                    if (result instanceof MFResult.Success)
                        // add points to user balance after payment done successfully
                        model.buyPackage(getRequestHeader(), packageId, this);
                    else if (result instanceof MFResult.Fail)
                        viewListener.showToastMessage(getApplication().getString(R.string.payment_failed));

                    setProgress(View.GONE);

                    return Unit.INSTANCE;
                });
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }


    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    @Override
    public void handleBuyPackageResponse(Response<ApiResponse<String>> response) {
        if (response.code() == 200) {
            ApiResponse<String> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true)))
                viewListener.showToastMessage(getApplication().getString(R.string.payment_done_successfully));
            else
                viewListener.showToastMessage(apiResponse.getMessage().concat(" ").concat(getApplication().getString(R.string.please_contact_us)));

        } else
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data).concat(" ").concat(getApplication().getString(R.string.please_contact_us)));

        viewListener.dismiss();
    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {
        void showToastMessage(String message);

        void setAvailablePayments(ArrayList<PaymentMethod> paymentMethods);

        Activity activity();

        String getCardNumber();

        String getExpiryMonth();

        String getExpiryYear();

        String getSecurityCode();

        void dismiss();
    }
}
