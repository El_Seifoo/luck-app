package com.luck_app.app.packages.payment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.PaymentMethodListItemBinding;
import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.Holder> {
    private List<PaymentMethod> methods;
    private List<Integer> selectedList = new ArrayList<>();
    private final MethodItemListener listener;

    public PaymentMethodsAdapter(MethodItemListener listener) {
        this.listener = listener;
    }

    public void setMethods(List<PaymentMethod> methods) {
        this.methods = methods;
        for (int i = 0; i < methods.size(); i++) {
            selectedList.add(View.GONE);
        }
        notifyDataSetChanged();
    }

    public void setItemChecked(int position) {
        if (selectedList.get(position) == View.VISIBLE) {
            selectedList.set(position, View.GONE);
            notifyItemChanged(position);
            return;
        }

        for (int i = 0; i < selectedList.size(); i++) {
            if (i == position) {
                selectedList.set(i, View.VISIBLE);
                notifyItemChanged(i);
            } else {
                if (selectedList.get(i) == View.VISIBLE) {
                    selectedList.set(i, View.GONE);
                    notifyItemChanged(i);
                }
            }
        }
    }

    public interface MethodItemListener {
        void onMethodItemClicked(int position, PaymentMethod paymentMethod);
    }

    @NonNull
    @Override
    public PaymentMethodsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((PaymentMethodListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.payment_method_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodsAdapter.Holder holder, int position) {
        holder.dataBinding.setPaymentMethod(methods.get(position));
        holder.dataBinding.setChecked(selectedList.get(position));
    }

    @Override
    public int getItemCount() {
        return methods != null ? methods.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        PaymentMethodListItemBinding dataBinding;

        public Holder(@NonNull PaymentMethodListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;

            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onMethodItemClicked(getAdapterPosition(), dataBinding.getPaymentMethod());
        }
    }
}
