package com.luck_app.app.packages.payment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.luck_app.app.R;
import com.luck_app.app.databinding.PaymentDialogFragmentLayoutBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;
import com.luck_app.app.utils.URLs;
import com.myfatoorah.sdk.model.executepayment.MFExecutePaymentRequest;
import com.myfatoorah.sdk.model.executepayment_cardinfo.MFCardInfo;
import com.myfatoorah.sdk.model.executepayment_cardinfo.MFDirectPaymentResponse;
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentRequest;
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentResponse;
import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod;
import com.myfatoorah.sdk.model.paymentstatus.MFGetPaymentStatusResponse;
import com.myfatoorah.sdk.utils.MFAPILanguage;
import com.myfatoorah.sdk.utils.MFCurrencyISO;
import com.myfatoorah.sdk.views.MFResult;
import com.myfatoorah.sdk.views.MFSDK;

import java.util.ArrayList;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class PaymentDialogFragment extends DialogFragment implements PaymentMethodsAdapter.MethodItemListener, PaymentViewModel.ViewListener {
    private PaymentDialogFragmentLayoutBinding dataBinding;
    private PaymentViewModel viewModel;
    private PaymentMethodsAdapter adapter;

    public static PaymentDialogFragment newInstance(String packageId, String amount) {
        PaymentDialogFragment fragment = new PaymentDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("packageId", packageId);
        bundle.putString("amount", amount);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.8), WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.payment_dialog_fragment_layout, container, false);
        dataBinding.setDirectPayment(View.GONE);
        viewModel = new ViewModelProvider(this).get(PaymentViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        RecyclerView newList = dataBinding.methodsList;
        adapter = new PaymentMethodsAdapter(this);
        newList.setAdapter(adapter);


        MFSDK.INSTANCE.setUpActionBar(getString(R.string.app_name), R.color.white,
                R.color.main_light_color, true);

        viewModel.initiatePayment(getArguments().getString("amount"));

        dataBinding.payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.handlePayButtonAction(getArguments().getString("packageId"), getArguments().getString("amount"), paymentMethod);
            }
        });

        return dataBinding.getRoot();
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setAvailablePayments(ArrayList<PaymentMethod> paymentMethods) {
        adapter.setMethods(paymentMethods);
    }

    @Override
    public Activity activity() {
        return getActivity();
    }

    @Override
    public String getCardNumber() {
        return dataBinding.cardNumber.getText().toString();
    }

    @Override
    public String getExpiryMonth() {
        return dataBinding.expiryMonth.getText().toString();
    }

    @Override
    public String getExpiryYear() {
        return dataBinding.expiryYear.getText().toString();
    }

    @Override
    public String getSecurityCode() {
        return dataBinding.securityCode.getText().toString();
    }

    private PaymentMethod paymentMethod;

    @Override
    public void onMethodItemClicked(int position, PaymentMethod paymentMethod) {
        if (this.paymentMethod != null) {
            if (this.paymentMethod.equals(paymentMethod)) {
                this.paymentMethod = null;
                dataBinding.setDirectPayment(View.GONE);
            } else {
                this.paymentMethod = paymentMethod;
                dataBinding.setDirectPayment(paymentMethod.getDirectPayment() ? View.VISIBLE : View.GONE);
            }
        } else {
            this.paymentMethod = paymentMethod;
            dataBinding.setDirectPayment(paymentMethod.getDirectPayment() ? View.VISIBLE : View.GONE);
        }

        adapter.setItemChecked(position);
    }

}
