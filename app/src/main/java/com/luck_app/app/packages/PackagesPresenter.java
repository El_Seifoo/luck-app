package com.luck_app.app.packages;

public interface PackagesPresenter {
    void onButtonClicked(int index);
}
