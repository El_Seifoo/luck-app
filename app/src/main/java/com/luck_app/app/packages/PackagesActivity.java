package com.luck_app.app.packages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityPackagesBinding;
import com.luck_app.app.models.PackageObj;
import com.luck_app.app.packages.payment.PaymentDialogFragment;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.util.List;

public class PackagesActivity extends AppCompatActivity implements PackagesViewModel.ViewListener, PackagesPresenter, PackageAdapter.ItemListener {
    private ActivityPackagesBinding dataBinding;
    private PackagesViewModel viewModel;
    private PackageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_packages);
        viewModel = new ViewModelProvider(this).get(PackagesViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        RecyclerView packagesList = dataBinding.packagesList;
        packagesList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapter = new PackageAdapter(this);
        packagesList.setAdapter(adapter);

        viewModel.requestPackages().observe(this, new Observer<List<PackageObj>>() {
            @Override
            public void onChanged(List<PackageObj> packages) {
                adapter.setPackages(packages);
            }
        });


    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPaymentMethodsDialog() {
        //Toast.makeText(getApplication(), getPackageObj().get().getId(), Toast.LENGTH_SHORT).show();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null)
            fragmentTransaction.remove(prev);

        fragmentTransaction.addToBackStack(null);

        PackageObj packageObj = viewModel.getPackageObj().get();
        DialogFragment dialogFragment = PaymentDialogFragment.newInstance(packageObj.getId(), packageObj.getPackageValue());
        dialogFragment.show(fragmentTransaction, "dialog");

    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : buy package
                |-> 2 : retry fetching packages request
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void onPackageItemClickListener(PackageObj packageObj, int index) {
        adapter.selectPackageByIndex(index);
        viewModel.setPackageObj(packageObj);
    }
}
