package com.luck_app.app.packages.payment;

import android.content.Context;
import android.view.View;

import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentModel {
    private Context context;

    public PaymentModel(Context context) {
        this.context = context;
    }

    protected void buyPackage(Map<String, String> header, String packageId, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().buyPackage(header, packageId);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.setProgress(View.GONE);
                callback.handleBuyPackageResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void handleBuyPackageResponse(Response<ApiResponse<String>> response);
    }
}
