package com.luck_app.app.packages;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.PackagesListItemBinding;
import com.luck_app.app.models.PackageObj;

import java.util.List;

public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.Holder> {
    private List<PackageObj> packages;
    private final ItemListener listener;

    public PackageAdapter(ItemListener listener) {
        this.listener = listener;
    }

    public void setPackages(List<PackageObj> packages) {
        this.packages = packages;
        notifyDataSetChanged();
    }

    public void selectPackageByIndex(int index) {
        if (packages.get(index).isSelected()) {
            packages.get(index).setSelected(false);
            notifyItemChanged(index);
            return;
        }

        for (int i = 0; i < packages.size(); i++) {
            if (i == index) {
                packages.get(i).setSelected(true);
                notifyItemChanged(i);
            } else {
                if (packages.get(i).isSelected()) {
                    packages.get(i).setSelected(false);
                    notifyItemChanged(i);
                }
            }
        }

    }


    protected interface ItemListener {
        void onPackageItemClickListener(PackageObj packageObj, int index);
    }

    @NonNull
    @Override
    public PackageAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((PackagesListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.packages_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PackageAdapter.Holder holder, int position) {
        holder.dataBinding.setPackageObj(packages.get(position));

    }

    @Override
    public int getItemCount() {
        return packages != null ? packages.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private PackagesListItemBinding dataBinding;

        public Holder(@NonNull PackagesListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;

            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onPackageItemClickListener(dataBinding.getPackageObj(), getAdapterPosition());
        }
    }
}
