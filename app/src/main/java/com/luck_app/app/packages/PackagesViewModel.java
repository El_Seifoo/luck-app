package com.luck_app.app.packages;

import android.app.Application;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.PackageObj;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PackagesViewModel extends AndroidViewModel implements PackagesModel.ModelCallback {
    private PackagesModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyListTextView /* flag to know if competitions list is empty or not */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<PackageObj> packageObj /* carry selected package object */;

    public PackagesViewModel(@NonNull Application application) {
        super(application);
        model = new PackagesModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        packageObj = new ObservableField<>(new PackageObj());
    }

    /*
        Call model fun. fetchPackages to get packages
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<List<PackageObj>> requestPackages() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchPackages(getRequestHeader(), this);
    }

    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : buy package
                |-> 2 : retry fetching packages request
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                if (getPackageObj().get().getId() == null)
                    viewListener.showToastMessage(getApplication().getString(R.string.please_select_package_first));
                else
                    viewListener.showPaymentMethodsDialog();
                break;
            case 2:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchPackages(getRequestHeader(), this);

        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<PackageObj> getPackageObj() {
        return packageObj;
    }

    public void setPackageObj(PackageObj packageObj) {
        if (packageObj.getId().equals(getPackageObj().get().getId())) {
            this.packageObj.set(new PackageObj());
        } else this.packageObj.set(packageObj);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {
        void finishActivity();

        void showToastMessage(String message);

        void showPaymentMethodsDialog();

    }
}
