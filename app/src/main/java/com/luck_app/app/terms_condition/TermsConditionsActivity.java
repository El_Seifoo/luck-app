package com.luck_app.app.terms_condition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.MenuItem;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityTermsConditionsBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

public class TermsConditionsActivity extends AppCompatActivity implements TermsConditionsPresenter {
    private TermsConditionsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        final ActivityTermsConditionsBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_terms_conditions);
        viewModel = new ViewModelProvider(this).get(TermsConditionsViewModel.class);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle(getString(R.string.terms_conditions));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestTermsConditions().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String data) {
                dataBinding.setTermsConditions(data);
            }
        });
    }

    @Override
    public void onRetryClicked() {
        viewModel.retry();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
