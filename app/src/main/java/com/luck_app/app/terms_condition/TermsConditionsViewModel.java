package com.luck_app.app.terms_condition;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;

public class TermsConditionsViewModel extends AndroidViewModel implements TermsConditionsModel.ModelCallback {
    private TermsConditionsModel model;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;

    public TermsConditionsViewModel(@NonNull Application application) {
        super(application);
        model = new TermsConditionsModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        Call model fun. fetchTermsConditions to get terms & conditions data
        show loader then make req.
     */
    protected MutableLiveData<String> requestTermsConditions() {
        setProgress(View.VISIBLE);
        return model.fetchTermsConditions(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                this);
    }

    /*
        retry fetching terms & conditions data
     */
    protected void retry() {
        setErrorView(View.GONE);
        setProgress(View.VISIBLE);

        model.fetchTermsConditions(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                this);
    }

    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }


    // Setters & Getters ----------> START
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setters & Getters ----------> END

}
