package com.luck_app.app.terms_condition;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Info;
import com.luck_app.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditionsModel {
    private Context context;

    public TermsConditionsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<String> fetchTermsConditions(String lang, final ModelCallback callback) {
        Call<ApiResponse<Info>> call = MySingleton.getInstance(context).createService().fetchTermsConditions(lang);

        call.enqueue(new Callback<ApiResponse<Info>>() {
            @Override
            public void onResponse(Call<ApiResponse<Info>> call, Response<ApiResponse<Info>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 200) {
                    if (response.body().getStatus().equals(context.getString(R.string.api_status_true))) {
                        if (response.body().getData() != null) {
                            stringMutableLiveData.setValue(response.body().getData().getData());
                        } else callback.showResponseMessage(response.body().getMessage());
                    } else callback.showResponseMessage(response.body().getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
            }

            @Override
            public void onFailure(Call<ApiResponse<Info>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
        return stringMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void showResponseMessage(String message);
    }
}
