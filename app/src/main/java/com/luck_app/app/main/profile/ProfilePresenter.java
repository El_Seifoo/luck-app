package com.luck_app.app.main.profile;

public interface ProfilePresenter {
    void onButtonClicked(int index);
}
