package com.luck_app.app.main.profile;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.ProfileResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileModel {
    private Context context;

    public ProfileModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<ProfileResponse> profileMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<ProfileResponse> fetchProfile(Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<ProfileResponse>> call = MySingleton.getInstance(context).createService().fetchProfileData(header);

        call.enqueue(new Callback<ApiResponse<ProfileResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<ProfileResponse>> call, Response<ApiResponse<ProfileResponse>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<ProfileResponse> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        if (apiResponse.getData() != null) {
                            ProfileResponse profile = apiResponse.getData();

                            if (profile.getUser() == null) {
                                callback.showResponseMessage(apiResponse.getMessage());
                                return;
                            }

                            List<Competition> myCompetitions = profile.getMyCompetitions();
                            List<Competition> history = profile.getHistory();

                            if (myCompetitions == null)
                                callback.setMyCompetitionsEmptyListTextView(View.VISIBLE);
                            else if (myCompetitions.isEmpty())
                                callback.setMyCompetitionsEmptyListTextView(View.VISIBLE);
                            else callback.setMyCompetitionsEmptyListTextView(View.GONE);

                            if (history == null)
                                callback.setHistoryEmptyListTextView(View.VISIBLE);
                            else if (history.isEmpty())
                                callback.setHistoryEmptyListTextView(View.VISIBLE);
                            else callback.setHistoryEmptyListTextView(View.GONE);

                            profileMutableLiveData.setValue(profile);

                        } else callback.showResponseMessage(apiResponse.getMessage());
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
            }

            @Override
            public void onFailure(Call<ApiResponse<ProfileResponse>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });

        return profileMutableLiveData;
    }

    protected void fetchDetails(Map<String, String> body, Map<String, String> header, boolean isHistory, final ModelCallback callback) {
        Call<ApiResponse<Competition>> call = MySingleton.getInstance(context).createService().privateCompDetails(header, body);

        call.enqueue(new Callback<ApiResponse<Competition>>() {
            @Override
            public void onResponse(Call<ApiResponse<Competition>> call, Response<ApiResponse<Competition>> response) {
                callback.handleRequestingDetailsResponse(response, isHistory);
            }

            @Override
            public void onFailure(Call<ApiResponse<Competition>> call, Throwable t) {
                callback.onFailureHandler(t, isHistory);
            }
        });

    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setMyCompetitionsEmptyListTextView(int empty);

        void setHistoryEmptyListTextView(int empty);

        void onFailureHandler(Throwable t);

        void onFailureHandler(Throwable t, boolean isHistory);

        void showResponseMessage(String message);

        void handleRequestingDetailsResponse(Response<ApiResponse<Competition>> response, boolean isHistory);
    }
}
