package com.luck_app.app.main.profile;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.messaging.FirebaseMessaging;
import com.luck_app.app.R;
import com.luck_app.app.edit_profile.EditProfileActivity;
import com.luck_app.app.login.LoginActivity;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.ProfileResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.notifications.NotificationsActivity;
import com.luck_app.app.packages.PackagesActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class ProfileViewModel extends AndroidViewModel implements ProfileModel.ModelCallback {
    private final int EDIT_PROFILE_REQUEST_CODE = 1;
    private ProfileModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> myCompetitionsEmptyListTextView /* flag to know if my competitions list is empty or not */;
    private ObservableField<Integer> historyEmptyListTextView /* flag to know if history list is empty or not */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private boolean isDetailsLoading;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        model = new ProfileModel(application);
        progress = new ObservableField<>(View.GONE);
        myCompetitionsEmptyListTextView = new ObservableField<>(View.GONE);
        historyEmptyListTextView = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
    }

    /*
        Call model fun. fetchProfile to get profile data
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<ProfileResponse> requestProfile() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchProfile(getRequestHeader(), this);
    }

    /*
        handle error response of requests
        fetching profile data
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(getFailureMessage(t));
    }

    /*
        handle error response of requests
        requesting details
     */
    @Override
    public void onFailureHandler(Throwable t, boolean isHistory) {
        isDetailsLoading = false;
        viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.GONE, isHistory);
        viewListener.showToastMessage(getFailureMessage(t));

    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    protected void requestDetails(String id, boolean isHistory, String password, int index) {
        if (isDetailsLoading) return;
        if (password.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_length_error));
            return;
        }

        isDetailsLoading = true;
        viewListener.setPrivateItemProgress(index, View.VISIBLE, isHistory);
        Map<String, String> body = new HashMap<>();
        body.put("competion", id);
        body.put("password", password);

        model.fetchDetails(body, getRequestHeader(), isHistory, this);
    }

    @Override
    public void handleRequestingDetailsResponse(Response<ApiResponse<Competition>> response, boolean isHistory) {
        isDetailsLoading = false;
        viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.VISIBLE, isHistory);
        int code = response.code();
        if (code == 200) {
            ApiResponse<Competition> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                Competition competition = apiResponse.getData();
                if (competition != null) {
                    viewListener.setCompetitionDetails(competition);
                    isDetailsLoading = false;
                    viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.GONE, isHistory);
                } else
                    this.showResponseMessage(apiResponse.getMessage(), isHistory);

            } else
                this.showResponseMessage(apiResponse.getMessage(), isHistory);
        } else if (code == 500)
            this.showResponseMessage(getApplication().getString(R.string.error_fetching_data), isHistory);
        else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                this.showResponseMessage(jsonObject.getString("message"), isHistory);
            } catch (Exception e) {
                this.showResponseMessage(getApplication().getString(R.string.error_sending_data), isHistory);
            }
        }
    }


    /*
        show response message
     */
    public void showResponseMessage(String message, boolean isHistory) {
        isDetailsLoading = false;
        viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(isHistory), View.GONE, isHistory);
        viewListener.showToastMessage(message);
    }

    /*
        handle action of every button in the fragment
        @Param index .. flag to know which button was clicked
                |-> 0 : logout icon
                |-> 1 : notification icon
                |-> 2 : edit profile icon
                |-> 3 : buy more text
                |-> 4 : retry fetching profile data
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                FirebaseMessaging.getInstance().unsubscribeFromTopic("LuckApp");
                String lang = MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key));
                MySingleton.getInstance(getApplication()).logout();
                MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.APP_LANGUAGE, lang);
                Intent intent = new Intent(getApplication(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                viewListener.navigateToDestination(intent);
                break;
            case 1:
                viewListener.navigateToDestination(new Intent(getApplication(), NotificationsActivity.class));
                break;
            case 2:
                viewListener.navigateToDestination(EditProfileActivity.class, EDIT_PROFILE_REQUEST_CODE);
                break;
            case 3:
                viewListener.navigateToDestination(new Intent(getApplication(), PackagesActivity.class));
                break;
            case 4:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchProfile(getRequestHeader(), this);
                break;
            case 5:
                viewListener.showProfilePicture();
        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public boolean isDetailsLoading() {
        return isDetailsLoading;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getMyCompetitionsEmptyListTextView() {
        return myCompetitionsEmptyListTextView;
    }

    @Override
    public void setMyCompetitionsEmptyListTextView(int empty) {
        this.myCompetitionsEmptyListTextView.set(empty);
    }

    public ObservableField<Integer> getHistoryEmptyListTextView() {
        return historyEmptyListTextView;
    }

    @Override
    public void setHistoryEmptyListTextView(int empty) {
        this.historyEmptyListTextView.set(empty);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_PROFILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            viewListener.updateUser((User) data.getSerializableExtra("user"));
        }
    }

    protected interface ViewListener {
        void navigateToDestination(Intent intent);

        void navigateToDestination(Class destination, int requestCode);

        void updateUser(User user);

        void showToastMessage(String message);

        void setCompetitionDetails(Competition competition);

        void setPrivateItemProgress(int index, int progress, boolean isHistory);

        int getSelectedIndex(boolean isHistory);

        void showProfilePicture();
    }
}
