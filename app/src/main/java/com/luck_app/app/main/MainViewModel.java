package com.luck_app.app.main;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;

import com.luck_app.app.R;
import com.luck_app.app.about_app.AboutAppActivity;
import com.luck_app.app.add_competition.AddActivity;
import com.luck_app.app.add_competition.CompetitionTypeActivity;
import com.luck_app.app.contact_us.ContactUsActivity;
import com.luck_app.app.login.LoginActivity;
import com.luck_app.app.main.home.HomeFragment;
import com.luck_app.app.main.profile.ProfileFragment;
import com.luck_app.app.terms_condition.TermsConditionsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

public class MainViewModel extends AndroidViewModel {
    private ViewListener viewListener;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    protected void checkSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
            viewListener.loadFragment();
        }
    }

    public boolean handleOnNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_contact_us:
                return viewListener.navigateToActivity(ContactUsActivity.class);
            case R.id.nav_about_the_app:
                return viewListener.navigateToActivity(AboutAppActivity.class);
            case R.id.nav_terms_conditions:
                return viewListener.navigateToActivity(TermsConditionsActivity.class);
            case R.id.nav_share_app:
                try {
                    Intent share = new Intent(Intent.ACTION_SEND)
                            .setType("text/plain")
                            .putExtra(Intent.EXTRA_TEXT, getApplication().getString(R.string.app_link, getApplication().getPackageName()));
                    viewListener.startSharing(share);
                } catch (Exception e) {
                    viewListener.showToastMessage(getApplication().getString(R.string.sorry_unable_to_share));
                }
                viewListener.closeDrawer();
                return true;
            case R.id.nav_language:
                MySingleton.getInstance(getApplication())
                        .saveStringToSharedPref(
                                Constants.APP_LANGUAGE,
                                MySingleton.getInstance(getApplication())
                                        .getStringFromSharedPref(
                                                Constants.APP_LANGUAGE,
                                                getApplication().getString(R.string.default_language_key)).equals(getApplication().getString(R.string.english_key)) ?
                                        getApplication().getString(R.string.arabic_key) :
                                        getApplication().getString(R.string.english_key));
                return viewListener.changeLanguage();
            case R.id.nav_logout:
                return viewListener.handleLogoutAction(LoginActivity.class);
            case R.id.nav_add:
                return viewListener.navigateToActivity(CompetitionTypeActivity.class);
            case R.id.nav_profile:
                // navItemIndex , CURRENT_TAG
                viewListener.handleNavFragments(1, MainActivity.TAG_PROFILE, true);
                break;
            case R.id.nav_home:
            default:
                // navItemIndex , CURRENT_TAG
                viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
        }

        viewListener.loadFragment();
        return true;
    }

    protected Fragment getSelectedFragment(int naveItemIndex) {
        switch (naveItemIndex) {
            case 1:
                return ProfileFragment.newInstance();
            case 0:
            default:
                return HomeFragment.newInstance();
        }
    }


    public boolean onBackBtnPressed(boolean drawerOpen, boolean shouldLoadHomeFragOnBackPress) {
        if (drawerOpen) {
            viewListener.closeDrawer();
            return true;
        }

        // return to home fragment if user pressed back in other fragment
        if (shouldLoadHomeFragOnBackPress) {
            viewListener.selectHome();
            return true;
        }

        return false;
    }


    // Setters & Getters ------------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }// Setters & Getters ------------> END


    protected interface ViewListener {

        void loadFragment();

        boolean navigateToActivity(Class destination);

        boolean handleLogoutAction(Class destination);

        void closeDrawer();

        void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress);

        void selectHome();

        void startSharing(Intent share);

        void showToastMessage(String message);

        boolean changeLanguage();
    }
}
