package com.luck_app.app.main.home;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.new_competition.NewActivity;
import com.luck_app.app.notifications.NotificationsActivity;
import com.luck_app.app.previous_competition.PreviousActivity;
import com.luck_app.app.private_competition.PrivateActivity;
import com.luck_app.app.trend.TrendActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeViewModel extends AndroidViewModel implements HomeModel.ModelCallback {
    private HomeModel model;
    private ViewListener viewListener /* communicator betn. view model and its view */;
    private ParentListener parentListener /* communicator betn. fragment and its parent activity (MainActivity) */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> sliderVisibility /* flag to know if slider is visible or no */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        model = new HomeModel(application);
        progress = new ObservableField<>(View.GONE);
        sliderVisibility = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
    }


    protected MutableLiveData<List<String>> requestSliderData() {
        setProgress(View.VISIBLE);
        return model.fetchSliderData(getRequestHeader(), this);
    }

    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        handle action of every button in the fragment
        @Param index .. flag to know which button was clicked
                |-> 0 : nav-drawer icon
                |-> 1 : notification icon
                |-> 2 : new competition
                |-> 3 : private competition
                |-> 4 : previous competition
                |-> 5 : organizer trend
                |-> 6 : retry fetching slider data
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                // call parent communicator to open nav-drawer
                parentListener.openDrawer();
                break;
            case 1:
                viewListener.navigateToDestination(NotificationsActivity.class);
                break;
            case 2:
                viewListener.navigateToDestination(NewActivity.class);
                break;
            case 3:
                viewListener.navigateToDestination(PrivateActivity.class);
                break;
            case 4:
                viewListener.navigateToDestination(PreviousActivity.class);
                break;
            case 5:
                viewListener.navigateToDestination(TrendActivity.class);
                break;
            case 6:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);

                model.fetchSliderData(getRequestHeader(), this);
        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void setParentListener(ParentListener parentListener) {
        this.parentListener = parentListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getSliderVisibility() {
        return sliderVisibility;
    }

    @Override
    public void setSliderVisibility(int visibility) {
        this.sliderVisibility.set(visibility);
    }


    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {

        void navigateToDestination(Class destination);
    }

    public interface ParentListener {
        void openDrawer();
    }
}
