package com.luck_app.app.main.home;

public interface HomePresenter {
    void onButtonClicked(int index);
}
