package com.luck_app.app.main.home;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.utils.MySingleton;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeModel {
    private Context context;

    public HomeModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<String>> sliderMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<String>> fetchSliderData(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<List<String>>> call = MySingleton.getInstance(context).createService().fetchHomeSlider(map);

        call.enqueue(new Callback<ApiResponse<List<String>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<String>>> call, Response<ApiResponse<List<String>>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 200) {
                    ApiResponse<List<String>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        List<String> sliderData = apiResponse.getData();
                        if (sliderData == null) {
                            callback.setSliderVisibility(View.GONE);
                        } else if (sliderData.isEmpty()) {
                            callback.setSliderVisibility(View.GONE);
                        } else {
                            callback.setSliderVisibility(View.VISIBLE);
                            sliderMutableLiveData.setValue(sliderData);
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));

            }

            @Override
            public void onFailure(Call<ApiResponse<List<String>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });

        return sliderMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setSliderVisibility(int visibility);

        void onFailureHandler(Throwable t);

        void showResponseMessage(String message);
    }
}
