package com.luck_app.app.main.profile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.CompetitionListItemBinding;
import com.luck_app.app.models.Competition;

import java.util.List;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.Holder>{
    private List<Competition> competitions;

    public void setCompetitions(List<Competition> competitions) {
        this.competitions = competitions;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ProfileAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CompetitionListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.competition_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileAdapter.Holder holder, int position) {
        holder.dataBinding.setCompetition(competitions.get(position));
    }

    @Override
    public int getItemCount() {
        return competitions != null ? competitions.size() : 0;
    }


    public class Holder extends RecyclerView.ViewHolder {
        private CompetitionListItemBinding dataBinding;

        public Holder(@NonNull CompetitionListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }
    }
}
