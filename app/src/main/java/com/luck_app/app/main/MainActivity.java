package com.luck_app.app.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;


import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.luck_app.app.R;
import com.luck_app.app.add_competition.CompetitionTypeActivity;
import com.luck_app.app.databinding.ActivityMainBinding;
import com.luck_app.app.main.home.HomeViewModel;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

public class MainActivity extends AppCompatActivity implements MainViewModel.ViewListener, BottomNavigationView.OnNavigationItemSelectedListener, HomeViewModel.ParentListener {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private BottomNavigationView bottomNavigation;
    ActivityMainBinding dataBinding;

    // tag for attached fragments
    protected static final String TAG_HOME = "home";//0
    protected static final String TAG_PROFILE = "profile";//1

    // index to know the selected nav nav_menu item
    public int naveItemIndex = 0;

    // tag for the current attached fragment
    private static String CURRENT_TAG = TAG_HOME;

    // flag to make decision when user press back button
    private boolean shouldLoadHomeFragOnBackPress = false;

    private Handler mHandler;
    private Runnable runnable;


    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.setViewListener(this);


        mHandler = new Handler();

        drawer = dataBinding.drawerLayout;
        drawer.setDrawerElevation(0);
        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        navigationView = dataBinding.navView;

        bottomNavigation = dataBinding.bottomNavigation.navigation;
        bottomNavigation.setOnNavigationItemSelectedListener(this);

        // setting navigation drawer items in the center of the drawer using header trick
        // make empty nav header set its height as 1/4 of navigation drawer whole height
        final ViewTreeObserver vto = dataBinding.drawerLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                dataBinding.drawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                navigationView.getHeaderView(0).getLayoutParams().height = (dataBinding.drawerLayout.getMeasuredHeight() / 4);
                navigationView.getHeaderView(0).requestLayout();
            }
        });

        // initialize navigation drawer
        setUpNavigation();

        viewModel.checkSavedInstanceState(savedInstanceState);
    }

    private void setUpNavigation() {
        navigationView.getMenu().getItem(4).setTitle(
                MySingleton.getInstance(getApplicationContext())
                        .getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language_key)).equals(getString(R.string.english_key)) ?
                        getString(R.string.arabic) : getString(R.string.english));

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return viewModel.handleOnNavigationItemSelected(menuItem);
            }
        });

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL)
                    dataBinding.bottomNavigation.container.setX(-slideOffset * drawerView.getWidth());
                else
                    dataBinding.bottomNavigation.container.setX(slideOffset * drawerView.getWidth());
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
    }

    @Override
    public void loadFragment() {

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            closeDrawer();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = viewModel.getSelectedFragment(naveItemIndex);
                if (fragment != null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    transaction.replace(R.id.frame_container, fragment);
                    transaction.commitAllowingStateLoss();
                }
            }
        };

        if (runnable != null)
            mHandler.post(runnable);

        // close navigation drawer
        closeDrawer();
    }

    @Override
    public boolean navigateToActivity(Class destination) {
        startActivity(new Intent(this, destination));
        closeDrawer();

        if (destination.getSimpleName().equals(CompetitionTypeActivity.class.getSimpleName()))
            return false;

        return true;
    }

    @Override
    public boolean handleLogoutAction(Class destination) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic("LuckApp");
        String lang = MySingleton.getInstance(getApplicationContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language_key));
        MySingleton.getInstance(this).logout();
        MySingleton.getInstance(getApplicationContext()).saveStringToSharedPref(Constants.APP_LANGUAGE, lang);
        finish();
        startActivity(new Intent(this, destination));
        closeDrawer();
        return true;
    }

    @Override
    public void closeDrawer() {
        drawer.closeDrawers();
    }

    @Override
    public void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress) {
        this.naveItemIndex = naveItemIndex;
        CURRENT_TAG = currentTag;
        this.shouldLoadHomeFragOnBackPress = shouldLoadHomeFragOnBackPress;
    }

    @Override
    public void selectHome() {
        bottomNavigation.setSelectedItemId(R.id.nav_home);
    }

    @Override
    public void startSharing(Intent share) {
        startActivity(share);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean changeLanguage() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        return true;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return viewModel.handleOnNavigationItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (!viewModel.onBackBtnPressed(drawer.isDrawerOpen(GravityCompat.START), shouldLoadHomeFragOnBackPress))
            super.onBackPressed();
    }

    /*
        parent communicator method to open navigation drawer from fragment
     */
    @Override
    public void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }
}
