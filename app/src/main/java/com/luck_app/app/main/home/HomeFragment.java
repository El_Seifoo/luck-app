package com.luck_app.app.main.home;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.islamkhsh.CardSliderViewPager;
import com.luck_app.app.R;
import com.luck_app.app.SliderAdapter;
import com.luck_app.app.databinding.FragmentHomeBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements HomePresenter, HomeViewModel.ViewListener, SliderAdapter.SliderItemListener {
    private HomeViewModel viewModel;
    private SliderAdapter adapter;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        FragmentHomeBinding dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home, container, false);

        viewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        viewModel.setViewListener(this);
        viewModel.setParentListener((HomeViewModel.ParentListener) getActivity());
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        CardSliderViewPager cardSliderViewPager = dataBinding.viewPager;
        adapter = new SliderAdapter(this);
        cardSliderViewPager.setAdapter(adapter);

        viewModel.requestSliderData().observe(getViewLifecycleOwner(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
                adapter.setImages(strings);
            }
        });

        return dataBinding.getRoot();

    }

    /*
        handle action of every button in the fragment
        @Param index .. flag to know which button was clicked
                |-> 0 : nav-drawer icon
                |-> 1 : notification icon
                |-> 2 : new competition
                |-> 3 : private competition
                |-> 4 : previous competition
                |-> 5 : organizer trend
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(getContext(), destination));
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(getContext(), photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        })
                .withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();

    }
}
