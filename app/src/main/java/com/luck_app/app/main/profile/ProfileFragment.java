package com.luck_app.app.main.profile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.luck_app.app.ProfileCompetitionsAdapter;
import com.luck_app.app.R;
import com.luck_app.app.competition_details.CompetitionDetailsActivity;
import com.luck_app.app.databinding.FragmentProfileBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.models.ProfileResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.previous_competition.PreviousAdapter;
import com.luck_app.app.previous_competition_details.PrevCompDetailsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ProfileFragment extends Fragment implements ProfileViewModel.ViewListener, ProfilePresenter, ProfileCompetitionsAdapter.PrivateCompetitionItemListener, ProfileCompetitionsAdapter.CompetitionItemListener {
    private FragmentProfileBinding dataBinding;
    private ProfileViewModel viewModel;
    private ProfileCompetitionsAdapter myCompetitionsAdapter, historyAdapter;


    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_profile, container, false);
        viewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        RecyclerView myCompetitionsList = dataBinding.myCompetitionsList;
        myCompetitionsList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        myCompetitionsAdapter = new ProfileCompetitionsAdapter(this, this, false);
        myCompetitionsList.setAdapter(myCompetitionsAdapter);

        RecyclerView historyList = dataBinding.historyList;
        historyList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        historyAdapter = new ProfileCompetitionsAdapter(this, this, true);
        historyList.setAdapter(historyAdapter);

        viewModel.requestProfile().observe(getViewLifecycleOwner(), new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse profileResponse) {
                dataBinding.setUser(profileResponse.getUser());

                ArrayList<Object> adapterList = new ArrayList<>();
                PrivateCompetition privateComp;
                Competition competition1;

                long startLong = 0;
                long currentLong = 0;
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                try {
                    currentLong = simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime())).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                List<Competition> competitions = profileResponse.getMyCompetitions();
                for (int i = 0; i < competitions.size(); i++) {
                    try {
                        startLong = simpleDateFormat.parse(competitions.get(i).getStartDate()).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                        Log.e("aaa", e.toString());
                    }
                    if (startLong < currentLong) {
                        if (competitions.get(i).getCompetitionType() == 1) {
                            adapterList.add(competitions.get(i));
                        } else {
                            competition1 = new PrivateCompetition();
                            competition1.setData(competitions.get(i));
                            privateComp = (PrivateCompetition) competition1;
                            privateComp.setProgress(View.GONE);
                            adapterList.add(privateComp);
                        }
                    }
                }

                myCompetitionsAdapter.setCompetitions(adapterList);


                ArrayList<Object> historyList = new ArrayList<>();
                List<Competition> historyCompetitions = profileResponse.getHistory();
                for (int i = 0; i < historyCompetitions.size(); i++) {
                    if (historyCompetitions.get(i).getCompetitionType() == 1) {
                        historyList.add(historyCompetitions.get(i));
                    } else {
                        competition1 = new PrivateCompetition();
                        competition1.setData(historyCompetitions.get(i));
                        privateComp = (PrivateCompetition) competition1;
                        privateComp.setProgress(View.GONE);
                        historyList.add(privateComp);
                    }
                }

                historyAdapter.setCompetitions(historyList);
            }
        });
        return dataBinding.getRoot();
    }

    /*
            handle action of every button in the fragment
            @Param index .. flag to know which button was clicked
                    |-> 0 : logout icon
                    |-> 1 : notification icon
                    |-> 2 : edit profile icon
                    |-> 3 : buy more text
         */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void navigateToDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void navigateToDestination(Class destination, int requestCode) {
        Intent intent = new Intent(getContext(), destination);
        intent.putExtra("user", dataBinding.getUser());
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void updateUser(User user) {
        dataBinding.setUser(user);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCompetitionDetails(Competition competition) {
        long endLong = 0;
        long currentLong = 0;
        try {
            endLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(competition.getStartDate()).getTime() + 86400000;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date today = Calendar.getInstance().getTime();
            currentLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dateFormat.format(today)).getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            Intent intent = null;
            if (currentLong >= endLong) //prev
                intent = new Intent(getContext().getApplicationContext(), PrevCompDetailsActivity.class);
            else
                intent = new Intent(getContext().getApplicationContext(), CompetitionDetailsActivity.class);

            intent.putExtra("competition", competition);
            startActivity(intent);
        }
    }

    @Override
    public void setPrivateItemProgress(int index, int progress, boolean isHistory) {
        if (isHistory)
            historyAdapter.setProgressState(index, progress);
        else
            myCompetitionsAdapter.setProgressState(index, progress);

    }

    @Override
    public int getSelectedIndex(boolean isHistory) {
        return isHistory ? historyAdapter.getSelectedIndex() : myCompetitionsAdapter.getSelectedIndex();
    }

    @Override
    public void showProfilePicture() {
        String photo = dataBinding.getUser().getPhoto();
        if (photo != null) {
            if (!photo.isEmpty()) {
                ArrayList<String> photos = new ArrayList<>();
                photos.add(photo);

                new StfalconImageViewer.Builder<>(getContext(), photos, new ImageLoader<String>() {
                    @Override
                    public void loadImage(ImageView imageView, String image) {
                        Glide.with(imageView.getContext())
                                .load(image)
                                .thumbnail(0.5f)
                                .error(R.mipmap.ic_launcher)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);
                    }
                })
                        .withStartPosition(0)
                        .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                        .allowSwipeToDismiss(true)
                        .show();

            }
        }
    }

    @Override
    public void onPrivateCompetitionClicked(Competition competition, String password, int index, boolean isHistory) {
        viewModel.requestDetails(competition.getId(), isHistory, password, index);
    }

    @Override
    public void onCompetitionClicked(Competition competition) {
        // if active == 0 that means competition is prev. so send competition object in intent and navigate to prevCompDetailsActivity
        // if active == 1 that means competition is new so send competitionId and questionType in intent and navigate to CompetitionDetailsActivity
        Intent intent = new Intent(getContext(), competition.getActive() == 0 ? PrevCompDetailsActivity.class : CompetitionDetailsActivity.class);
        if (competition.getActive() == 0)
            intent.putExtra("competition", competition);
        else {
            intent.putExtra("competitionId", competition.getId());
            intent.putExtra("questionType", competition.getQuestionType());
        }
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }
}
