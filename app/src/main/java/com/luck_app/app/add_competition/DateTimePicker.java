package com.luck_app.app.add_competition;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.luck_app.app.R;
import com.luck_app.app.databinding.DateTimePickerLayoutBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimePicker extends BottomSheetDialogFragment {
    private DateTimePickerLayoutBinding dataBinding;
    private String[] days, months, hours, minutes, amPm;
    private ParentCommunicator parentListener;

    public static DateTimePicker newInstance() {
        return new DateTimePicker();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.MyBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.date_time_picker_layout, container, false);
        dataBinding.setDaysPicker(dataBinding.days);
        dataBinding.setMonthsPicker(dataBinding.months);
        dataBinding.setHoursPicker(dataBinding.hours);
        dataBinding.setMinutesPicker(dataBinding.minutes);

        parentListener = (ParentCommunicator) getContext();


        String currentDate =
                new SimpleDateFormat("yyyy MM dd hh mm a", Locale.getDefault())
                        .format(Calendar
                                .getInstance()
                                .getTime());
        String[] currentDateArr = currentDate.split(" ");

        dataBinding.setCurrentYear(Integer.valueOf(currentDateArr[0]));

        days = new String[31];
        for (int i = 0; i < 31; i++) {
            if (i + 1 < 10)
                days[i] = "0".concat(String.valueOf(i + 1));
            else
                days[i] = String.valueOf(i + 1);
        }

        dataBinding.setDaysMinValue(1);
        dataBinding.setDaysMaxValue(days.length);
        dataBinding.setDaysValue(Integer.valueOf(currentDateArr[2]));
        dataBinding.setDays(days);

        months = getResources().getStringArray(R.array.months);


        dataBinding.setMonthsMinValue(1);
        dataBinding.setMonthsMaxValue(months.length);
        dataBinding.setMonthsValue(Integer.valueOf(currentDateArr[1]));
        dataBinding.setMonths(months);

        hours = new String[12];
        for (int i = 0; i < 12; i++) {
            if (i + 1 < 10)
                hours[i] = "0".concat(String.valueOf(i + 1));
            else
                hours[i] = String.valueOf(i + 1);
        }

        dataBinding.setHoursMinValue(1);
        dataBinding.setHoursMaxValue(hours.length);
        dataBinding.setHoursValue(Integer.valueOf(currentDateArr[3]));
        dataBinding.setHours(hours);

        minutes = new String[60];
        for (int i = 0; i < 60; i++) {
            if (i < 10)
                minutes[i] = "0".concat(String.valueOf(i));
            else
                minutes[i] = String.valueOf(i);
        }

        dataBinding.setMinutesMinValue(1);
        dataBinding.setMinutesMaxValue(minutes.length);
        dataBinding.setMinutesValue(Integer.valueOf(currentDateArr[4]));
        dataBinding.setMinutes(minutes);

        amPm = new String[2];
        amPm[0] = getString(R.string.am);
        amPm[1] = getString(R.string.pm);

        dataBinding.setAmPmsMinValue(1);
        dataBinding.setAmPmsMaxValue(amPm.length);
        dataBinding.setAmPmsValue(currentDateArr[5].toLowerCase().equals(getString(R.string.am)) ? 1 : 2);
        dataBinding.setAmPms(amPm);

        dataBinding.doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedDateTime = currentDateArr[0];// year
                selectedDateTime += "-" + (dataBinding.months.getValue());// month
                selectedDateTime += "-" + days[dataBinding.days.getValue() - 1];// days
                selectedDateTime += " " + hours[dataBinding.hours.getValue() - 1];// hours
                selectedDateTime += ":" + minutes[dataBinding.minutes.getValue() - 1];// minutes
                selectedDateTime += " " + amPm[dataBinding.amPm.getValue() - 1];
                parentListener.setStartDate(convert12HTo24H(selectedDateTime));
                dismiss();
            }
        });

        return dataBinding.getRoot();
    }

    private String convert12HTo24H(String startDate) {

        String rsltDate = startDate;
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm a").parse(startDate);

            rsltDate =
                    new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
                            .format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            return rsltDate;
        }
    }



    public interface ParentCommunicator {
        void setStartDate(String startDate);
    }
}
