package com.luck_app.app.add_competition;

public interface AddPresenter {
    void onButtonClicked(int index);
}
