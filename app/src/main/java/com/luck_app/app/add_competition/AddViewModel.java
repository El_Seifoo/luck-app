package com.luck_app.app.add_competition;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.main.MainActivity;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.Gift;
import com.luck_app.app.models.Price;
import com.luck_app.app.notifications.NotificationsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class AddViewModel extends AndroidViewModel implements AddModel.ModelCallback {
    private final int PERMISSION_EXTERNAL_STORAGE = 1;
    private final int PICK_GIFT_PICTURES_REQUEST_CODE = 2;
    private final int PICK_MEDIA_QUESTION_REQUEST_CODE = 3;
    private AddModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> checkedAnswer/* flag to know which answer is the correct one */;
    private ObservableField<Integer> questionType/* flag to know the visibility of answers depending on question type */;
    private ObservableField<Integer> mediaViewerVisibility/* flag to know the visibility of media viewer depending on question type */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> priceProgress/* flag to know the status of price progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;

    private ArrayList<String> selectedGiftPhotos = new ArrayList<>();
    private ArrayList<String> selectedMediaQuestion = new ArrayList<>();


    public AddViewModel(@NonNull Application application) {
        super(application);
        model = new AddModel(application);
        progress = new ObservableField<>(View.GONE);
        priceProgress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        checkedAnswer = new ObservableField<>(-1);
        questionType = new ObservableField<>(0);// 0 means question
        mediaViewerVisibility = new ObservableField<>(View.GONE);
    }

    protected MutableLiveData<Price> requestPrice() {
        setPriceProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchPrice(getRequestHeader(false), this);
    }

    /*
        validate all user inputs then make request to create new competition

     */
    protected void requestPublishing(int competitionType, EditText competitionNameEditText, EditText startDateEditText, EditText winnersCountEditText,
                                     EditText giftNameEditText, EditText questionEditText, EditText firstAnswerEditText,
                                     EditText secondAnswerEditText, EditText thirdAnswerEditText, EditText fourthAnswerEditText,
                                     EditText conditionsEditText, EditText passwordEditText, EditText confirmPasswordEditText) {


        String competitionName = competitionNameEditText.getText().toString();
        if (competitionName.isEmpty()) {
            viewListener.showErrorMessage(competitionNameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String startDate = startDateEditText.getText().toString();
        if (startDate.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.start_date_can_not_be_blank));
            return;
        }


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        String todayString = dateFormat.format(Calendar.getInstance().getTime());
        Long diff = 0l;
        try {
            Date currentDate = dateFormat.parse(todayString);
            Date start = dateFormat.parse(startDate);

            diff = start.getTime() - currentDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (diff > 432000000) {
            viewListener.showToastMessage(getApplication().getString(R.string.start_date_must_be_before_five_days_from_now));
            return;
        }
//        if (diff <= 3600000) {
//            viewListener.showToastMessage(getApplication().getString(R.string.start_date_must_be_after_one_hour_from_now));
//            return;
//        }


        String winnersCount = winnersCountEditText.getText().toString();
        if (winnersCount.isEmpty()) {
            viewListener.showErrorMessage(winnersCountEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String giftName = giftNameEditText.getText().toString();
        if (giftName.isEmpty()) {
            viewListener.showErrorMessage(giftNameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (giftName.length() < 2) {
            viewListener.showErrorMessage(giftNameEditText, getApplication().getString(R.string.gift_length_error));
            return;
        }

        if (selectedGiftPhotos.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.please_pick_competition_gift_photo));
            return;
        }

        List<String> encodedGiftPhotos = new ArrayList<>();
        for (int i = 0; i < selectedGiftPhotos.size(); i++) {
            try {
                encodedGiftPhotos.add(encodeGiftPhotos(selectedGiftPhotos.get(i)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


        // if question type = 1 (normal) check if question is empty or not
        String question = questionEditText.getText().toString();
        if (questionType.get().intValue() == 1 && question.isEmpty()) {
            viewListener.showErrorMessage(questionEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if ((questionType.get() == 2 || questionType.get() == 3) && selectedMediaQuestion.isEmpty()) {
            viewListener.showToastMessage(questionType.get() == 2 ? getApplication().getString(R.string.please_pick_question_photo)
                    : getApplication().getString(R.string.please_pick_question_video));
            return;
        }

        String firstAnswer = firstAnswerEditText.getText().toString();
        String secondAnswer = secondAnswerEditText.getText().toString();
        String thirdAnswer = thirdAnswerEditText.getText().toString();
        String fourthAnswer = fourthAnswerEditText.getText().toString();

        // if question type = 1,2 or 3 (normal,photo or video) check answers
        if (questionType.get() != 4 && (firstAnswer.isEmpty() || secondAnswer.isEmpty() || thirdAnswer.isEmpty() || fourthAnswer.isEmpty())) {
            if (firstAnswer.isEmpty())
                viewListener.showErrorMessage(firstAnswerEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            else if (secondAnswer.isEmpty())
                viewListener.showErrorMessage(secondAnswerEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            else if (thirdAnswer.isEmpty())
                viewListener.showErrorMessage(thirdAnswerEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            else
                viewListener.showErrorMessage(fourthAnswerEditText, getApplication().getString(R.string.this_field_can_not_be_blank));

            return;
        }

        // if question type = 1,2 or 3 (normal,photo or video) check checked answers
        if (questionType.get() != 4 && checkedAnswer.get() == -1) {
            viewListener.showToastMessage(getApplication().getString(R.string.please_check_the_correct_answer));
            return;
        }

        String conditions = conditionsEditText.getText().toString();
        if (conditions.isEmpty()) {
            viewListener.showErrorMessage(conditionsEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }


        String password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();
        if (competitionType == 2) {
            if (password.isEmpty()) {
                viewListener.showErrorMessage(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
                return;
            }

            if (confirmPassword.isEmpty()) {
                viewListener.showErrorMessage(confirmPasswordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
                return;
            }

            if (!password.equals(confirmPassword)) {
                viewListener.showErrorMessage(passwordEditText, getApplication().getString(R.string.password_confirm_not_matching));
                return;
            }
        }


        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        handleRequest(competitionName, startDate + ":00"/* for seconds */, winnersCount, competitionType,
                conditions, giftName, encodedGiftPhotos, question, questionType.get().intValue(),
                firstAnswer, secondAnswer, thirdAnswer, fourthAnswer, password);
    }

    /*
        handle request body and decide which model method will be called depending on questionType and competitionType
        if competition type is private -> add password to the body

        if question type is "normal or without question" normal method without part param.
        else call method with part param. to upload photo/video question
     */
    private void handleRequest(String competitionName, String startDate, String winnersCount, int competitionType, String conditions,
                               String giftName, List<String> encodedGiftPhotos, String question, int questionType,
                               String firstAnswer, String secondAnswer, String thirdAnswer, String fourthAnswer, String password) {

        Map<String, RequestBody> body = new HashMap<>();

        body.put("name", RequestBody.create(competitionName, MultipartBody.FORM));
        body.put("start", RequestBody.create(convertArNumToEn(startDate), MultipartBody.FORM));
        body.put("winner", RequestBody.create(winnersCount, MultipartBody.FORM));
        body.put("competionType", RequestBody.create(String.valueOf(competitionType), MultipartBody.FORM));
        body.put("conditions", RequestBody.create(conditions, MultipartBody.FORM));
        body.put("giftName", RequestBody.create(giftName, MultipartBody.FORM));

        for (int i = 0; i < encodedGiftPhotos.size(); i++) {
            body.put("gift[" + i + "]", RequestBody.create(encodedGiftPhotos.get(i), MultipartBody.FORM));
        }

//        MultipartBody.Part giftPart = null;
//        File fileGift = new File(selectedGiftPhotos.get(0));
//        RequestBody fileRequestBodyGift = RequestBody.create(fileGift, MediaType.parse("image/*"));
//        giftPart = MultipartBody.Part.createFormData("gift[0]", fileGift.getName(), fileRequestBodyGift);

        body.put("type", RequestBody.create(String.valueOf(questionType), MultipartBody.FORM));
        body.put("tasbeet", RequestBody.create("0", MultipartBody.FORM));


        MultipartBody.Part part = null;
        if (questionType == 1)
            body.put("question", RequestBody.create(question, MultipartBody.FORM));
        else if (questionType == 2 || questionType == 3) {
            File file = new File(selectedMediaQuestion.get(0));
            RequestBody fileRequestBody = RequestBody.create(file, MediaType.parse("image/*"));
            part = MultipartBody.Part.createFormData("question", file.getName(), fileRequestBody);
        }

        if (questionType != 4) {
            JSONArray answers = new JSONArray();
            answers.put(getJson(firstAnswer, R.id.first_answer_check_box));
            answers.put(getJson(secondAnswer, R.id.second_answer_check_box));
            answers.put(getJson(thirdAnswer, R.id.third_answer_check_box));
            answers.put(getJson(fourthAnswer, R.id.fourth_answer_check_box));

            body.put("answers", RequestBody.create(answers.toString(), MultipartBody.FORM));
        }

        if (competitionType == 2)
            body.put("password", RequestBody.create(password, MultipartBody.FORM));

        if (questionType == 1 || questionType == 4)
            model.createCompetition(body, getRequestHeader(true), this);
        else
            model.createCompetition(getRequestHeader(true), body, part, this);

    }

    /*
        if app lang. is arabic .. numbers of start date will be arabic numbers and this will be invalid input for api
        so we convert arabic to english numbers to validate start date
     */
    private String convertArNumToEn(String data) {
        for (int i = 0; i < data.length(); i++) {
            switch (data.charAt(i)) {
                case '٠':
                    data = data.replace(data.charAt(i), '0');
                    break;
                case '١':
                    data = data.replace(data.charAt(i), '1');
                    break;
                case '٢':
                    data = data.replace(data.charAt(i), '2');
                    break;
                case '٣':
                    data = data.replace(data.charAt(i), '3');
                    break;
                case '٤':
                    data = data.replace(data.charAt(i), '4');
                    break;
                case '٥':
                    data = data.replace(data.charAt(i), '5');
                    break;
                case '٦':
                    data = data.replace(data.charAt(i), '6');
                    break;
                case '٧':
                    data = data.replace(data.charAt(i), '7');
                    break;
                case '٨':
                    data = data.replace(data.charAt(i), '8');
                    break;
                case '٩':
                    data = data.replace(data.charAt(i), '9');
                    break;
            }
        }


        return data;
    }


    /*
        convert answer into JSONObject
     */
    private JSONObject getJson(String answer, int answerId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("answer", answer);
            jsonObject.put("right", checkedAnswer.get() == answerId ? "1" : "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    /*
        convert image into base 64
     */
    private String encodeGiftPhotos(String photo) throws FileNotFoundException {
        InputStream inputStream = getApplication().getContentResolver().openInputStream(Uri.fromFile(new File(photo)));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(inputStream), 100, 100, true);
        bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.NO_WRAP);

//        String base64Image = "";
//        File file = new File(photo);
//        try (FileInputStream imageInFile = new FileInputStream(file)) {
//            byte[] imageDate = new byte[(int) file.length()];
//            imageInFile.read(imageDate);
//            base64Image = Base64.encodeToString(imageDate, Base64.DEFAULT);
//        } catch (FileNotFoundException e) {
//            Log.e("fileNotFound", e.toString());
//        } catch (IOException ioe) {
//            Log.e("IOE", ioe.toString());
//        }
//        return base64Image;
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back button
                |-> 1 : notification buton
                |-> 2 : select date-time
                |-> 3 : upload gift photos
                |-> 4 : upload Q. photo/video
                |-> 5 : publish button
                |-> 6 : retry fetching competition price req.
     */
    private int selectedIndex = 0;

    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                viewListener.navigateToDestination(NotificationsActivity.class);
                break;
            case 2:
                viewListener.showDateTimePicker();
                break;
            case 3:
            case 4:
                selectedIndex = index;
                startPickingGiftPhotos();
                break;
            case 5:
                viewListener.startPublishing();
                break;
            case 6:
                setErrorView(View.GONE);
                setPriceProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchPrice(getRequestHeader(false), this);
        }
    }

    /*
        create request header
        @Param withToken .. flag to know we have to add token or not
     */
    private Map<String, String> getRequestHeader(boolean withToken) {
        Map<String, String> header = new HashMap<>();
        if (withToken)
            header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    /*
        asking for user run time permission
     */
    private void startPickingGiftPhotos() {
        ActivityCompat.requestPermissions(viewListener.activity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_EXTERNAL_STORAGE
        );
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_EXTERNAL_STORAGE) {
            FilePickerBuilder pickerBuilder = FilePickerBuilder.getInstance()
                    // enable videos if user is picking video question
                    .enableVideoPicker(selectedIndex == 4 && questionType.get().intValue() == 3)
                    .enableDocSupport(false)
                    // enable image picker if user is picking photos for gifts or picking photo question
                    .enableImagePicker(selectedIndex == 3 || (selectedIndex == 4 && questionType.get().intValue() == 2))
                    .setSelectedFiles(selectedIndex == 3 ? selectedGiftPhotos : selectedMediaQuestion)
                    .enableSelectAll(false)
                    .showGifs(false)

                    .setActivityTheme(R.style.LibAppTheme);
            // if user is picking photo/video question
            if (selectedIndex == 4)
                pickerBuilder.setMaxCount(1);
            else pickerBuilder.setMaxCount(10);

            pickerBuilder.pickPhoto(viewListener.activity(), selectedIndex == 3 ? PICK_GIFT_PICTURES_REQUEST_CODE : PICK_MEDIA_QUESTION_REQUEST_CODE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_GIFT_PICTURES_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<String> rslt = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);
                if (rslt.isEmpty())
                    selectedGiftPhotos.clear();
                else {
                    selectedGiftPhotos.clear();
                    selectedGiftPhotos.addAll(rslt);
                }
                List<Gift> gifts = new ArrayList<>();
                Gift gift;
                for (String string : selectedGiftPhotos) {
                    gift = new Gift("ID", string);
                    gifts.add(gift);
                }
                viewListener.setGiftPhotos(gifts);
            }
            return;
        }

        if (requestCode == PICK_MEDIA_QUESTION_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<String> rslt = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);
                if (rslt.isEmpty()) {
                    selectedMediaQuestion.clear();
                    setMediaViewer(View.GONE);
                } else {
                    selectedMediaQuestion.addAll(rslt);
                    setMediaViewer(View.VISIBLE);
                }

                viewListener.setMediaQuestion(rslt.get(0));
            }
        }
    }

    // clear media question when user change question type
    public void clearMediaQuestion() {
        selectedMediaQuestion.clear();
        viewListener.setMediaQuestion("");
        setMediaViewer(View.GONE);
    }


    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(getFailureMessage(t)
                    .concat("\n")
                    .concat(getApplication().getString(R.string.tap_to_retry))
                    .concat(" ")
                    .concat(getApplication().getString(R.string.to_know_comptition_price)));
        } else
            viewListener.showToastMessage(getFailureMessage(t));
    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        handle create competition req.'s response
     */
    @Override
    public void handleCreateCompetitionResponse(Response<ApiResponse<Competition>> response) {
        int code = response.code();
        if (code == 200) {// if req. succeeded show response message .
            // if competition created move to home activity
            ApiResponse<Competition> apiResponse = response.body();
            viewListener.showToastMessage(apiResponse.getMessage());
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                Intent intent = new Intent(getApplication(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                viewListener.navigateDestination(intent);
            }
        } else if (code == 500)
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getCheckedAnswer() {
        return checkedAnswer;
    }

    public void setCheckedAnswer(int checkedAnswer) {
        this.checkedAnswer.set(checkedAnswer);
    }

    public ObservableField<Integer> getMediaViewer() {
        return mediaViewerVisibility;
    }

    public void setMediaViewer(int visibility) {
        this.mediaViewerVisibility.set(visibility);
    }

    public ObservableField<Integer> getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int type) {
        this.questionType.set(type);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getPriceProgress() {
        return priceProgress;
    }

    @Override
    public void setPriceProgress(int progress) {
        this.priceProgress.set(progress);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {
        void finishActivity();

        void navigateToDestination(Class destination);

        void showDateTimePicker();

        Activity activity();

        void setGiftPhotos(List<Gift> selectedGiftPhotos);

        void setMediaQuestion(String media);

        void startPublishing();

        void showErrorMessage(EditText editText, String error);

        void showToastMessage(String message);

        void navigateDestination(Intent intent);
    }
}
