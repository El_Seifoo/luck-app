package com.luck_app.app.add_competition;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.Price;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.HeaderMap;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public class AddModel {
    private Context context;

    public AddModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<Price> priceMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<Price> fetchPrice(Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<Price>> call = MySingleton.getInstance(context).createService().fetchCompetitionPrice(header);

        call.enqueue(new Callback<ApiResponse<Price>>() {
            @Override
            public void onResponse(Call<ApiResponse<Price>> call, Response<ApiResponse<Price>> response) {
                callback.setPriceProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<Price> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        Price price = apiResponse.getData();
                        if (price == null) callback.showResponseMessage(apiResponse.getMessage());
                        else priceMutableLiveData.setValue(price);
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else if (response.code() == 500)
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
                else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        callback.showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        callback.showResponseMessage(context.getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Price>> call, Throwable t) {
                callback.setPriceProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return priceMutableLiveData;
    }

    protected void createCompetition(Map<String, RequestBody> body , Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<Competition>> call = MySingleton.getInstance(context).createService().createCompetition(header, body);

        call.enqueue(new Callback<ApiResponse<Competition>>() {
            @Override
            public void onResponse(Call<ApiResponse<Competition>> call, Response<ApiResponse<Competition>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleCreateCompetitionResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<Competition>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }

    protected void createCompetition(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> body, @Part MultipartBody.Part file, final ModelCallback callback) {
        Call<ApiResponse<Competition>> call = MySingleton.getInstance(context).createService().createCompetition(header, body, file);

        call.enqueue(new Callback<ApiResponse<Competition>>() {
            @Override
            public void onResponse(Call<ApiResponse<Competition>> call, Response<ApiResponse<Competition>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleCreateCompetitionResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<Competition>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }


    protected interface ModelCallback {
        void setPriceProgress(int progress);

        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t, int index);

        void showResponseMessage(String message);

        void handleCreateCompetitionResponse(Response<ApiResponse<Competition>> response);
    }
}
