package com.luck_app.app.add_competition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityCompetitionTypeBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

public class CompetitionTypeActivity extends AppCompatActivity {
    private ActivityCompetitionTypeBinding dataBinding;
    private int competitionType = 1;
    private final String TYPE_KEY = "competitionTypeKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_competition_type);

        dataBinding.main.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        if (savedInstanceState != null) {
            competitionType = savedInstanceState.getInt(TYPE_KEY);
            if (competitionType == 1) {
                dataBinding.publicCheckMark.setVisibility(View.VISIBLE);
                dataBinding.privateCheckMark.setVisibility(View.GONE);
            } else {
                dataBinding.publicCheckMark.setVisibility(View.GONE);
                dataBinding.privateCheckMark.setVisibility(View.VISIBLE);
            }
        }

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.public_icon:
            case R.id.public_label:
                competitionType = 1;
                dataBinding.publicCheckMark.setVisibility(View.VISIBLE);
                dataBinding.privateCheckMark.setVisibility(View.GONE);
                break;
            case R.id.private_icon:
            case R.id.private_label:
                competitionType = 2;
                dataBinding.publicCheckMark.setVisibility(View.GONE);
                dataBinding.privateCheckMark.setVisibility(View.VISIBLE);
                break;
            case R.id.back_icon:
                finish();
                break;
            case R.id.done_icon:
                Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                intent.putExtra("competitionType", competitionType);
                startActivity(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TYPE_KEY, competitionType);
    }


}
