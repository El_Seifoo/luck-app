package com.luck_app.app.add_competition;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.luck_app.app.R;
import com.luck_app.app.competition_details.GiftsAdapter;
import com.luck_app.app.databinding.ActivityAddBinding;
import com.luck_app.app.models.Gift;
import com.luck_app.app.models.Price;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;

public class AddActivity extends AppCompatActivity implements AddViewModel.ViewListener, AddPresenter, DateTimePicker.ParentCommunicator, GiftsAdapter.GiftItemListener {
    private ActivityAddBinding dataBinding;
    private AddViewModel viewModel;
    private DateTimePicker dateTimePicker;
    private GiftsAdapter giftsAdapter;
    private RecyclerView giftsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_add);
        viewModel = new ViewModelProvider(this).get(AddViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);
        dataBinding.setFirstAnswer(dataBinding.firstAnswerCheckBox);
        dataBinding.setSecondAnswer(dataBinding.secondAnswerCheckBox);
        dataBinding.setThirdAnswer(dataBinding.thirdAnswerCheckBox);
        dataBinding.setFourthAnswer(dataBinding.fourthAnswerCheckBox);
        dataBinding.setCompetitionType(getIntent().getExtras().getInt("competitionType"));

        giftsList = dataBinding.giftPhotosList;
        giftsList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        giftsAdapter = new GiftsAdapter(this);
        giftsList.setAdapter(giftsAdapter);

        viewModel.requestPrice().observe(this, new Observer<Price>() {
            @Override
            public void onChanged(Price price) {
                dataBinding.setPrice(price);
            }
        });
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(this, destination));
    }

    @Override
    public void showDateTimePicker() {
        dateTimePicker = DateTimePicker.newInstance();
        dateTimePicker.show(getSupportFragmentManager(), "dateTimePicker");
    }

    @Override
    public Activity activity() {
        return this;
    }

    @Override
    public void setGiftPhotos(List<Gift> selectedGiftPhotos) {
        giftsAdapter.setGifts(selectedGiftPhotos);
    }

    @Override
    public void setMediaQuestion(String media) {
        dataBinding.setMedia(media);
    }

    @Override
    public void startPublishing() {
        viewModel.requestPublishing(dataBinding.getCompetitionType().intValue(), dataBinding.nameEditText, dataBinding.startEditText, dataBinding.winnersEditText,
                dataBinding.giftEditText, dataBinding.questionEditText, dataBinding.firstAnswerEditText,
                dataBinding.secondAnswerEditText, dataBinding.thirdAnswerEditText, dataBinding.fourthAnswerEditText,
                dataBinding.conditionsEditText, dataBinding.passwordEditText, dataBinding.confirmPasswordEditText);
    }

    @Override
    public void showErrorMessage(EditText editText, String error) {
        editText.setError(error);
        editText.requestFocus();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateDestination(Intent intent) {
        startActivity(intent);
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back button
                |-> 1 : notification buton
                |-> 2 : select date-time
                |-> 3 : upload gift photos
                |-> 4 : upload Q. photo/video
                |-> 5 : publish button
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setStartDate(String startDate) {
        dataBinding.startEditText.setText(startDate);
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
//        Intent fullImageIntent = new Intent(getApplicationContext(), FullScreenImageViewActivity.class);
//
//        fullImageIntent.putExtra(FullScreenImageViewActivity.URI_LIST_DATA, photos);
//
//        fullImageIntent.putExtra(FullScreenImageViewActivity.IMAGE_FULL_SCREEN_CURRENT_POS, position);
//        startActivity(fullImageIntent);
    }
}
