package com.luck_app.app.add_competition;

import com.google.gson.annotations.SerializedName;
import com.luck_app.app.models.Answer;

import java.util.List;

public class Add {
    @SerializedName("winner")
    public String winners;
    @SerializedName("tasbeet")
    public int tasbeet = 0;
    @SerializedName("question")
    public String question;
    @SerializedName("giftName")
    public String giftName;
    @SerializedName("gift[]")
    public List<String> gifts;
    @SerializedName("name")
    public String name;
    @SerializedName("start")
    public String startDate;
    @SerializedName("answers")
    public List<Answer> answers;
    @SerializedName("conditions")
    public String conditions;
    @SerializedName("type")
    public int questionType;
    @SerializedName("competionType")
    public int competionType;

    public Add(String winners, String giftName, List<String> gifts, String name, String startDate, String conditions, int questionType, int competionType) {
        this.winners = winners;
        this.giftName = giftName;
        this.gifts = gifts;
        this.name = name;
        this.startDate = startDate;
        this.conditions = conditions;
        this.questionType = questionType;
        this.competionType = competionType;
    }
}
