package com.luck_app.app.trend;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.TrendListItemBinding;
import com.luck_app.app.models.Trend;

import java.util.List;

public class TrendAdapter extends RecyclerView.Adapter<TrendAdapter.Holder> {
    private List<Trend> trends;
    private final TrendItemListener listener;

    public TrendAdapter(TrendItemListener listener) {
        this.listener = listener;
    }

    public void setTrends(List<Trend> trends) {
        this.trends = trends;
        notifyDataSetChanged();

    }

    public interface TrendItemListener {
        void onTrendItemCLicked(String userId);
    }

    @NonNull
    @Override
    public TrendAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((TrendListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.trend_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TrendAdapter.Holder holder, int position) {
        holder.dataBinding.setTrend(trends.get(position));
    }

    @Override
    public int getItemCount() {
        return trends != null ? trends.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TrendListItemBinding dataBinding;

        public Holder(@NonNull TrendListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onTrendItemCLicked(dataBinding.getTrend().getUser().getId());
        }
    }
}
