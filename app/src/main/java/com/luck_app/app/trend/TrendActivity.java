package com.luck_app.app.trend;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.islamkhsh.CardSliderViewPager;
import com.luck_app.app.R;
import com.luck_app.app.SliderAdapter;
import com.luck_app.app.databinding.ActivityTrendBinding;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.Trend;
import com.luck_app.app.trend_details.TrendDetailsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class TrendActivity extends AppCompatActivity implements TrendPresenter, TrendViewModel.ViewListener, TrendAdapter.TrendItemListener, SliderAdapter.SliderItemListener {
    private TrendViewModel viewModel;
    private TrendAdapter adapter;
    SliderAdapter sliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        ActivityTrendBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_trend);
        viewModel = new ViewModelProvider(this).get(TrendViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        RecyclerView trendList = dataBinding.trendList;
//        trendList.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new TrendAdapter(this);
        trendList.setAdapter(adapter);

        CardSliderViewPager cardSliderViewPager = dataBinding.viewPager;
        sliderAdapter = new SliderAdapter(this);
        cardSliderViewPager.setAdapter(sliderAdapter);

        viewModel.requestTrends().observe(this, new Observer<CompetitionsTrendsResponse<Trend>>() {
            @Override
            public void onChanged(CompetitionsTrendsResponse<Trend> trendCompetitionsTrendsResponse) {
                adapter.setTrends(trendCompetitionsTrendsResponse.getList());
                sliderAdapter.setImages(trendCompetitionsTrendsResponse.getSlider());
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : notification icon
                |-> 2 : retry fetching competitions request
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(this, destination));
    }

    @Override
    public void onTrendItemCLicked(String userId) {
        Intent intent = new Intent(getApplicationContext(), TrendDetailsActivity.class);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        })
                .withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();
    }
}
