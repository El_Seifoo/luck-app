package com.luck_app.app.trend;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.Trend;
import com.luck_app.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrendModel {
    private Context context;

    public TrendModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<CompetitionsTrendsResponse<Trend>> trendMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<CompetitionsTrendsResponse<Trend>> fetchTrends(Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<CompetitionsTrendsResponse<Trend>>> call = MySingleton.getInstance(context).createService().fetchTrends(header);

        call.enqueue(new Callback<ApiResponse<CompetitionsTrendsResponse<Trend>>>() {
            @Override
            public void onResponse(Call<ApiResponse<CompetitionsTrendsResponse<Trend>>> call, Response<ApiResponse<CompetitionsTrendsResponse<Trend>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<CompetitionsTrendsResponse<Trend>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        CompetitionsTrendsResponse<Trend> trendsResponse = apiResponse.getData();
                        List<Trend> trends = trendsResponse.getList();
                        List<String> slider = trendsResponse.getSlider();
                        if (trends == null && slider == null) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            callback.setSliderVisibility(View.GONE);
                        } else if (trends == null || slider == null) {
                            checkTrends(trends, callback);
                            checkSlider(slider, callback);
                            trendMutableLiveData.setValue(trendsResponse);
                        } else if (trends.isEmpty() && slider.isEmpty()) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            callback.setSliderVisibility(View.GONE);
                        } else if (trends.isEmpty() || slider.isEmpty()) {
                            checkTrends(trends, callback);
                            checkSlider(slider, callback);
                            trendMutableLiveData.setValue(trendsResponse);
                        } else {
                            callback.setEmptyListTextView(View.GONE);
                            callback.setSliderVisibility(View.VISIBLE);
                            trendMutableLiveData.setValue(trendsResponse);
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
            }

            @Override
            public void onFailure(Call<ApiResponse<CompetitionsTrendsResponse<Trend>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });

        return trendMutableLiveData;
    }

    private void checkTrends(List<Trend> list, ModelCallback callback) {
        if (list == null) callback.setEmptyListTextView(View.VISIBLE);
        else if (list.isEmpty()) callback.setEmptyListTextView(View.VISIBLE);
        else callback.setEmptyListTextView(View.GONE);
    }

    private void checkSlider(List<String> list, ModelCallback callback) {
        if (list == null) callback.setSliderVisibility(View.GONE);
        else if (list.isEmpty()) callback.setSliderVisibility(View.GONE);
        else callback.setSliderVisibility(View.VISIBLE);
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setEmptyListTextView(int empty);

        void setSliderVisibility(int visibility);

        void onFailureHandler(Throwable t);

        void showResponseMessage(String message);
    }
}
