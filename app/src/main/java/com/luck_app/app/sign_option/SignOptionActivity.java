package com.luck_app.app.sign_option;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.luck_app.app.R;
import com.luck_app.app.login.LoginActivity;
import com.luck_app.app.register.RegisterActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

public class SignOptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        setContentView(R.layout.activity_sign_option);
    }

    public void onClick(View view) {
        int id = view.getId();
        startActivity(new Intent(getApplicationContext(), id == R.id.login_label || id == R.id.login_icon ? LoginActivity.class : RegisterActivity.class));
    }
}
