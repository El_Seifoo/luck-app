package com.luck_app.app.language;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.luck_app.app.R;
import com.luck_app.app.sign_option.SignOptionActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

public class LanguageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        setContentView(R.layout.activity_language);
    }

    public void onClick(View view) {
        MySingleton.getInstance(getApplicationContext())
                .saveStringToSharedPref(Constants.APP_LANGUAGE, view.getId() == R.id.arabic_label ? getString(R.string.arabic_key) : getString(R.string.english_key));
        startActivity(new Intent(getApplicationContext(), SignOptionActivity.class));
    }
}
