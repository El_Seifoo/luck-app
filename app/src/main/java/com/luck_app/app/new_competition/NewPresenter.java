package com.luck_app.app.new_competition;

public interface NewPresenter {
    void onButtonClicked(int index);
}
