package com.luck_app.app.new_competition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.islamkhsh.CardSliderViewPager;
import com.luck_app.app.CompetitionsAdapter;
import com.luck_app.app.GridSpacingItemDecoration;
import com.luck_app.app.R;
import com.luck_app.app.SliderAdapter;
import com.luck_app.app.competition_details.CompetitionDetailsActivity;
import com.luck_app.app.databinding.ActivityNewBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;

public class NewActivity extends AppCompatActivity implements NewViewModel.ViewListener, NewPresenter, CompetitionsAdapter.CompetitionItemListener, SliderAdapter.SliderItemListener {
    private NewViewModel viewModel;
    private CompetitionsAdapter adapter;
    SliderAdapter sliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        ActivityNewBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_new);
        viewModel = new ViewModelProvider(this).get(NewViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        RecyclerView newList = dataBinding.newList;
        adapter = new CompetitionsAdapter(this);
        newList.setAdapter(adapter);


        CardSliderViewPager cardSliderViewPager = dataBinding.viewPager;
        sliderAdapter = new SliderAdapter(this);
        cardSliderViewPager.setAdapter(sliderAdapter);

        viewModel.requestCompetitions().observe(this, new Observer<CompetitionsTrendsResponse<Competition>>() {
            @Override
            public void onChanged(CompetitionsTrendsResponse<Competition> competitionCompetitionsTrendsResponse) {
                adapter.setCompetitions(competitionCompetitionsTrendsResponse.getList());
                sliderAdapter.setImages(competitionCompetitionsTrendsResponse.getSlider());
            }
        });
    }


    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(this, destination));
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : notification icon
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void onCompetitionClicked(Competition competition) {
        Intent intent = new Intent(getApplicationContext(), CompetitionDetailsActivity.class);
        intent.putExtra("competitionId", competition.getId());
        intent.putExtra("questionType", competition.getQuestionType());
        startActivity(intent);
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        })
                .withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();
    }
}
