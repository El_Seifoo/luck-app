package com.luck_app.app.new_competition;

import android.app.Application;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.notifications.NotificationsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewViewModel extends AndroidViewModel implements NewModel.ModelCallback {
    private NewModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyListTextView /* flag to know if competitions list is empty or not */;
    private ObservableField<Integer> sliderVisibility /* flag to know if slider is visible or no */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;

    public NewViewModel(@NonNull Application application) {
        super(application);
        model = new NewModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        sliderVisibility = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
    }

    /*
        Call model fun. fetchCompetitions to get new competitions
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<CompetitionsTrendsResponse<Competition>> requestCompetitions() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchCompetitions(getRequestHeader(), this);
    }


    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : notification icon
                |-> 2 : retry fetching competitions request
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                viewListener.navigateToDestination(NotificationsActivity.class);
                break;
            case 2:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchCompetitions(getRequestHeader(), this);

        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getSliderVisibility() {
        return sliderVisibility;
    }

    @Override
    public void setSliderVisibility(int visibility) {
        this.sliderVisibility.set(visibility);
    }


    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {
        void finishActivity();

        void navigateToDestination(Class destination);
    }

}
