package com.luck_app.app.new_competition;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionResponse;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.Trend;
import com.luck_app.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewModel {
    private Context context;

    public NewModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<CompetitionsTrendsResponse<Competition>> competitionMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<CompetitionsTrendsResponse<Competition>> fetchCompetitions(Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<CompetitionsTrendsResponse<Competition>>> call = MySingleton.getInstance(context).createService().fetchNewCompetitions(header);

        call.enqueue(new Callback<ApiResponse<CompetitionsTrendsResponse<Competition>>>() {
            @Override
            public void onResponse(Call<ApiResponse<CompetitionsTrendsResponse<Competition>>> call, Response<ApiResponse<CompetitionsTrendsResponse<Competition>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<CompetitionsTrendsResponse<Competition>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        CompetitionsTrendsResponse<Competition> competitionsResponse = apiResponse.getData();
                        List<Competition> competitions = competitionsResponse.getList();
                        List<String> slider = competitionsResponse.getSlider();
                        if (competitions == null && slider == null) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            callback.setSliderVisibility(View.GONE);
                        } else if (competitions == null || slider == null) {
                            checkCompetitions(competitions, callback);
                            checkSlider(slider, callback);
                            competitionMutableLiveData.setValue(competitionsResponse);
                        } else if (competitions.isEmpty() && slider.isEmpty()) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            callback.setSliderVisibility(View.GONE);
                        } else if (competitions.isEmpty() || slider.isEmpty()) {
                            checkCompetitions(competitions, callback);
                            checkSlider(slider, callback);
                            competitionMutableLiveData.setValue(competitionsResponse);
                        } else {
                            callback.setEmptyListTextView(View.GONE);
                            callback.setSliderVisibility(View.VISIBLE);
                            competitionMutableLiveData.setValue(competitionsResponse);
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
            }

            @Override
            public void onFailure(Call<ApiResponse<CompetitionsTrendsResponse<Competition>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });

        return competitionMutableLiveData;
    }

    private void checkCompetitions(List<Competition> list, ModelCallback callback) {
        if (list == null) callback.setEmptyListTextView(View.VISIBLE);
        else if (list.isEmpty()) callback.setEmptyListTextView(View.VISIBLE);
        else callback.setEmptyListTextView(View.GONE);
    }

    private void checkSlider(List<String> list, ModelCallback callback) {
        if (list == null) callback.setSliderVisibility(View.GONE);
        else if (list.isEmpty()) callback.setSliderVisibility(View.GONE);
        else callback.setSliderVisibility(View.VISIBLE);
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setEmptyListTextView(int empty);

        void setSliderVisibility(int visibility);

        void onFailureHandler(Throwable t);

        void showResponseMessage(String message);
    }
}
