package com.luck_app.app.previous_competition_details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.luck_app.app.R;
import com.luck_app.app.competition_details.GiftsAdapter;
import com.luck_app.app.databinding.ActivityPrevCompDetailsBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.Gift;
import com.luck_app.app.models.Winner;
import com.luck_app.app.send_mail.SendMailDialogFragment;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class PrevCompDetailsActivity extends AppCompatActivity implements PrevCompDetailsViewModel.ViewListener, PrevCompDetailsPresenter, GiftsAdapter.GiftItemListener {
    private ActivityPrevCompDetailsBinding dataBinding;
    private PrevCompDetailsViewModel viewModel;
    private RecyclerView winnersList, giftsList;
    private WinnersAdapter winnersAdapter;
    private GiftsAdapter giftsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_prev_comp_details);
        viewModel = new ViewModelProvider(this).get(PrevCompDetailsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);
        dataBinding.setCompetition((Competition) getIntent().getSerializableExtra("competition"));

        viewModel.setShareViewVisibility(dataBinding.getCompetition().getCompetitionType());


        giftsList = dataBinding.giftsList;
        giftsList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        giftsAdapter = new GiftsAdapter(this);
        giftsList.setAdapter(giftsAdapter);

        viewModel.getGifts(dataBinding.getCompetition().getGifts()).observe(this, new Observer<List<Gift>>() {
            @Override
            public void onChanged(List<Gift> gifts) {
                giftsAdapter.setGifts(gifts);
            }
        });


        winnersList = dataBinding.winnersList;
        winnersList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        winnersAdapter = new WinnersAdapter();
        winnersList.setAdapter(winnersAdapter);

        viewModel.fetchWinners(dataBinding.getCompetition().getId()).observe(this, new Observer<List<Winner>>() {
            @Override
            public void onChanged(List<Winner> winners) {
                winnersAdapter.setWinners(winners);
            }
        });

    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : share icon
                |-> 2 : report
                |-> 3 : retry fetching winners list
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public String getCompetitionId() {
        return dataBinding.getCompetition().getId();
    }

    @Override
    public void showReportDialogFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null)
            fragmentTransaction.remove(prev);

        fragmentTransaction.addToBackStack(null);

        DialogFragment dialogFragment = SendMailDialogFragment.newInstance(dataBinding.getCompetition().getId());
        dialogFragment.show(fragmentTransaction, "dialog");
    }


    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        })
                .withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();
    }
}
