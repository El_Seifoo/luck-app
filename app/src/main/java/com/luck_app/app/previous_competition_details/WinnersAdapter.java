package com.luck_app.app.previous_competition_details;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.WinnersListItemBinding;
import com.luck_app.app.models.Winner;

import java.util.List;

public class WinnersAdapter extends RecyclerView.Adapter<WinnersAdapter.Holder> {
    private List<Winner> winners;

    public void setWinners(List<Winner> winners) {
        this.winners = winners;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((WinnersListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.winners_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setWinner(winners.get(position));
    }

    @Override
    public int getItemCount() {
        return winners != null ? winners.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private WinnersListItemBinding dataBinding;

        public Holder(WinnersListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }
    }
}
