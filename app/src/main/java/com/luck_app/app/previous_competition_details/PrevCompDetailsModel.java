package com.luck_app.app.previous_competition_details;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Winner;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrevCompDetailsModel {
    private Context context;

    public PrevCompDetailsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<Winner>> winnersMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Winner>> fetchWinners(String id, Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<List<Winner>>> call = MySingleton.getInstance(context).createService().fetchWinners(header, id);

        call.enqueue(new Callback<ApiResponse<List<Winner>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Winner>>> call, Response<ApiResponse<List<Winner>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<List<Winner>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        List<Winner> winners = apiResponse.getData();
                        if (winners == null)
                            callback.setEmptyWinnersListTextView(View.VISIBLE);
                        else if (winners.isEmpty())
                            callback.setEmptyWinnersListTextView(View.VISIBLE);
                        else {
                            callback.setEmptyWinnersListTextView(View.GONE);
                            winnersMutableLiveData.setValue(winners);
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage(), 0);
                } else if (response.code() == 500)
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data), 0);
                else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        callback.showResponseMessage(jsonObject.getString("message"), 0);
                    } catch (Exception e) {
                        callback.showResponseMessage(context.getString(R.string.error_sending_data), 0);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Winner>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return winnersMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setEmptyWinnersListTextView(int empty);

        void onFailureHandler(Throwable t, int index);

        void showResponseMessage(String message, int index);
    }
}
