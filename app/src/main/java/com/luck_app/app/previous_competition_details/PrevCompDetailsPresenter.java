package com.luck_app.app.previous_competition_details;

public interface PrevCompDetailsPresenter {
    void onButtonClicked(int index);
}
