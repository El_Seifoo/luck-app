package com.luck_app.app.previous_competition_details;

import android.app.Application;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.Gift;
import com.luck_app.app.models.Winner;
import com.luck_app.app.notifications.NotificationsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrevCompDetailsViewModel extends AndroidViewModel implements PrevCompDetailsModel.ModelCallback {
    private PrevCompDetailsModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyWinnersListTextView /* flag to know if winners list is empty or not */;
    private ObservableField<Integer> emptyListTextView /* flag to know if gifts list is empty or not */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<Integer> shareView/* flag to know if share view is visible or not */;


    public PrevCompDetailsViewModel(@NonNull Application application) {
        super(application);
        model = new PrevCompDetailsModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyWinnersListTextView = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        shareView = new ObservableField<>(View.INVISIBLE);
    }

    private Intent getShareIntent() {
        Intent share = new Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, getApplication().getString(R.string.share_private_competition_link, viewListener.getCompetitionId()));

        return share;
    }

    public void setShareViewVisibility(int competitionType) {
        if (getShareIntent().resolveActivity(getApplication().getPackageManager()) == null)
            setShareView(View.INVISIBLE);
        else setShareView(competitionType == 1 ? View.INVISIBLE : View.VISIBLE);
    }

    protected MutableLiveData<List<Winner>> fetchWinners(String id) {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchWinners(id, getRequestHeader(), this);
    }

    private MutableLiveData<List<Gift>> giftsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Gift>> getGifts(List<Gift> gifts) {
        if (gifts == null)
            setEmptyListTextView(View.VISIBLE);
        else if (gifts.isEmpty())
            setEmptyListTextView(View.VISIBLE);
        else {
            setEmptyListTextView(View.GONE);
            giftsMutableLiveData.setValue(gifts);
        }

        return giftsMutableLiveData;
    }


    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(getFailureMessage(t));
        } else {
            viewListener.showToastMessage(getFailureMessage(t));
        }
    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    @Override
    public void showResponseMessage(String message, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(message);
        } else
            viewListener.showToastMessage(message);
    }


    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    /*
       handle action of every button in the activity
       @Param index .. flag to know which button was clicked
               |-> 0 : back icon
               |-> 1 : share icon
               |-> 2 : report
               |-> 3 : retry fetching winners list
    */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                // start sharing ...
                viewListener.navigateToDestination(getShareIntent());
                break;
            case 2:
                viewListener.showReportDialogFragment();
                break;
            case 3:
                // retry fetching winners details
                setErrorView(View.GONE);
                setErrorView(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchWinners(viewListener.getCompetitionId(), getRequestHeader(), this);
        }
    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getShareView() {
        return shareView;
    }

    public void setShareView(int shareView) {
        this.shareView.set(shareView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<Integer> getEmptyWinnersListTextView() {
        return emptyWinnersListTextView;
    }

    @Override
    public void setEmptyWinnersListTextView(int empty) {
        this.emptyWinnersListTextView.set(empty);
    }

    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {
        void finishActivity();

        void showToastMessage(String message);

        void navigateToDestination(Intent intent);

        String getCompetitionId();

        void showReportDialogFragment();

    }
}
