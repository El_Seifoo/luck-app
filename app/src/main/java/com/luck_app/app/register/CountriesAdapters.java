package com.luck_app.app.register;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.CountriesListItemBinding;
import com.luck_app.app.models.Country;

import java.util.List;

public class CountriesAdapters extends RecyclerView.Adapter<CountriesAdapters.Holder> {
    private List<Country> countries;
    private final ItemListener listener;

    public CountriesAdapters(List<Country> countries, ItemListener listener) {
        this.countries = countries;
        notifyDataSetChanged();
        this.listener = listener;
    }

    protected interface ItemListener {
        void onItemClickListener(Country country);
    }

    @NonNull
    @Override
    public CountriesAdapters.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CountriesListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.countries_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CountriesAdapters.Holder holder, int position) {
        holder.dataBinding.setCountry(countries.get(position));
    }

    @Override
    public int getItemCount() {
        return countries != null ? countries.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CountriesListItemBinding dataBinding;

        public Holder(@NonNull CountriesListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;

            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClickListener(dataBinding.getCountry());
        }
    }
}
