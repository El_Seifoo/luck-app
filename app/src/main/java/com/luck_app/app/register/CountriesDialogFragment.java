package com.luck_app.app.register;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.CountriesDialogFragmentLayoutBinding;
import com.luck_app.app.models.Country;

import java.io.Serializable;
import java.util.List;

public class CountriesDialogFragment extends DialogFragment implements CountriesAdapters.ItemListener {
    private CountriesDialogFragmentLayoutBinding dataBinding;
    private RecyclerView countriesList;
    private ParentListener listener /* communicator betn. dialog and its parent (RegisterActivity) */;

    public static CountriesDialogFragment newInstance(List<Country> countries) {
        CountriesDialogFragment fragment = new CountriesDialogFragment();

        Bundle args = new Bundle();
        args.putSerializable("countries", (Serializable) countries);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.8), (int) (getResources().getDisplayMetrics().heightPixels * 0.7));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.countries_dialog_fragment_layout, container, false);

        listener = (ParentListener) getActivity();


        List<Country> countries = (List<Country>) getArguments().getSerializable("countries");
        if (!countries.isEmpty()) {
            dataBinding.emptyListTextView.setVisibility(View.GONE);
            countriesList = dataBinding.countriesList;
            countriesList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            countriesList.setAdapter(new CountriesAdapters(countries, this));
        } else dataBinding.emptyListTextView.setVisibility(View.VISIBLE);


        return dataBinding.getRoot();
    }

    @Override
    public void onItemClickListener(Country country) {
        listener.setCountry(country);
        dismiss();
    }

    public interface ParentListener {
        void setCountry(Country country);
    }
}
