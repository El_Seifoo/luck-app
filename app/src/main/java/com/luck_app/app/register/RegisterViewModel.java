package com.luck_app.app.register;

import android.app.Application;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.login.LoginActivity;
import com.luck_app.app.verification.VerificationActivity;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Country;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.terms_condition.TermsConditionsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

public class RegisterViewModel extends AndroidViewModel implements RegisterModel.ModelCallback {
    private RegisterModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<Country> country/* carry selected country */;

    public RegisterViewModel(@NonNull Application application) {
        super(application);
        model = new RegisterModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        country = new ObservableField<>();
    }

    /*
        Call model fun. fetchCountries to get all countries
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<List<Country>> requestCountries() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchCountries(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)), this);
    }

    /*
        handle error response of requests
        @Param index .. flag to know the request of the response
                |-> 0 : countries req.
                |-> 1 : register req.
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        switch (index) {
            case 0:
                setErrorView(View.VISIBLE);
                setErrorMessage(t instanceof IOException ?
                        getApplication().getString(R.string.no_internet_connection) :
                        getApplication().getString(R.string.error_fetching_data));
                break;
            case 1:///// NOT USED GOT TO VERIFICATION ACTIVITY \\\\\
                viewListener.showToastMessage(t instanceof IOException ?
                        getApplication().getString(R.string.no_internet_connection) :
                        getApplication().getString(R.string.error_fetching_data));
                break;
        }
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        validate all user inputs ..
        then call model method to make register req.
     */
    public void requestRegister(EditText usernameEditText, EditText emailEditText, EditText phoneEditText, EditText passwordEditText, EditText confirmPasswordEditText, boolean termsIsChecked) {
        if (getCountry().get().getId().equals("-1")) {
            viewListener.showToastMessage(getApplication().getString(R.string.no_countries_available_now_try_again_later));
            return;
        }

        String userName = usernameEditText.getText().toString().trim();
        if (userName.isEmpty()) {
            viewListener.showEditTextError(usernameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        String phone = phoneEditText.getText().toString().trim();
        if (phone.isEmpty()) {
            viewListener.showEditTextError(phoneEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (getCountry().get() == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.please_select_your_country));
            return;
        }

        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.password_length_error));
            return;
        }

        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        if (confirmPassword.isEmpty()) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!password.equals(confirmPassword)) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.password_confirm_not_matching));
            return;
        }

        if (!termsIsChecked) {
            viewListener.showToastMessage(getApplication().getString(R.string.terms_conditions_error_message));
            return;
        }


        Intent intent = new Intent(getApplication(), VerificationActivity.class);
        intent.putExtra("user", new UserLoginRegister(userName, email, phone, getCountry().get().getCode(), password));
        viewListener.navigateToDestination(intent);
//        setProgress(View.VISIBLE);
//        setButtonsClickable(false);
//        model.register(new UserLoginRegister(userName, email, phone, getCountry().get().getCode(), password),
//                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
//                this);
    }

    /*
        check email validation using RE
     */
    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    /*
        ///// NOT USED GOT TO VERIFICATION ACTIVITY \\\\\
        handle register req.'s response

     */
    @Override
    public void handleRegisterResponse(Response<ApiResponse<String>> response) {
        int code = response.code();
        if (code == 200) {
            ApiResponse<String> registerResponse = response.body();
            if (registerResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                Intent intent = new Intent(getApplication(), LoginActivity.class);
                intent.putExtra("email", viewListener.getEmail());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                viewListener.navigateToDestination(intent);
            } else viewListener.showToastMessage(registerResponse.getMessage());
        } else if (code == 500) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        } else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : terms & conditions
                |-> 1 : register button
                |-> 2 : countries edit text
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                Intent intent = new Intent(getApplication(), TermsConditionsActivity.class);
                viewListener.navigateToDestination(intent);
                break;
            case 1:
                viewListener.startRegistering();
                break;
            case 2:
                viewListener.showCountriesDialog();
        }

    }

    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Country> getCountry() {
        return country;
    }

    @Override
    public void setCountry(Country country) {
        this.country.set(country);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {

        void showToastMessage(String message);

        void navigateToDestination(Intent intent);

        void showCountriesDialog();

        void startRegistering();

        void showEditTextError(EditText editText, String message);

        String getEmail();
    }
}
