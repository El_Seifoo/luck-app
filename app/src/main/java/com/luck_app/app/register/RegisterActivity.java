package com.luck_app.app.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityRegisterBinding;
import com.luck_app.app.models.Country;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.util.List;


public class RegisterActivity extends AppCompatActivity implements RegisterViewModel.ViewListener, RegisterPresenter, CountriesDialogFragment.ParentListener {
    private ActivityRegisterBinding dataBinding;
    private RegisterViewModel viewModel;
    private List<Country> countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setElevation(0);
        actionBar.setDisplayHomeAsUpEnabled(true);

        viewModel.requestCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countriesList) {
                countries = countriesList;
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : terms & conditions
                |-> 1 : register button
                |-> 2 : countries edit text
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showCountriesDialog() {
        if (countries != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null)
                fragmentTransaction.remove(prev);

            fragmentTransaction.addToBackStack(null);

            DialogFragment dialogFragment = CountriesDialogFragment.newInstance(countries);
            dialogFragment.show(fragmentTransaction, "dialog");
        }

    }

    @Override
    public void startRegistering() {
        viewModel.requestRegister(dataBinding.usernameEditText, dataBinding.emailEditText, dataBinding.phoneEditText, dataBinding.passwordEditText, dataBinding.confirmPasswordEditText, dataBinding.agreeCheckBox.isChecked());
    }

    @Override
    public void showEditTextError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    @Override
    public String getEmail() {
        return dataBinding.emailEditText.getText().toString();
    }

    @Override
    public void setCountry(Country country) {
        viewModel.setCountry(country);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
