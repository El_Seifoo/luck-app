package com.luck_app.app.register;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Country;
import com.luck_app.app.models.UserLoginRegister;
import com.luck_app.app.utils.MySingleton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterModel {
    private Context context;

    public RegisterModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<Country>> countriesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Country>> fetchCountries(String lang, final ModelCallback callback) {
        Call<ApiResponse<List<Country>>> call = MySingleton.getInstance(context).createService().fetchCountries(lang);
        call.enqueue(new Callback<ApiResponse<List<Country>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Country>>> call, Response<ApiResponse<List<Country>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);

                if (response.code() == 200) {
                    ApiResponse<List<Country>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        List<Country> countries = apiResponse.getData();
                        if (countries == null) {
                            callback.setCountry(new Country("-1", context.getString(R.string.no_countries), ""));
                        } else if (countries.isEmpty()) {
                            callback.setCountry(new Country("-1", context.getString(R.string.no_countries), ""));
                        } else {
                            callback.setCountry(countries.get(0));
                            countriesMutableLiveData.setValue(countries);
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));

            }

            @Override
            public void onFailure(Call<ApiResponse<List<Country>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return countriesMutableLiveData;
    }

    ///// NOT USED GOT TO VERIFICATION ACTIVITY \\\\\
    ///// NOT USED GOT TO VERIFICATION ACTIVITY \\\\\
    protected void register(UserLoginRegister user, String lang, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().register(lang, user);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleRegisterResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t, int index);

        void showResponseMessage(String message);

        void setCountry(Country country);

        void handleRegisterResponse(Response<ApiResponse<String>> response);
    }
}
