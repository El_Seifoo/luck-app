package com.luck_app.app.edit_profile;

public interface EditProfilePresenter {
    void onButtonClicked(int index);
}
