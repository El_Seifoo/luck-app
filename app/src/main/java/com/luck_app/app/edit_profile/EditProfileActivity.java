package com.luck_app.app.edit_profile;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ActivityEditProfileBinding;
import com.luck_app.app.edit_profile.change_password.ChangePassDialogFragment;
import com.luck_app.app.models.Country;
import com.luck_app.app.models.User;
import com.luck_app.app.register.CountriesDialogFragment;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.util.List;

public class EditProfileActivity extends AppCompatActivity implements EditProfileViewModel.ViewListener, EditProfilePresenter, CountriesDialogFragment.ParentListener {
    private ActivityEditProfileBinding dataBinding;
    private EditProfileViewModel viewModel;
    private List<Country> countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        viewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);
        User user = (User) getIntent().getSerializableExtra("user");
        dataBinding.setUser(user);

        User user1 = user.Copy();
        viewModel.setCurrentUserData(user1);
        viewModel.setCountry(new Country("fake", user.getCountryCode(), "fake"));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setElevation(0);
        actionBar.setDisplayHomeAsUpEnabled(true);

        viewModel.requestCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countriesList) {
                countries = countriesList;
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : pick photo
                |-> 1 : pick country
                |-> 2 : change password
                |-> 3 : edit profile
                |-> 4 : retry fetching countries
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showDialogFragment(int index) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(index == 1 ? "countriesDialog" : "changePassDialog");
        if (prev != null)
            fragmentTransaction.remove(prev);

        fragmentTransaction.addToBackStack(null);

        if (index == 1) {
            if (countries != null) {
                DialogFragment dialogFragment = CountriesDialogFragment.newInstance(countries);
                dialogFragment.show(fragmentTransaction, "countriesDialog");
            }
        } else {
            DialogFragment dialogFragment = ChangePassDialogFragment.newInstance();
            dialogFragment.show(fragmentTransaction, "changePassDialog");
        }
    }

    @Override
    public void startEditing() {
        viewModel.requestEdit(dataBinding.usernameEditText, dataBinding.emailEditText, dataBinding.phoneEditText, dataBinding.bioEditText);
    }

    @Override
    public void showEditTextError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    @Override
    public void editUserDone(User user) {
        showToastMessage(getString(R.string.profile_updated_successfully));
        Intent intent = getIntent();
        intent.putExtra("user", user);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public User getUpdatedUserData() {
        return dataBinding.getUser();
    }

    @Override
    public Activity activity() {
        return this;
    }

    @Override
    public void setPickedPicture(String path) {
        dataBinding.getUser().setPhoto(path);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setCountry(Country country) {
        viewModel.setCountry(country);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }
}
