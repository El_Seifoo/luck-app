package com.luck_app.app.edit_profile.change_password;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.luck_app.app.R;
import com.luck_app.app.databinding.ChangePasswordDialogFragmentLayoutBinding;

public class ChangePassDialogFragment extends DialogFragment implements ChangePassPresenter, ChangePassViewModel.ViewListener {
    private ChangePasswordDialogFragmentLayoutBinding dataBinding;
    private ChangePassViewModel viewModel;

    public static ChangePassDialogFragment newInstance() {
        return new ChangePassDialogFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.change_password_dialog_fragment_layout, container, false);
        viewModel = new ViewModelProvider(this).get(ChangePassViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        return dataBinding.getRoot();
    }

    @Override
    public void onChangeButtonClicked() {
        viewModel.requestChangePass(dataBinding.passwordEditText, dataBinding.newPasswordEditText, dataBinding.confirmPasswordEditText);
    }

    @Override
    public void showEditTextError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void changePassDone(String message) {
        showToastMessage(message);
        dismiss();
    }
}
