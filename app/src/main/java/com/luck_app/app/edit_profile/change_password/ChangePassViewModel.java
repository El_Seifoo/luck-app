package com.luck_app.app.edit_profile.change_password;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class ChangePassViewModel extends AndroidViewModel implements ChangePassModel.ModelCallback {
    private ChangePassModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    protected ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;

    public ChangePassViewModel(@NonNull Application application) {
        super(application);
        model = new ChangePassModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        validate all user inputs ..
        then call model method to make changePass req.
     */
    protected void requestChangePass(EditText passwordEditText, EditText newPasswordEditText, EditText confirmPasswordEditText) {
        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }


        String newPassword = newPasswordEditText.getText().toString().trim();
        if (newPassword.isEmpty()) {
            viewListener.showEditTextError(newPasswordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (newPassword.length() < 6) {
            viewListener.showEditTextError(newPasswordEditText, getApplication().getString(R.string.password_length_error));
            return;
        }

        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        if (confirmPassword.isEmpty()) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!newPassword.equals(confirmPassword)) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.password_confirm_not_matching));
            return;
        }


        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));

        Map<String, String> body = new HashMap<>();
        body.put("old_password", password);
        // use new pass. twice because I compared new/confirm pass before
        body.put("password", newPassword);
        body.put("password_confirmation", newPassword);


        model.changePass(header, body, this);

    }


    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    @Override
    public void handleChangePassResponse(Response<ApiResponse<User>> response) {
        int code = response.code();
        if (code == 200) {
            ApiResponse<User> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                viewListener.changePassDone(getApplication().getString(R.string.your_password_changed_successfully));
            } else viewListener.showToastMessage(apiResponse.getMessage());
        } else if (code == 500) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        } else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {

        void showEditTextError(EditText editText, String message);

        void showToastMessage(String message);

        void changePassDone(String message);
    }
}
