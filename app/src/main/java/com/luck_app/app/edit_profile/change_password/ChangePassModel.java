package com.luck_app.app.edit_profile.change_password;

import android.content.Context;
import android.view.View;

import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.User;
import com.luck_app.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassModel {
    private Context context;

    public ChangePassModel(Context context) {
        this.context = context;
    }

    protected void changePass(Map<String, String> header, Map<String, String> body, final ModelCallback callback) {
        Call<ApiResponse<User>> call = MySingleton.getInstance(context).createService().changePassword(header, body);

        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleChangePassResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleChangePassResponse(Response<ApiResponse<User>> response);
    }
}
