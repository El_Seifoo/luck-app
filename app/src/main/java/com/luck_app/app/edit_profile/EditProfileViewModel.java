package com.luck_app.app.edit_profile;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Country;
import com.luck_app.app.models.User;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;

public class EditProfileViewModel extends AndroidViewModel implements EditProfileModel.ModelCallback {
    private final int PERMISSION_EXTERNAL_STORAGE = 1;
    private final int PICK_PROFILE_PICTURE_REQUEST_CODE = 2;
    private EditProfileModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<User> currentUserData;
    private ObservableField<Country> country/* carry selected country */;
    private ArrayList<String> profilePicture = new ArrayList<>();

    public EditProfileViewModel(@NonNull Application application) {
        super(application);
        model = new EditProfileModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        currentUserData = new ObservableField<>();
        country = new ObservableField<>();
    }

    /*
        Call model fun. fetchCountries to get all countries
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<List<Country>> requestCountries() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchCountries(
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                this);
    }


    /*
       handle error response of requests
       @Param index .. flag to know the request of the response
               |-> 0 : countries req.
               |-> 1 : editProfile req.
    */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        switch (index) {
            case 0:
                setErrorView(View.VISIBLE);
                setErrorMessage(getFailureMessage(t));
                break;
            case 1:
                viewListener.showToastMessage(getFailureMessage(t));
        }
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    /*
        validate all user inputs ..
        then call model method to make editProfile req.
     */
    public void requestEdit(EditText usernameEditText, EditText emailEditText, EditText phoneEditText, EditText bioEditText) {

        String userName = usernameEditText.getText().toString().trim();
        if (userName.isEmpty()) {
            viewListener.showEditTextError(usernameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        String phone = phoneEditText.getText().toString().trim();
        if (phone.isEmpty()) {
            viewListener.showEditTextError(phoneEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (getCountry().get() == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.please_select_your_country));
            return;
        }

        String bio = bioEditText.getText().toString().trim();
        if (bio.isEmpty()) {
            viewListener.showEditTextError(bioEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }


        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        if (profilePicture.isEmpty()) {
            Map<String, Object> body = new HashMap<>();

            body.put("username", userName);
            body.put("email", email);
            body.put("phoneNumber", phone);
            body.put("country", country.get().getCode());
            body.put("bio", bio);
            model.update(getRequestHeader(), body, this);
        } else {
            Map<String, RequestBody> body = new HashMap<>();

            body.put("username", RequestBody.create(userName, MultipartBody.FORM));
            body.put("email", RequestBody.create(email, MultipartBody.FORM));
            body.put("phoneNumber", RequestBody.create(phone, MultipartBody.FORM));
            body.put("country", RequestBody.create(country.get().getCode(), MultipartBody.FORM));
            body.put("bio", RequestBody.create(bio, MultipartBody.FORM));

            File file = new File(profilePicture.get(0));
            RequestBody fileRequestBody = RequestBody.create(file, MediaType.parse("image/*"));
            MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), fileRequestBody);

            model.update(getRequestHeader(), body, part, this);
        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    /*
        check email validation using RE
     */
    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    /*
        handle register req.'s response
        @Param code : response code
     */
    @Override
    public void handleEditProfileResponse(Response<ApiResponse<User>> response) {
        int code = response.code();
        if (code == 200) {
            ApiResponse<User> registerResponse = response.body();
            if (registerResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                viewListener.editUserDone(registerResponse.getData());
            } else viewListener.showToastMessage(registerResponse.getMessage());
        } else if (code == 500) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
        } else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : pick photo
                |-> 1 : pick country
                |-> 2 : change password
                |-> 3 : edit profile
                |-> 4 : retry fetching countries
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                startPickingPicture();
                break;
            case 1:
            case 2:
                viewListener.showDialogFragment(index);
                break;
            case 3:
                if (viewListener.getUpdatedUserData().equals(currentUserData.get()) && currentUserData.get().getCountryCode().equals(country.get().getCode()))// updated/current user data are the same
                    viewListener.showToastMessage(getApplication().getString(R.string.no_changes_to_save));
                else viewListener.startEditing();
                break;
            case 4:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchCountries(
                        MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                        this);
        }

    }

    /*
        Asking for user run time permission
     */
    private void startPickingPicture() {
        ActivityCompat.requestPermissions(viewListener.activity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_EXTERNAL_STORAGE
        );
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_EXTERNAL_STORAGE) {
            FilePickerBuilder.getInstance()
                    .enableVideoPicker(false)
                    .enableDocSupport(false)
                    .enableImagePicker(true)
                    .setMaxCount(1)
                    .setSelectedFiles(profilePicture)
                    .setActivityTheme(R.style.LibAppTheme)
                    .pickPhoto(viewListener.activity(), PICK_PROFILE_PICTURE_REQUEST_CODE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_PROFILE_PICTURE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<String> rslt = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);
                if (rslt.isEmpty()) {
                    profilePicture.clear();
                    viewListener.setPickedPicture("");
                } else {
                    profilePicture.clear();
                    profilePicture.addAll(rslt);
                    viewListener.setPickedPicture(profilePicture.get(0));
                }

            }
        }

    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }


    public ObservableField<User> getCurrentUserData() {
        return currentUserData;
    }

    public void setCurrentUserData(User user) {
        this.currentUserData.set(user);
    }

    public ObservableField<Country> getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country.set(country);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END

    protected interface ViewListener {
        void showToastMessage(String message);

        void navigateToDestination(Intent intent);

        void showDialogFragment(int index);

        void startEditing();

        void showEditTextError(EditText editText, String message);

        void editUserDone(User user);

        User getUpdatedUserData();

        Activity activity();

        void setPickedPicture(String path);

    }
}
