package com.luck_app.app.edit_profile;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Country;
import com.luck_app.app.models.User;
import com.luck_app.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileModel {
    private Context context;

    public EditProfileModel(Context context) {
        this.context = context;
    }


    private MutableLiveData<List<Country>> countriesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Country>> fetchCountries(String lang, final ModelCallback callback) {
        Call<ApiResponse<List<Country>>> call = MySingleton.getInstance(context).createService().fetchCountries(lang);
        call.enqueue(new Callback<ApiResponse<List<Country>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Country>>> call, Response<ApiResponse<List<Country>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);

                if (response.code() == 200) {
                    ApiResponse<List<Country>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        List<Country> countries = apiResponse.getData();
                        if (countries != null) {
                            if (!countries.isEmpty()) {
                                countriesMutableLiveData.setValue(countries);
                            }
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));

            }

            @Override
            public void onFailure(Call<ApiResponse<List<Country>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return countriesMutableLiveData;
    }

    protected void update(Map<String, String> header, Map<String, Object> body, final ModelCallback callback) {
        Call<ApiResponse<User>> call = MySingleton.getInstance(context).createService().updateUser(header, body);

        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleEditProfileResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }

    protected void update(Map<String, String> header, Map<String, RequestBody> body, MultipartBody.Part photo, final ModelCallback callback) {
        Call<ApiResponse<User>> call = MySingleton.getInstance(context).createService().updateUser(header, body, photo);

        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleEditProfileResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t, int index);

        void showResponseMessage(String message);


        void handleEditProfileResponse(Response<ApiResponse<User>> response);
    }
}
