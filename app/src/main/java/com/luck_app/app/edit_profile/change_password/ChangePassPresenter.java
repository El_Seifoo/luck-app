package com.luck_app.app.edit_profile.change_password;

public interface ChangePassPresenter {
    void onChangeButtonClicked();
}
