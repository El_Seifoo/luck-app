package com.luck_app.app.contact_us;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Info;
import com.luck_app.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactModel {
    private Context context;

    public ContactModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<String>> stringsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<String>> fetchContact(String lang, final ModelCallback callback) {

        Call<ApiResponse<Info>> call = MySingleton.getInstance(context).createService().fetchInfo(lang);

        call.enqueue(new Callback<ApiResponse<Info>>() {
            @Override
            public void onResponse(Call<ApiResponse<Info>> call, Response<ApiResponse<Info>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 200) {
                    if (response.body().getStatus().equals(context.getString(R.string.api_status_true))) {
                        Info info = response.body().getData();
                        if (info != null) {
                            List<String> contactData = new ArrayList<>();
                            if (info.getPhoneNumber() == null)
                                contactData.add("");
                            else contactData.add(info.getPhoneNumber());

                            if (info.getWhatsNumber() == null)
                                contactData.add("");
                            else contactData.add(info.getWhatsNumber());

                            if (info.getEmail() == null)
                                contactData.add("");
                            else contactData.add(info.getEmail());

                            stringsMutableLiveData.setValue(contactData);
                        } else callback.showResponseMessage(response.body().getMessage());

                    } else callback.showResponseMessage(response.body().getMessage());
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data));
            }

            @Override
            public void onFailure(Call<ApiResponse<Info>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
        return stringsMutableLiveData;
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void showResponseMessage(String message);
    }
}
