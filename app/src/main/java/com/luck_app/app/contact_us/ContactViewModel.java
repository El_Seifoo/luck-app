package com.luck_app.app.contact_us;

import android.Manifest;
import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.util.List;

public class ContactViewModel extends AndroidViewModel implements ContactModel.ModelCallback {
    private ContactModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;

    public ContactViewModel(@NonNull Application application) {
        super(application);
        model = new ContactModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        Call model fun. fetchContact to get contact data data
        show loader then make req.
            List indices
                |-> 0 : phone
                |-> 1 : whats app
                |-> 2 : email
     */
    protected MutableLiveData<List<String>> requestContactUs() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchContact(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                this);
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : call us
                |-> 1 : whats app
                |-> 2 : email us
                |-> 3 : retry fetching contact us data
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                String phone = viewListener.getContactsData(index);
                if (!phone.isEmpty()) {
                    if (ContextCompat.checkSelfPermission(getApplication(),
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        viewListener.requestPhoneCallPermission();
                    } else
                        makePhoneCall(phone);
                } else
                    viewListener.showToastMessage(getApplication().getString(R.string.sorry_phone_not_available_now));
                break;
            case 1:
                String whatsApp = viewListener.getContactsData(index);
                if (!whatsApp.isEmpty())
                    viewListener.startIntent(new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/" + whatsApp)));
                else
                    viewListener.showToastMessage(getApplication().getString(R.string.sorry_whats_app_not_available_now));
                break;
            case 2:
                viewListener.showSendMailDialogFragment();
                break;
            case 3:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchContact(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)),
                        this);

        }
    }

    private void makePhoneCall(String phone) {
        viewListener.startIntent(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone)));
    }

    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == ContactUsActivity.PHONE_CALL_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makePhoneCall(viewListener.getContactsData(0));
            }
        } else
            viewListener.showToastMessage(getApplication().getString(R.string.permission_denied));
    }

    // Setters & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setters & Getters ----------> END


    protected interface ViewListener {

        String getContactsData(int index);

        void startIntent(Intent intent);

        void showToastMessage(String message);

        void requestPhoneCallPermission();

        void showSendMailDialogFragment();
    }
}
