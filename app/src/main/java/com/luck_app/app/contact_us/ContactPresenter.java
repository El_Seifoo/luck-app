package com.luck_app.app.contact_us;

public interface ContactPresenter {
    void onButtonsClicked(int index);
}
