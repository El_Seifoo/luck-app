package com.luck_app.app.contact_us;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.luck_app.app.R;
import com.luck_app.app.send_mail.SendMailDialogFragment;
import com.luck_app.app.databinding.ActivityContactUsBinding;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;

import java.util.List;

public class ContactUsActivity extends AppCompatActivity implements ContactPresenter, ContactViewModel.ViewListener {
    protected static final int PHONE_CALL_REQUEST = 1;
    private ActivityContactUsBinding dataBinding;
    private ContactViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        viewModel = new ViewModelProvider(this).get(ContactViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle(getString(R.string.about_the_app));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestContactUs().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
//                for (String string : strings) {
//                    Log.e("string", string);
//                }
                dataBinding.setContacts(strings);
            }
        });
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : call us
                |-> 1 : whats app
                |-> 2 : email us
                |-> 3 : retry fetching contact us data
     */
    @Override
    public void onButtonsClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public String getContactsData(int index) {
        return (String) dataBinding.getContacts().get(index);
    }

    @Override
    public void startIntent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void requestPhoneCallPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_REQUEST);
    }

    @Override
    public void showSendMailDialogFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null)
            fragmentTransaction.remove(prev);

        fragmentTransaction.addToBackStack(null);

        DialogFragment dialogFragment = SendMailDialogFragment.newInstance("");
        dialogFragment.show(fragmentTransaction, "dialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
