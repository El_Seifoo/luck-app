package com.luck_app.app.private_competition.check_external_private_password;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.luck_app.app.R;
import com.luck_app.app.databinding.PasswordDialogFragmentLayoutBinding;
import com.luck_app.app.models.Competition;

public class PasswordDialogFragment extends DialogFragment implements PasswordViewModel.ViewListener, PasswordPresenter {
    private PasswordDialogFragmentLayoutBinding dataBinding;
    private PasswordViewModel viewModel;
    private ParentListener listener /* communicator betn. dialog and its parent (PrivateActivity) */;

    public static PasswordDialogFragment newInstance(String competitionId) {
        PasswordDialogFragment fragment = new PasswordDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("competitionId", competitionId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.password_dialog_fragment_layout, container, false);
        viewModel = new ViewModelProvider(this).get(PasswordViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        listener = (ParentListener) getActivity();

        return dataBinding.getRoot();
    }

    @Override
    public void onSubmitButtonClicked() {
        viewModel.requestDetails(getArguments().getString("competitionId"), dataBinding.passwordEditText);
    }

    @Override
    public void showEditTextError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCompetitionDetails(Competition competition) {
        listener.setCompetitionDetails(competition);
        dismiss();
    }

    public interface ParentListener {
        void setCompetitionDetails(Competition competition);
    }
}
