package com.luck_app.app.private_competition;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.R;
import com.luck_app.app.databinding.PrivateCompetitionsListItemBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.PrivateCompetition;

import java.util.List;

public class PrivateAdapter extends RecyclerView.Adapter<PrivateAdapter.Holder> {
    private List<PrivateCompetition> competitions;
    private final ItemListener listener;

    public PrivateAdapter(ItemListener listener) {
        this.listener = listener;

    }

    public void setCompetitions(List<PrivateCompetition> competitions) {
        this.competitions = competitions;
        notifyDataSetChanged();
    }

    public void setProgressState(int index, int state) {
        competitions.get(index).setProgress(state);
        notifyDataSetChanged();
    }

    public int isActiveByIndex(int position) {
        return competitions.get(position).getActive();
    }

    public int getSelectedIndex() {
        for (int i = 0; i < competitions.size(); i++) {
            if (competitions.get(i).getProgress() == View.VISIBLE) {
                return i;
            }
        }
        return 0;
    }

    public interface ItemListener {
        void onPrivateItemClickListener(String id, String password, int index);
    }

    @NonNull
    @Override
    public PrivateAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((PrivateCompetitionsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.private_competitions_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PrivateAdapter.Holder holder, int position) {
        holder.dataBinding.setCompetition(competitions.get(position));
    }

    @Override
    public int getItemCount() {
        return competitions != null ? competitions.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private PrivateCompetitionsListItemBinding dataBinding;

        public Holder(@NonNull PrivateCompetitionsListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;

            dataBinding.submitButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onPrivateItemClickListener(dataBinding.getCompetition().getId(), dataBinding.getCompetition().getPassword(), getAdapterPosition());
        }
    }
}
