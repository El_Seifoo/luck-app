package com.luck_app.app.private_competition.check_external_private_password;

import android.content.Context;
import android.view.View;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordModel {
    private Context context;

    public PasswordModel(Context context) {
        this.context = context;
    }

    protected void fetchDetails(Map<String, String> body, Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<Competition>> call = MySingleton.getInstance(context).createService().privateCompDetails(header, body);

        call.enqueue(new Callback<ApiResponse<Competition>>() {
            @Override
            public void onResponse(Call<ApiResponse<Competition>> call, Response<ApiResponse<Competition>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleFetchingDetailsResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse<Competition>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });

    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleFetchingDetailsResponse(Response<ApiResponse<Competition>> response);
    }
}
