package com.luck_app.app.private_competition;

public interface PrivatePresenter {
    void onButtonClicked(int index);
}
