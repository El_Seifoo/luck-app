package com.luck_app.app.private_competition.check_external_private_password;

public interface PasswordPresenter {
    void onSubmitButtonClicked();
}
