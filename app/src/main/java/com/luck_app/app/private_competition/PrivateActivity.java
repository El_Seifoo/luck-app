package com.luck_app.app.private_competition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.islamkhsh.CardSliderViewPager;
import com.luck_app.app.R;
import com.luck_app.app.SliderAdapter;
import com.luck_app.app.databinding.ActivityPrivateBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.private_competition.check_external_private_password.PasswordDialogFragment;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;

public class PrivateActivity extends AppCompatActivity implements PrivateViewModel.ViewListener, PrivatePresenter, PrivateAdapter.ItemListener, PasswordDialogFragment.ParentListener, SliderAdapter.SliderItemListener {
    private PrivateViewModel viewModel;
    private PrivateAdapter adapter;
    private SliderAdapter sliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        ActivityPrivateBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_private);
        viewModel = new ViewModelProvider(this).get(PrivateViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        RecyclerView privateList = dataBinding.privateList;
        adapter = new PrivateAdapter(this);
        privateList.setAdapter(adapter);

        CardSliderViewPager cardSliderViewPager = dataBinding.viewPager;
        sliderAdapter = new SliderAdapter(this);
        cardSliderViewPager.setAdapter(sliderAdapter);

        viewModel.requestCompetitions().observe(this, new Observer<CompetitionsTrendsResponse<PrivateCompetition>>() {
            @Override
            public void onChanged(CompetitionsTrendsResponse<PrivateCompetition> competitionCompetitionsTrendsResponse) {
                adapter.setCompetitions(competitionCompetitionsTrendsResponse.getList());
                sliderAdapter.setImages(competitionCompetitionsTrendsResponse.getSlider());
            }
        });

        viewModel.checkIntents(getIntent().getData());
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(this, destination));
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void NavigateToDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void setPrivateItemProgress(int index, int progress) {
        adapter.setProgressState(index, progress);
    }

    @Override
    public int getSelectedIndex() {
        return adapter.getSelectedIndex();
    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : notification icon
                |-> 2 : retry fetching competitions request
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public void onPrivateItemClickListener(String id, String password, int index) {
        viewModel.requestDetails(id, password, index);
    }

    @Override
    public void setCompetitionDetails(Competition competition) {
        viewModel.setDetails(competition, true);
    }

    @Override
    public void showPasswordDialog(String competitionId) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null)
            fragmentTransaction.remove(prev);

        fragmentTransaction.addToBackStack(null);

        DialogFragment dialogFragment = PasswordDialogFragment.newInstance(competitionId);
        dialogFragment.show(fragmentTransaction, "dialog");

    }

    @Override
    public int isActive(int selectedIndex) {
        return adapter.isActiveByIndex(selectedIndex);
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        })
                .withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();
    }
}
