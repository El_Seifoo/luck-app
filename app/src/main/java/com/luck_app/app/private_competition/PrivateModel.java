package com.luck_app.app.private_competition;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivateModel {
    private Context context;

    public PrivateModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<CompetitionsTrendsResponse<PrivateCompetition>> competitionMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<CompetitionsTrendsResponse<PrivateCompetition>> fetchCompetitions(Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>>> call = MySingleton.getInstance(context).createService().fetchPrivateCompetitions(header);

        call.enqueue(new Callback<ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>>>() {
            @Override
            public void onResponse(Call<ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>>> call, Response<ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 200) {
                    ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        CompetitionsTrendsResponse<PrivateCompetition> competitionsResponse = apiResponse.getData();
                        List<PrivateCompetition> competitions = competitionsResponse.getList();
                        List<String> slider = competitionsResponse.getSlider();
                        if (competitions == null && slider == null) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            callback.setSliderVisibility(View.GONE);
                        } else if (competitions == null || slider == null) {
                            checkCompetitions(competitions, callback);
                            checkSlider(slider, callback);
                            competitionMutableLiveData.setValue(competitionsResponse);
                        } else if (competitions.isEmpty() && slider.isEmpty()) {
                            callback.setEmptyListTextView(View.VISIBLE);
                            callback.setSliderVisibility(View.GONE);
                        } else if (competitions.isEmpty() || slider.isEmpty()) {
                            checkCompetitions(competitions, callback);
                            checkSlider(slider, callback);
                            competitionMutableLiveData.setValue(competitionsResponse);
                        } else {
                            callback.setEmptyListTextView(View.GONE);
                            callback.setSliderVisibility(View.VISIBLE);
                            competitionMutableLiveData.setValue(competitionsResponse);
                        }
                    } else callback.showResponseMessage(apiResponse.getMessage(), 0);
                } else
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data), 0);
            }

            @Override
            public void onFailure(Call<ApiResponse<CompetitionsTrendsResponse<PrivateCompetition>>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });


        return competitionMutableLiveData;
    }

    protected void fetchDetails(Map<String, String> body, Map<String, String> header, final ModelCallback callback) {
        Call<ApiResponse<Competition>> call = MySingleton.getInstance(context).createService().privateCompDetails(header, body);

        call.enqueue(new Callback<ApiResponse<Competition>>() {
            @Override
            public void onResponse(Call<ApiResponse<Competition>> call, Response<ApiResponse<Competition>> response) {
                if (response.code() == 200) {
                    ApiResponse<Competition> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(context.getString(R.string.api_status_true))) {
                        Competition competition = apiResponse.getData();
                        if (competition != null)
                            callback.setDetails(competition, false);
                        else
                            callback.showResponseMessage(apiResponse.getMessage(), 1);

                    } else
                        callback.showResponseMessage(apiResponse.getMessage(), 1);
                } else if (response.code() == 500)
                    callback.showResponseMessage(context.getString(R.string.error_fetching_data), 1);
                else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        callback.showResponseMessage(jsonObject.getString("message"), 1);
                    } catch (Exception e) {
                        callback.showResponseMessage(context.getString(R.string.error_sending_data), 1);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Competition>> call, Throwable t) {
                callback.onFailureHandler(t, 1);
            }
        });

    }

    private void checkCompetitions(List<PrivateCompetition> list, ModelCallback callback) {
        if (list == null) callback.setEmptyListTextView(View.VISIBLE);
        else if (list.isEmpty()) callback.setEmptyListTextView(View.VISIBLE);
        else callback.setEmptyListTextView(View.GONE);
    }

    private void checkSlider(List<String> list, ModelCallback callback) {
        if (list == null) callback.setSliderVisibility(View.GONE);
        else if (list.isEmpty()) callback.setSliderVisibility(View.GONE);
        else callback.setSliderVisibility(View.VISIBLE);
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setEmptyListTextView(int empty);

        void setSliderVisibility(int visibility);

        void onFailureHandler(Throwable t, int index);

        void showResponseMessage(String message, int index);

        void setDetails(Competition competition, boolean externalLink);
    }

}
