package com.luck_app.app.private_competition;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.luck_app.app.R;
import com.luck_app.app.competition_details.CompetitionDetailsActivity;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.notifications.NotificationsActivity;
import com.luck_app.app.previous_competition_details.PrevCompDetailsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PrivateViewModel extends AndroidViewModel implements PrivateModel.ModelCallback {
    private PrivateModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    private ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Integer> emptyListTextView /* flag to know if competitions list is empty or not */;
    private ObservableField<Integer> sliderVisibility /* flag to know if slider is visible or no */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;
    private ObservableField<String> errorMessage/* response error message displayed on error view*/;
    private ObservableField<Integer> errorView/* flag to know if error view is visible or not */;
    private boolean isDetailsLoading;

    public PrivateViewModel(@NonNull Application application) {
        super(application);
        model = new PrivateModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        sliderVisibility = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
    }

    public void checkIntents(Uri uri) {
        if (uri != null) {
            if (MySingleton.getInstance(getApplication()).isLoggedIn())
                viewListener.showPasswordDialog(uri.getLastPathSegment());
            else
                viewListener.showToastMessage(getApplication().getString(R.string.please_login_first_before_using_this_link));
        }


    }


    /*
        Call model fun. fetchCompetitions to get private competitions
        show loader and disable all buttons then make req.
     */
    protected MutableLiveData<CompetitionsTrendsResponse<PrivateCompetition>> requestCompetitions() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        return model.fetchCompetitions(getRequestHeader(), this);
    }


    protected void requestDetails(String id, String password, int index) {
        if (isDetailsLoading) return;
        if (password.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_length_error));
            return;
        }

        isDetailsLoading = true;
        viewListener.setPrivateItemProgress(index, View.VISIBLE);
        Map<String, String> body = new HashMap<>();
        body.put("competion", id);
        body.put("password", password);

        model.fetchDetails(body, getRequestHeader(), this);
    }

    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(getFailureMessage(t));
        } else {
            isDetailsLoading = false;
            viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(), View.GONE);
            viewListener.showToastMessage(getFailureMessage(t));
        }

    }

    private String getFailureMessage(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    /*
        show response message
     */
    @Override
    public void showResponseMessage(String message, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(message);
        } else {
            isDetailsLoading = false;
            viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(), View.GONE);
            viewListener.showToastMessage(message);
        }

    }

    @Override
    public void setDetails(Competition competition, boolean externalLink) {
        if (!externalLink) {
            isDetailsLoading = false;
            viewListener.setPrivateItemProgress(viewListener.getSelectedIndex(), View.GONE);
        }
        long endLong = 0l;
        long currentLong = 0l;
        try {
            endLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(competition.getStartDate()).getTime() + 86400000;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date today = Calendar.getInstance().getTime();
            currentLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dateFormat.format(today)).getTime();


        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            Intent intent = null;
            if (currentLong >= endLong) //prev
                intent = new Intent(getApplication(), PrevCompDetailsActivity.class);
            else
                intent = new Intent(getApplication(), CompetitionDetailsActivity.class);

            intent.putExtra("competition", competition);
            viewListener.NavigateToDestination(intent);
        }

    }

    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : notification icon
                |-> 2 : retry fetching competitions request
     */
    public void handleButtonsAction(int index) {
        switch (index) {
            case 0:
                viewListener.finishActivity();
                break;
            case 1:
                viewListener.navigateToDestination(NotificationsActivity.class);
                break;
            case 2:
                setErrorView(View.GONE);
                setProgress(View.VISIBLE);
                setButtonsClickable(false);

                model.fetchCompetitions(getRequestHeader(), this);
        }
    }

    private Map<String, String> getRequestHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));
        return header;
    }

    // Setter & Getters ----------> START
    public boolean isDetailsLoading() {
        return isDetailsLoading;
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    public ObservableField<Integer> getSliderVisibility() {
        return sliderVisibility;
    }

    @Override
    public void setSliderVisibility(int visibility) {
        this.sliderVisibility.set(visibility);
    }


    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {
        void finishActivity();

        void navigateToDestination(Class destination);

        void showToastMessage(String message);

        void NavigateToDestination(Intent intent);

        void setPrivateItemProgress(int index, int progress);

        int getSelectedIndex();

        void showPasswordDialog(String competitionId);

        int isActive(int selectedIndex);
    }

}
