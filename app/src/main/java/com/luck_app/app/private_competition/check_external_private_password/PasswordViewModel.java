package com.luck_app.app.private_competition.check_external_private_password;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.luck_app.app.R;
import com.luck_app.app.models.ApiResponse;
import com.luck_app.app.models.Competition;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class PasswordViewModel extends AndroidViewModel implements PasswordModel.ModelCallback {
    private PasswordModel model;
    private ViewListener viewListener/* communicator betn. view model and its view */;
    protected ObservableField<Integer> progress/* flag to know the status of progress dialog */;
    private ObservableField<Boolean> buttonsClickable/* flag to disable or enable buttons */;

    public PasswordViewModel(@NonNull Application application) {
        super(application);
        model = new PasswordModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    protected void requestDetails(String competitionId, EditText passwordEditText) {
        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_length_error));
            return;
        }

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language_key)));

        Map<String, String> body = new HashMap<>();
        body.put("competion", competitionId);
        body.put("password", password);

        model.fetchDetails(body, header, this);
    }


    /*
        handle error response of requests
     */
    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    @Override
    public void handleFetchingDetailsResponse(Response<ApiResponse<Competition>> response) {
        int code = response.code();
        if (code == 200) {
            ApiResponse<Competition> apiResponse = response.body();
            if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_status_true))) {
                Competition competition = apiResponse.getData();
                if (competition != null)
                    viewListener.setCompetitionDetails(competition);
                else
                    viewListener.showToastMessage(apiResponse.getMessage());

            } else
                viewListener.showToastMessage(apiResponse.getMessage());
        } else if (code == 500)
            viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
        else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                viewListener.showToastMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            }
        }
    }


    // Setter & Getters ----------> START
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setter & Getters ----------> END


    protected interface ViewListener {

        void showEditTextError(EditText editText, String message);

        void showToastMessage(String message);

        void setCompetitionDetails(Competition competition);
    }
}
