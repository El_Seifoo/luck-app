package com.luck_app.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.luck_app.app.databinding.CompetitionListItemBinding;
import com.luck_app.app.models.Competition;

import java.util.List;

public class CompetitionsAdapter extends RecyclerView.Adapter<CompetitionsAdapter.Holder> {
    private List<Competition> competitions;
    private final CompetitionItemListener listener;

    public CompetitionsAdapter(CompetitionItemListener listener) {
        this.listener = listener;
    }

    public void setCompetitions(List<Competition> competitions) {
        this.competitions = competitions;
        notifyDataSetChanged();
    }

    public interface CompetitionItemListener {
        void onCompetitionClicked(Competition competition);
    }

    @NonNull
    @Override
    public CompetitionsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CompetitionListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.competition_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CompetitionsAdapter.Holder holder, int position) {
        holder.dataBinding.setCompetition(competitions.get(position));
    }

    @Override
    public int getItemCount() {
        return competitions != null ? competitions.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CompetitionListItemBinding dataBinding;

        public Holder(@NonNull CompetitionListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onCompetitionClicked(dataBinding.getCompetition());
        }
    }
}
