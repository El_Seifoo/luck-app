package com.luck_app.app.previous_competition;

public interface PreviousPresenter {
    void onButtonClicked(int index);
}
