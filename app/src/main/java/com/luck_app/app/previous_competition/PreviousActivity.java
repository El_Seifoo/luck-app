package com.luck_app.app.previous_competition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.islamkhsh.CardSliderViewPager;
import com.luck_app.app.CompetitionsAdapter;
import com.luck_app.app.R;
import com.luck_app.app.SliderAdapter;
import com.luck_app.app.competition_details.CompetitionDetailsActivity;
import com.luck_app.app.databinding.ActivityPreviousBinding;
import com.luck_app.app.models.Competition;
import com.luck_app.app.models.CompetitionsTrendsResponse;
import com.luck_app.app.models.PrivateCompetition;
import com.luck_app.app.previous_competition_details.PrevCompDetailsActivity;
import com.luck_app.app.utils.Constants;
import com.luck_app.app.utils.Language;
import com.luck_app.app.utils.MySingleton;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;

public class PreviousActivity extends AppCompatActivity implements PreviousViewModel.ViewListener, PreviousPresenter, PreviousAdapter.CompetitionItemListener, PreviousAdapter.PrivateCompetitionItemListener, SliderAdapter.SliderItemListener {
    private PreviousViewModel viewModel;
    private PreviousAdapter adapter;
    private SliderAdapter sliderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.default_language_key)));
        ActivityPreviousBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_previous);
        viewModel = new ViewModelProvider(this).get(PreviousViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        RecyclerView previousList = dataBinding.previousList;
        adapter = new PreviousAdapter(this, this);
        previousList.setAdapter(adapter);

        CardSliderViewPager cardSliderViewPager = dataBinding.viewPager;
        sliderAdapter = new SliderAdapter(this);
        cardSliderViewPager.setAdapter(sliderAdapter);

        viewModel.requestCompetitions().observe(this, new Observer<CompetitionsTrendsResponse<Competition>>() {
            @Override
            public void onChanged(CompetitionsTrendsResponse<Competition> competitionCompetitionsTrendsResponse) {
                ArrayList<Object> adapterList = new ArrayList<>();
                ArrayList<Competition> responseList = (ArrayList<Competition>) competitionCompetitionsTrendsResponse.getList();
                PrivateCompetition privateComp;
                Competition competition1;

                for (int i = 0; i < responseList.size(); i++) {
                    if (responseList.get(i).getCompetitionType() == 1) {
                        adapterList.add(responseList.get(i));
                    } else {
                        competition1 = new PrivateCompetition();
                        competition1.setData(responseList.get(i));
                        privateComp = (PrivateCompetition) competition1;
                        privateComp.setProgress(View.GONE);
                        adapterList.add(privateComp);
                    }
                }

                adapter.setCompetitions(adapterList);
                sliderAdapter.setImages(competitionCompetitionsTrendsResponse.getSlider());
            }
        });
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(this, destination));
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void setPrivateItemProgress(int index, int progress) {
        adapter.setProgressState(index, progress);
    }

    @Override
    public int getSelectedIndex() {
        return adapter.getSelectedIndex();
    }

    @Override
    public void NavigateToDestination(Intent intent) {
        startActivity(intent);
    }


    /*
        handle action of every button in the activity
        @Param index .. flag to know which button was clicked
                |-> 0 : back icon
                |-> 1 : notification icon
                |-> 2 : retry fetching competitions req.
     */
    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }


    @Override
    public void onCompetitionClicked(Competition competition) {
        Intent intent = new Intent(getApplicationContext(), PrevCompDetailsActivity.class);
        intent.putExtra("competition", competition);
        startActivity(intent);
    }

    @Override
    public void onPrivateCompetitionClicked(Competition competition, String password, int index) {
        viewModel.requestDetails(competition.getId(), password, index);
    }

    @Override
    public void onClick(int position, ArrayList<String> photos) {
        new StfalconImageViewer.Builder<>(this, photos, new ImageLoader<String>() {
            @Override
            public void loadImage(ImageView imageView, String image) {
                Glide.with(imageView.getContext())
                        .load(image)
                        .thumbnail(0.5f)
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        })
                .withStartPosition(position)
                .withBackgroundColor(getResources().getColor(R.color.main_light_color))
                .allowSwipeToDismiss(true)
                .show();
    }
}
