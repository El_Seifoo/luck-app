<?xml version="1.0" encoding="utf-8"?>
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:fillViewport="true"
    tools:context=".forget_password.ForgetPasswordActivity">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@drawable/main_background">

        <View
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:background="@drawable/transparent_white_30_oval"
            app:layout_constraintBottom_toBottomOf="@id/forget_password_icon"
            app:layout_constraintEnd_toEndOf="@id/forget_password_icon"
            app:layout_constraintStart_toStartOf="@id/forget_password_icon"
            app:layout_constraintTop_toTopOf="@id/forget_password_icon" />

        <ImageView
            android:id="@+id/forget_password_icon"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:padding="@dimen/_24sdp"
            android:src="@drawable/ic_forget_password_label_icon"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.35" />

        <TextView
            android:id="@+id/forget_password_label"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/_6sdp"
            android:gravity="center"
            android:text="@string/forget_password"
            android:textColor="@color/white"
            android:textSize="@dimen/_20sdp"
            android:textStyle="bold"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/forge